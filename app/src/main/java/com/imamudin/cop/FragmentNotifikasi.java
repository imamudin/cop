package com.imamudin.cop;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.imamudin.cop.berita_acara.Interogasi;
import com.imamudin.cop.berita_acara.InterogasiNotif;
import com.imamudin.cop.berita_acara.LITDokumenNotif;
import com.imamudin.cop.berita_acara.ObservasiNotif;
import com.imamudin.cop.berita_acara.SurveillanceNotif;
import com.imamudin.cop.config.CDatabase;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.listAdapter.ListAdapterNotifikasi;
import com.imamudin.cop.db.DBHelper;
import com.imamudin.cop.location.LokasiKejadian;
import com.imamudin.cop.model.NotifikasiItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by imamudin on 10/11/16.
 */
public class FragmentNotifikasi extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    DBHelper mydb;
    List<NotifikasiItem> notifikasis = new ArrayList<NotifikasiItem>();
    ListView lv1;
    ListAdapterNotifikasi adapter;

    private SwipeRefreshLayout swipeContainer;

    int offSet=0;

    Runnable runnable;
    Boolean disableSwipeDown = false;       //untuk mendisable swipe down list view

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener lv1ener;
    View parentLayout;


    public FragmentNotifikasi() {
    }
    public static FragmentNotifikasi newInstance(String param1, String param2) {
        FragmentNotifikasi fragment = new FragmentNotifikasi();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        ((MainActivityMonitor) getActivity()).setActionBarTitle("Notifikasi");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_notifikasi, container, false);

        //untuk mengganti menu
        setHasOptionsMenu(true);

        parentLayout = rootView.findViewById(R.id.parentlayout);
        lv1 = (ListView)rootView.findViewById(R.id.lv_notifikasi);
        notifikasis.clear();
        adapter = new ListAdapterNotifikasi(getActivity(), notifikasis);
        lv1.setAdapter(adapter);

        //untuk swipelist
        swipeContainer = (SwipeRefreshLayout)rootView.findViewById(R.id.swipeContainer);

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                notifikasis.clear();
                adapter.notifyDataSetChanged();
                callNews(0);
                Toast.makeText(getActivity(),"refresh",Toast.LENGTH_LONG).show();
            }
        });

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected item text from ListView
                Object o = lv1.getItemAtPosition(position);
                NotifikasiItem notifikasiItem = (NotifikasiItem) o;

                Log.d(GlobalConfig.TAG, "berkas ke : "+notifikasiItem.getBerkas_ke()+" id kejadian "+notifikasiItem.getKasus_id());

                if(notifikasiItem.getBerkas_ke() == 0){
                    //berkas ke -1 untuk kejadian baru
                    Intent kejadian = new Intent(getActivity(), LokasiKejadian.class);
                    kejadian.putExtra(GlobalConfig.G_ID_KEJADIAN, notifikasiItem.getKasus_id());
                    startActivity(kejadian);
                }else {
                    Intent output = null;
                    if (notifikasiItem.getJenisBerkas().equals("DOC11")) {        //untuk observasi
                        output = new Intent(getActivity(), ObservasiNotif.class);
                    } else if (notifikasiItem.getJenisBerkas().equals("DOC12")) {        //untuk interogasi
                        output = new Intent(getActivity(), InterogasiNotif.class);
                    } else if (notifikasiItem.getJenisBerkas().equals("DOC13")) {        //untuk surveillance
                        output = new Intent(getActivity(), SurveillanceNotif.class);
                    } else if (notifikasiItem.getJenisBerkas().equals("DOC16")) {        //untuk LIT Dokumen
                        output = new Intent(getActivity(), LITDokumenNotif.class);
                    } else if (notifikasiItem.getJenisBerkas().equals("DOC12")) {        //untuk interogasi
                        output = new Intent(getActivity(), InterogasiNotif.class);
                    }

                    if (output != null) {
                        output.putExtra("dari_notifikasi", true);
                        output.putExtra(CDatabase.N_NRP, notifikasiItem.getNrp());
                        output.putExtra(CDatabase.N_KASUS_ID, notifikasiItem.getKasus_id());
                        output.putExtra(CDatabase.N_BERKAS_KE, notifikasiItem.getBerkas_ke());

                        startActivity(output);
                    }
                }
            }
        });

        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
                notifikasis.clear();
                adapter.notifyDataSetChanged();
                callNews(0);
            }
        });

        lv1.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }
            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {
                    if(!disableSwipeDown) {
                        swipeContainer.setRefreshing(true);
                        Log.d(GlobalConfig.TAG, "start refresh");
                        runnable = new Runnable() {
                            public void run() {
                                Log.d(GlobalConfig.TAG, "start refresh");
                                callNews(offSet);
                            }
                        };
                        runnable.run();
                    }else{
                        Log.d(GlobalConfig.TAG, "disabled");
                        //Toast.makeText(CariKasus.this,"Data telah ditampilkan semua.",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        return rootView;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_menu_notifikasi, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_delete_notifikasi:
                showdialogBackPress();
                return true;
            default:
                break;
        }
        return false;
    }
    private void showdialogBackPress(){
        LayoutInflater layoutInflater = (LayoutInflater)getActivity().getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.notif_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);

        final TextView t_title  = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final TextView t_text   = (TextView) promptView.findViewById(R.id.t_text_dialog);
        t_title.setText("Hapus Notifikasi");
        t_text.setText("Apakah anda yakin menghapus semua notifikasi?");
        // setup a dialog window
        alertDialogBuilder.setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mydb = new DBHelper(getActivity());
                        mydb.deleteAllNotifikasi();
                        callNews(0);
                    }
                })
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    private void callNews(int page){
        swipeContainer.setRefreshing(true);
        HashMap<String, String> temp = new HashMap<String, String>();

        mydb = new DBHelper(getActivity());
        ArrayList<HashMap<String, String>> notifs = mydb.getLimitNotifikasi(page);

        NotifikasiItem notifikasiItem = null;
        String namaPolisi, jenisBerkas, namaKasus, nrp, kasus_id, waktu;
        if(notifs.size()<CDatabase.LIMIT){         //50 dari jumlah limit di web
            disableSwipeDown = true;
        }

        offSet+=notifs.size();
        for(int i=0; i<notifs.size();i++){
            temp    = notifs.get(i);

            int id      = Integer.parseInt(temp.get(CDatabase.N_ID));
            namaPolisi  = temp.get(CDatabase.N_NAMA_POLISI);
            jenisBerkas = temp.get(CDatabase.N_JENIS_BERKAS);
            namaKasus   = temp.get(CDatabase.N_NAMA_KASUS);
            nrp         = temp.get(CDatabase.N_NRP);
            kasus_id    = temp.get(CDatabase.N_KASUS_ID);
            waktu       = temp.get(CDatabase.N_WAKTU);
            int berkas_ke= Integer.parseInt(temp.get(CDatabase.N_BERKAS_KE));
            notifikasiItem = new NotifikasiItem(id, namaPolisi, jenisBerkas, namaKasus, nrp, kasus_id, waktu, berkas_ke);

            notifikasis.add(notifikasiItem);
            swipeContainer.setRefreshing(false);
            adapter.notifyDataSetChanged();
        }
        swipeContainer.setRefreshing(false);
        if(notifs.size()==0){
            Snackbar snack = Snackbar.make(parentLayout, "Tidak ada notifikasi.", Snackbar.LENGTH_LONG);
            snack.setActionTextColor(getResources().getColor(android.R.color.white ))
                    .show();
        }
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (lv1ener != null) {
            lv1ener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            lv1ener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        lv1ener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}