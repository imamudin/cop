package com.imamudin.cop.berita_acara;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.imamudin.cop.R;
import com.imamudin.cop.app.MyAppController;
import com.imamudin.cop.config.CDatabase;
import com.imamudin.cop.config.CLITDokumen;
import com.imamudin.cop.config.CLITDokumen;
import com.imamudin.cop.config.CLogin;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.location.ShowLokasi;
import com.imamudin.cop.mysp.ObscuredSharedPreferences;
import com.imamudin.cop.volley.CustomVolleyRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by imamudin on 08/11/16.
 */
public class LITDokumenNotif extends AppCompatActivity {

    LinearLayout llPertanyaan, llGambar, ll_notif_pesan, ll_main;
    TextView tv_nama_kasus, tv_dasar, tv_sasaran_barang, tv_koordinat;
    Button btn_kembali;
    Intent old;
    String nama_kasus, dasar, sbarang, koordinat;

    public JsonObjectRequest request =null;
    ObscuredSharedPreferences pref;
    private ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.berkas_lit_dokumen_notif);

        //set Toolbar
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Berkas LIT Dokumen");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        init();

        btn_kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity();
            }
        });

        old  = getIntent();

        Boolean dari_notifikasi = old.getBooleanExtra("dari_notifikasi", false);

        if(dari_notifikasi){
            ll_notif_pesan.setVisibility(View.GONE);
            view_notifikasi();
        }else{
            view_upload();
        }
    }
    private void init(){
        tv_nama_kasus   = (TextView)findViewById(R.id.tv_nama_kasus);
        tv_dasar        = (TextView)findViewById(R.id.tv_dasar);
        tv_sasaran_barang= (TextView)findViewById(R.id.tv_sasaran_barang);
        tv_koordinat    = (TextView)findViewById(R.id.tv_koordinat);
        btn_kembali     = (Button)findViewById(R.id.btn_kembali);

        llPertanyaan    = (LinearLayout)findViewById(R.id.ll_pertanyaan);
        llGambar        = (LinearLayout)findViewById(R.id.ll_gambar);
        ll_notif_pesan  = (LinearLayout)findViewById(R.id.ll_notif_pesan);
        ll_main         = (LinearLayout)findViewById(R.id.ll_main);

        pref = new ObscuredSharedPreferences(this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
    }
    private void view_upload(){
        nama_kasus   = old.getStringExtra(CLITDokumen.KEYUP_KASUS_NAMA);
        dasar        = old.getStringExtra(CLITDokumen.KEYUP_DASAR);
        sbarang      = old.getStringExtra(CLITDokumen.KEYUP_SASARAN_BARANG);
        koordinat    = old.getStringExtra(CLITDokumen.KEYUP_KOORDINAT);

        tv_nama_kasus.setText(nama_kasus);
        tv_dasar.setText(dasar);
        tv_sasaran_barang.setText(sbarang);
        tv_koordinat.setText(koordinat);



        try {
            JSONArray temuans     = new JSONArray(old.getStringExtra(CLITDokumen.KEYUP_TEMUAN));
            JSONArray analisas    = new JSONArray(old.getStringExtra(CLITDokumen.KEYUP_ANALISA));

            if(temuans.length()>0 && analisas.length()>0){
                for(int i=0; i<temuans.length();i++){
                    add_layout_pertanyaan(""+temuans.get(i), ""+analisas.get(i));
                }
            }

            JSONArray gambar    = new JSONArray(old.getStringExtra(CLITDokumen.KEYUP_GAMBAR));
            if(gambar.length()>0){
                for(int i=0; i<gambar.length();i++){
                    add_layout_gambar(decodeSampledBitmapFromFile((String) gambar.get(i), 1680, 960));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void view_notifikasi(){
        String nrp = old.getStringExtra(CDatabase.N_NRP);
        String kasus_id = old.getStringExtra(CDatabase.N_KASUS_ID);
        int berkas_ke = old.getIntExtra(CDatabase.N_BERKAS_KE, -1);

        getBerkas(nrp, kasus_id, berkas_ke);
    }
    private void getBerkas(String nrp, String kasus_id, int berkas_ke){
        final ProgressDialog loading = new ProgressDialog(this);
        loading.setTitle("Mencari berkas");
        loading.setMessage("Mohon tunggu...");
        loading.setCancelable(false);
        loading.setButton(DialogInterface.BUTTON_NEGATIVE, "Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cancelRequest();
                dialog.dismiss();
            }
        });
        loading.show();
        String url ="";
        if(pref.getString(GlobalConfig.IP_KEY, null) != null){
            url = "http://"+pref.getString(GlobalConfig.IP_KEY, "")+GlobalConfig.WEB_URL+ CLITDokumen.URL_GET_BERKAS;
        }else{
            url = "http://"+GlobalConfig.IP+GlobalConfig.WEB_URL+CLITDokumen.URL_GET_BERKAS;
        }
        //upload dokumen
        JSONObject jsonBody;
        Log.d(GlobalConfig.TAG, url);
        try {
            jsonBody = new JSONObject();
            jsonBody.put(CLITDokumen.USER_ID, ""+pref.getString(CLogin.G_ID_USER,""));
            jsonBody.put(CLITDokumen.USER_REGID, ""+pref.getString(GlobalConfig.gcmregId,""));

            jsonBody.put(CLITDokumen.KEYUP_KASUS_ID, kasus_id);
            jsonBody.put(CLITDokumen.KEYUP_NRP, ""+nrp);
            jsonBody.put(CLITDokumen.KEYUP_BERKAS_KE, ""+berkas_ke);

            Log.d(GlobalConfig.TAG, jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        loading.dismiss();
                        int status  = response.getInt("status");
                        String pesan= response.getString("pesan");
                        JSONObject data = response.getJSONObject("data");
                        Log.d(GlobalConfig.TAG+" result ", ""+response.toString());
                        if(status==1){
                            JSONArray berkass = data.getJSONArray("berkas");
                            if(berkass.length()==1){
                                JSONObject berkas = berkass.getJSONObject(0);
                                nama_kasus   = berkas.getString(CLITDokumen.G_NAMA_KASUS);
                                dasar        = berkas.getString(CLITDokumen.G_DASAR);
                                sbarang      = berkas.getString(CLITDokumen.G_SASARAN_BARANG);
                                koordinat    = berkas.getString(CLITDokumen.G_LATITUDE)+";"+berkas.getString(CLITDokumen.G_LONGITUDE);

                                final String g_laTitude  = berkas.getString(CLITDokumen.G_LATITUDE);
                                final String g_longitude = berkas.getString(CLITDokumen.G_LONGITUDE);

                                Button btn_maps     = (Button)findViewById(R.id.btn_maps);

                                tv_nama_kasus.setText(nama_kasus);
                                tv_dasar.setText(dasar);
                                tv_sasaran_barang.setText(sbarang);
                                btn_maps.setText(koordinat+" | Lihat");
                                btn_maps.setVisibility(View.VISIBLE);
                                tv_koordinat.setVisibility(View.GONE);

                                btn_maps.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent showLokasi = new Intent(LITDokumenNotif.this, ShowLokasi.class);
                                        showLokasi.putExtra("LATITUDE", g_laTitude);
                                        showLokasi.putExtra("LONGITUDE", g_longitude);

                                        startActivity(showLokasi);
                                    }
                                });

                                JSONArray tanyas = data.getJSONArray("detail");
                                if(tanyas.length()>0){
                                    for(int i=0; i<tanyas.length();i++){
                                        JSONObject tanya = tanyas.getJSONObject(i);
                                        String s_tanya = tanya.getString(CLITDokumen.G_TEMUAN);
                                        String s_jawab = tanya.getString(CLITDokumen.G_ANALISA);

                                        Log.d(GlobalConfig.TAG, s_tanya+" "+s_jawab);
                                        add_layout_pertanyaan(""+s_tanya, ""+s_jawab);
                                    }
                                }
                                JSONArray gambars = data.getJSONArray("gambar");
                                if(gambars.length()>0){
                                    for(int i=0; i<gambars.length();i++){
                                        JSONObject gambar = gambars.getJSONObject(i);
                                        String s_gambar = "http://"+GlobalConfig.IP+GlobalConfig.WEB_URL+"/assets/images/"+CLITDokumen.KODE_BERKAS+"/"+gambar.getString(CLITDokumen.G_GAMBAR);

                                        add_layout_gambar_notifikasi(s_gambar);
                                    }
                                }
                            }
                        }else{
                            notifikasi(pesan);
                        }
                    } catch (JSONException e) {
                        loading.dismiss();
                        notifikasi(GlobalConfig.MSG_KESALAHAN);
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    loading.dismiss();
                    notifikasi(GlobalConfig.MSG_KESALAHAN);
                    //Log.d("respons",error.getMessage().toString());
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_NAME, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);
        } catch (JSONException e) {
            loading.dismiss();
            e.printStackTrace();
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    public void add_layout_pertanyaan(String tanya, String jawab){
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        final View inflatedLayout= inflater.inflate(R.layout.view_pertanyaan_notif, llPertanyaan, false);

        final TextView t_tanya  = (TextView) inflatedLayout.findViewById(R.id.tv_tanya);
        final TextView t_jawab  = (TextView) inflatedLayout.findViewById(R.id.tv_jawab);

        t_tanya.setText(tanya);
        t_jawab.setText(jawab);

        llPertanyaan.addView(inflatedLayout);
    }
    public void add_layout_gambar(Bitmap bmp){
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        final View inflatedLayout= inflater.inflate(R.layout.view_gambar_notif, llPertanyaan, false);

        final ImageView gambar  = (ImageView) inflatedLayout.findViewById(R.id.img_berkas);

        gambar.setImageBitmap(bmp);

        llGambar.addView(inflatedLayout);
    }
    //memperkecil ukuran gambar
    public static Bitmap decodeSampledBitmapFromFile(String imagePath, int reqWidth, int reqHeight) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(imagePath, options);
    }
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        int min = width;
        if (width > height) {
            int temp = reqHeight;
            reqHeight = reqWidth;
            reqWidth = temp;
        }
        Log.d(GlobalConfig.TAG+"copcamera1/", "height : " + height + ", width : " + width);
        if (height > reqHeight || width > reqWidth) {
            int halfHeight = height;
            int halfWidth = width;
            while (true) {
                if (halfHeight / inSampleSize <= reqHeight && halfWidth / inSampleSize <= reqWidth) {
                    break;
                }
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }
    public void closeActivity(){
        super.onBackPressed();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
        closeActivity();
    }
    public static Bitmap decodeBase64(String input)
    {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }
    public void add_layout_gambar_notifikasi(String url){
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        final View inflatedLayout= inflater.inflate(R.layout.view_gambar_notif, llPertanyaan, false);

        final NetworkImageView gambar  = (NetworkImageView) inflatedLayout.findViewById(R.id.imageView);

        imageLoader = CustomVolleyRequest.getInstance(getApplicationContext())
                .getImageLoader();
        imageLoader.get(url, ImageLoader.getImageListener(gambar,
                R.mipmap.ic_launcher_cop, android.R.drawable
                        .ic_dialog_alert));
        gambar.setImageUrl(url, imageLoader);

        llGambar.addView(inflatedLayout);
    }
}