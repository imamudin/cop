package com.imamudin.cop.berita_acara;

/**
 * Created by imamudin on 21/10/16.
 */

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.imamudin.cop.CariKasus;
import com.imamudin.cop.R;
import com.imamudin.cop.app.MyAppController;
import com.imamudin.cop.config.CInterogasi;
import com.imamudin.cop.config.CLogin;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.location.CariLokasi;
import com.imamudin.cop.mysp.ObscuredSharedPreferences;
import com.imamudin.cop.zoomimage.MainZoomImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by agung on 23/02/2016.
 */
public class Interogasi extends AppCompatActivity{

    Button btnSimpan, btnTambahPertanyaan;
    Button btnLokasi,  btnTambahGambar;
    LinearLayout llPertanyaan, llGambar;
    TextView tv_kasus_id, tv_kasus_nama, tv_keterangan_gambar;
    ImageView img_kasus_nama, img_nama, img_alamat, img_lokasi;
    EditText et_nama, et_lokasi, et_alamat;

    int id_gambar = 0;


    ObscuredSharedPreferences pref;

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_GALLERY = 2;
    static final int MAX_FOTO = 5;
    String imgDecodableString;                                              //untuk mengambil image dari gallery

    //untuk TextView warning
    TextView tv_warning_nama_kasus, tv_warning_lokasi, tv_warning_nama, tv_warning_alamat, tv_warning_pertanyaan, tv_warning_koordinat;


    // UI Widgets.
    protected TextView mLatLongLabel;
    protected TextView mLatLong;
    LinearLayout ll_koordinat;

    public String pictureImagePath="";

    public JsonObjectRequest request =null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.berkas_interogasi);

        init();
    }

    private void init(){
        //inisialisasi variabel
        pref = new ObscuredSharedPreferences(this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        btnTambahPertanyaan = (Button) findViewById(R.id.btn_tambah_pertanyaan);
        btnTambahGambar = (Button) findViewById(R.id.btn_tambah_gambar);
        btnSimpan = (Button) findViewById(R.id.btn_simpan);
        btnLokasi = (Button) findViewById(R.id.btnLokasi);

        llPertanyaan = (LinearLayout) findViewById(R.id.ll_pertanyaan);
        llGambar = (LinearLayout) findViewById(R.id.ll_tambah_gambar_dalam);

        tv_kasus_id = (TextView) findViewById(R.id.tv_kasus_id);
        tv_kasus_nama = (TextView) findViewById(R.id.tv_kasus_nama);
        tv_keterangan_gambar = (TextView) findViewById(R.id.tv_keterangan_gambar);

        //lokasi
        mLatLongLabel   = (TextView) findViewById(R.id.tv_latlong_label);
        mLatLong        = (TextView) findViewById(R.id.tv_latlong);
        ll_koordinat    = (LinearLayout) findViewById(R.id.ll_koordinat);

        img_kasus_nama = (ImageView) findViewById(R.id.img_kasus_nama);
        img_lokasi = (ImageView) findViewById(R.id.img_kasus_lokasi);
        img_alamat = (ImageView) findViewById(R.id.img_alamat);
        img_nama = (ImageView) findViewById(R.id.img_nama);

        et_nama = (EditText) findViewById(R.id.et_nama);
        et_lokasi = (EditText) findViewById(R.id.et_lokasi);
        et_alamat = (EditText) findViewById(R.id.et_alamat);

        et_nama.setOnFocusChangeListener(NoFocus);
        et_lokasi.setOnFocusChangeListener(NoFocus);
        et_alamat.setOnFocusChangeListener(NoFocus);


        tv_warning_nama_kasus   = (TextView)findViewById(R.id.tv_notif_nama_kasus);
        tv_warning_lokasi       = (TextView)findViewById(R.id.tv_notif_lokasi);
        tv_warning_nama         = (TextView)findViewById(R.id.tv_notif_nama);
        tv_warning_alamat       = (TextView)findViewById(R.id.tv_notif_alamat);
        tv_warning_pertanyaan   = (TextView)findViewById(R.id.tv_keterangan_pertanyaan);
        tv_warning_koordinat    = (TextView)findViewById(R.id.tv_notif_koordinate);

        //konfigurasi toolbar
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Form Interogasi");
        //actionBar.setIcon(R.drawable.search_24);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);
        //toolbar.setTitleTextColor(getResources().getColor(R.color.putih));

        btnTambahPertanyaan.setOnClickListener(btnClick);
        btnLokasi.setOnClickListener(btnClick);
        btnTambahGambar.setOnClickListener(btnClick);
        tv_kasus_nama.setOnClickListener(btnClick);
        btnSimpan.setOnClickListener(btnClick);
    }

    EditText.OnFocusChangeListener NoFocus= new View.OnFocusChangeListener(){
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(v==et_nama){
                if(!hasFocus){
                    showMessage(et_nama, img_nama, tv_warning_nama);
                    //Toast.makeText(getApplicationContext(),""+et_nama.getText().toString(),Toast.LENGTH_SHORT).show();
                }
            }else if(v==et_alamat){
                if(!hasFocus){
                    showMessage(et_alamat, img_alamat, tv_warning_alamat);
                    //Toast.makeText(getApplicationContext(),""+et.getText().toString(),Toast.LENGTH_SHORT).show();
                }
            }else if(v==et_lokasi){
                if(!hasFocus){
                    showMessage(et_lokasi, img_lokasi, tv_warning_lokasi);
                    //Toast.makeText(getApplicationContext(),""+et_nama.getText().toString(),Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    public void showMessage(final EditText et, final ImageView iv, final TextView tv){
        if(et.getText().toString().trim().length()<=0){
            iv.setImageDrawable(ContextCompat.getDrawable(Interogasi.this, R.drawable.ic_error));
            tv.setVisibility(View.VISIBLE);
        }else{
            iv.setImageDrawable(ContextCompat.getDrawable(Interogasi.this, R.drawable.ic_done));
            tv.setVisibility(View.GONE);
        }
    }


    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //untuk menambah gambar
            if (v == btnTambahGambar) {
                if (llGambar.getChildCount() < MAX_FOTO) {
                    showdialogFoto();
                } else {
                    Toast.makeText(getApplicationContext(), "Jumlah foto maksimal " + MAX_FOTO, Toast.LENGTH_SHORT).show();
                }
            }
            //untuk mencari kasus
            else if(v==tv_kasus_nama){
                showdialogCariKasus();
            }
            //untuk menyimpan semua data
            else if(v==btnSimpan){
                btnSimpan.setEnabled(false);
                simpanForm();
            }
            //untuk mendapatkan lokasi
            else if(v==btnLokasi){
                Intent iCariKasus = new Intent(Interogasi.this,CariLokasi.class);
                startActivityForResult(iCariKasus, CInterogasi.KODE_BERKAS1_CARILOKASI);

                //getLokasi();
                //startActivity(new Intent(Interogasi.this, MainLocation.class));
                //checkLocationSettings();
            }
            //untuk menambah pertanyaan
            else if(v==btnTambahPertanyaan){
                showdialogTambahPertanyaan();
            }
        }
    };
    public void simpanForm(){
        //untuk kasus id
        final String kasus_id = tv_kasus_id.getText().toString().trim();

        //untuk lokasi TKP, nama, alamat
        final String lokasi   = et_lokasi.getText().toString().trim();
        final String nama     = et_nama.getText().toString().trim();
        final String alamat   = et_alamat.getText().toString().trim();

        //inisialisai pertanyaan jawab
        final List<String> tanya = new ArrayList<String>();
        final List<String> jawab = new ArrayList<String>();
        jawab.clear();
        tanya.clear();

        //untuk mendapatkan pertanyaan
        //linear layout level 1
        int total1 = 0, total2 = 0, total3 = 0;
        int count = llPertanyaan.getChildCount();
        View vChild = null;
        Log.d(GlobalConfig.TAG+"tanya : total ", "" + count);
        for (int i = 0; i < count; i++) {
            vChild = llPertanyaan.getChildAt(i);
            //linear layout level2
            if (vChild instanceof LinearLayout) {
                View vChild2 = null;
                total2 = ((LinearLayout) vChild).getChildCount();
                //Log.d(GlobalConfig.TAG+"tanya : total2 ",""+total2);
                for (int j = 0; j < ((LinearLayout) vChild).getChildCount(); j++) {
                    vChild2 = ((LinearLayout) vChild).getChildAt(j);
                    //linear layout level3
                    if (vChild2 instanceof LinearLayout) {
                        View vChild3 = null;
                        total3 = ((LinearLayout) vChild2).getChildCount();
                        //Log.d(GlobalConfig.TAG+"tanya : total3 ",""+total3);

                        //disini pada vchild2(1) lokasi text view pertanyaan dan jawab
                        View vchild_tanya = null;
                        vchild_tanya = ((LinearLayout) vChild2).getChildAt(1);

                        //tanya
                        vChild3 = ((LinearLayout) vchild_tanya).getChildAt(0);
                        tanya.add(((TextView) vChild3).getText().toString().trim());
                        Log.d(GlobalConfig.TAG+"tanya : ", "" + tanya.get(0));
                        //jawab
                        vChild3 = ((LinearLayout) vchild_tanya).getChildAt(1);
                        jawab.add(((TextView) vChild3).getText().toString().trim());
                        Log.d(GlobalConfig.TAG+"jawab : ", "" + jawab.get(0));
                    }
                }
            }
        }
        //untuk gambar
        List<String> img_path = new ArrayList<String>();
        img_path.clear();
        Log.d(GlobalConfig.TAG+"gambar : total ", "" + count);
        for (int i = 0; i < llGambar.getChildCount(); i++) {
            vChild = llGambar.getChildAt(i);
            if (vChild instanceof LinearLayout) {
                total1 = ((LinearLayout) vChild).getChildCount();
                Log.d(GlobalConfig.TAG+"view dalam : total ", "" + total1);

                //get child indeks ke 0 karena path berada pada awal
                View vChild2 = ((LinearLayout) vChild).getChildAt(0);
                if(vChild2 instanceof TextView){
                    Log.d(GlobalConfig.TAG+"img_path", "" + ((TextView) vChild2).getText().toString().trim());
                    img_path.add(((TextView) vChild2).getText().toString().trim());
                }
            }
        }

        //merubah gambar kedalam bentuk text
        //final List<String> img_string = new ArrayList<String>();
        final List<String> img_string_compress = new ArrayList<String>();
        //img_string.clear();
        img_string_compress.clear();
        for(int i=0;i<img_path.size();i++){
            Bitmap bmp_compress = decodeSampledBitmapFromFile(img_path.get(i), 1680, 960);   //gambar dikompres terlebih dahulu
            //Bitmap bmp          = BitmapFactory.decodeFile(img_path.get(i));               //untuk ukuran asli
            //img_string.add(encodeToBase64(bmp));
            img_string_compress.add(encodeToBase64(bmp_compress));
        }

        final String latlong = mLatLong.getText().toString().trim();

        Log.d(GlobalConfig.TAG+"kasus_id",kasus_id);
        Log.d(GlobalConfig.TAG+"nama",nama);
        Log.d(GlobalConfig.TAG+"lokasi",lokasi);
        Log.d(GlobalConfig.TAG+"alamat",alamat);
        for(int i=0;i<tanya.size();i++){
            Log.d(GlobalConfig.TAG+"tanya"+i,tanya.get(i));
            Log.d(GlobalConfig.TAG+"jawab"+i,jawab.get(i));
        }
        for(int i =0; i<img_string_compress.size();i++){
            Log.d(GlobalConfig.TAG+"gambar"+i,img_string_compress.get(i));
        }
        Log.d(GlobalConfig.TAG+"latlong :",latlong);

        //cek require form
        boolean success = true;

        if(llPertanyaan.getChildCount()<1){
            tv_warning_pertanyaan.setText("Minimal 1 pertanyaan!");
            tv_warning_pertanyaan.setTextColor(Color.RED);
            tv_warning_pertanyaan.setFocusable(true);
            success = false;
        }
        if(alamat.length()<=0){
            tv_warning_alamat.setVisibility(View.VISIBLE);
            et_alamat.setFocusable(true);
            img_alamat.setImageDrawable(ContextCompat.getDrawable(Interogasi.this, R.drawable.ic_error));
            success = false;
        }
        if(nama.length()<=0){
            tv_warning_nama.setVisibility(View.VISIBLE);
            et_nama.setFocusable(true);
            img_nama.setImageDrawable(ContextCompat.getDrawable(Interogasi.this, R.drawable.ic_error));
            success = false;
        }
        if(lokasi.length()<=0){
            tv_warning_lokasi.setVisibility(View.VISIBLE);
            et_lokasi.setFocusable(true);
            img_lokasi.setImageDrawable(ContextCompat.getDrawable(Interogasi.this, R.drawable.ic_error));
            success = false;
        }
        if(kasus_id.length()<=0){
            tv_warning_nama_kasus.setVisibility(View.VISIBLE);
            tv_kasus_nama.setFocusable(true);
            img_kasus_nama.setImageDrawable(ContextCompat.getDrawable(Interogasi.this, R.drawable.ic_error));
            success = false;
        }
        if(mLatLong.getText().toString().trim().length()<=0){
            tv_warning_koordinat.setText("Koordinat tidak boleh kosong.");
            tv_warning_koordinat.setTextColor(Color.RED);
            tv_warning_koordinat.setFocusable(true);
            success = false;
        }

        if(success){
            final ProgressDialog loading = new ProgressDialog(this);
            loading.setTitle("Mengirim berkas");
            loading.setMessage("Mohon tunggu...");
            loading.setCancelable(false);
            loading.setButton(DialogInterface.BUTTON_NEGATIVE, "Batal", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cancelRequest();
                    dialog.dismiss();
                }
            });
            loading.show();
            String url ="";
            if(pref.getString(GlobalConfig.IP_KEY, null) != null){
                url = "http://"+pref.getString(GlobalConfig.IP_KEY, "")+GlobalConfig.WEB_URL+CInterogasi.URL_UPLOAD_DOKUMEN;
            }else{
                url = "http://"+GlobalConfig.IP+GlobalConfig.WEB_URL+CInterogasi.URL_UPLOAD_DOKUMEN;
            }
            //upload dokumen
            JSONObject jsonBody;
            Log.d(GlobalConfig.TAG, url);
            try {
                jsonBody = new JSONObject();
                jsonBody.put(CInterogasi.USER_ID, ""+pref.getString(CLogin.G_ID_USER,""));
                jsonBody.put(CInterogasi.USER_REGID, ""+pref.getString(GlobalConfig.gcmregId,""));

                jsonBody.put(CInterogasi.KEYUP_KASUS_ID, kasus_id);
                jsonBody.put(CInterogasi.KEYUP_TKP, lokasi);
                jsonBody.put(CInterogasi.KEYUP_NAMA, ""+nama);
                jsonBody.put(CInterogasi.KEYUP_ALAMAT, ""+alamat);

                final JSONArray json_tanya    = new JSONArray();
                final JSONArray json_jawab    = new JSONArray();
                for(int i=0; i<tanya.size();i++){
                    json_tanya.put(i, ""+tanya.get(i));
                    json_jawab.put(i, ""+jawab.get(i));
                }

                jsonBody.put(CInterogasi.KEYUP_TANYA, json_tanya);
                jsonBody.put(CInterogasi.KEYUP_JAWAB, json_jawab);
                //final JSONArray json_gambar         = new JSONArray();
                final JSONArray json_gambar_compress= new JSONArray();
                for(int i=0; i<img_string_compress.size();i++){
                    //json_gambar.put(i, ""+img_string.get(i));
                    json_gambar_compress.put(i, ""+img_string_compress.get(i));
                }
                jsonBody.put(CInterogasi.KEYUP_GAMBAR, json_gambar_compress);
                jsonBody.put(CInterogasi.KEYUP_KOORDINAT, ""+latlong);

                final JSONArray img_path_json = new JSONArray();
                for(int i=0; i<img_path.size();i++){
                    img_path_json.put(i, img_path.get(i));
                }
                Log.d(GlobalConfig.TAG, jsonBody.toString());
                request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            loading.dismiss();
                            int status  = response.getInt("status");
                            String pesan= response.getString("pesan");
                            Log.d(GlobalConfig.TAG+" result ", ""+response.toString());
                            if(status==1){
                                //buka halaman notifikasi interogasi
                                Intent notif = new Intent(Interogasi.this, InterogasiNotif.class);
                                notif.putExtra(CInterogasi.KEYUP_KASUS_NAMA, tv_kasus_nama.getText().toString().trim());
                                notif.putExtra(CInterogasi.KEYUP_TKP, lokasi);
                                notif.putExtra(CInterogasi.KEYUP_NAMA, nama);
                                notif.putExtra(CInterogasi.KEYUP_ALAMAT, alamat);
                                notif.putExtra(CInterogasi.KEYUP_TANYA, json_tanya.toString());
                                notif.putExtra(CInterogasi.KEYUP_JAWAB, json_jawab.toString());
                                notif.putExtra(CInterogasi.KEYUP_KOORDINAT, latlong);
                                notif.putExtra(CInterogasi.KEYUP_GAMBAR, img_path_json.toString());

                                startActivity(notif);
                                finish();

                                //JSONArray kasuss = response.getJSONArray("data");
                            }else{
                                Toast.makeText(getApplicationContext(),pesan , Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            loading.dismiss();
                            e.printStackTrace();
                        }
                        //Log.d("respons",response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // do something
                        loading.dismiss();
                        Log.d("respons",error.getMessage().toString());
                    }
                }){
                    public Map<String, String> getHeaders() {
                        Map<String,String> headers = new Hashtable<String, String>();

                        //Adding parameters
                        headers.put(GlobalConfig.APP_NAME, GlobalConfig.APP_ID);
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }};

                request.setRetryPolicy(new DefaultRetryPolicy(
                        GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                //Adding request to the queue
                // Adding request to request queue
                MyAppController.getInstance().addToRequestQueue(request);

            } catch (JSONException e) {
                loading.dismiss();
                e.printStackTrace();
            }
        }else{
            View parentLayout = findViewById(R.id.parentlayout);
            Snackbar snack = Snackbar.make(parentLayout, "Isi semua form", Snackbar.LENGTH_LONG);
            View view = snack.getView();
//            FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
//            params.gravity = Gravity.TOP;
//            view.setLayoutParams(params);
            snack.setAction("CLOSE", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    })
                    .setActionTextColor(getResources().getColor(android.R.color.white ))
                    .show();
        }
        btnSimpan.setEnabled(true);
    }
    public String encodeToBase64(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
    public static Bitmap decodeBase64(String input)
    {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }
    @Override
    public void onBackPressed() {
        // Write your code here
        Log.d(GlobalConfig.TAG,llPertanyaan.getChildCount()+"|"+llGambar.getChildCount());
        if(llPertanyaan.getChildCount()>0 || et_alamat.getText().toString().trim().length()>0 || et_lokasi.getText().toString().trim().length()>0
                || et_nama.getText().toString().trim().length()>0 || !tv_kasus_nama.getText().toString().trim().equals("Cari Kasus")
                || llGambar.getChildCount()>0 || mLatLong.getText().toString().length()>0){
            showdialogBackPress();
        }else{
            closeActivity();
        }
    }
    public void closeActivity(){
        super.onBackPressed();
    }
    //untuk menampilkan notifikasi keluar
    private void showdialogBackPress(){
        LayoutInflater layoutInflater = (LayoutInflater)getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.notif_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Interogasi.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title  = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final TextView t_text   = (TextView) promptView.findViewById(R.id.t_text_dialog);
        t_title.setText("Peringatan");
        t_text.setText("Data belum tersimpan!.\nApakah anda yakin tidak menyimpannya?");
        // setup a dialog window
        alertDialogBuilder.setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        closeActivity();
                    }
                })
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    //untuk mendapatakan lokasi longitude latitude
    public void getLocation() {
        // Get the location manager
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(bestProvider);
        Double lat,lon;
        try {
            lat = location.getLatitude ();
            lon = location.getLongitude ();
            Toast.makeText(getApplicationContext(), lat+" "+lon,Toast.LENGTH_SHORT).show();
            //return new LatLng(lat, lon);
        }
        catch (NullPointerException e){
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), " "+e.getMessage(),Toast.LENGTH_SHORT).show();
            //return null;
        }
    }
    //untuk membuka intent mengambil foto dari kamera
    private void loadImagefromKamera() {
        createFolder();
        pictureImagePath = getNewImagePath();
        File file = new File(pictureImagePath);
        myScanFile(this.pictureImagePath);
        Uri outputFileUri = Uri.fromFile(file);
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        Log.d(GlobalConfig.TAG+"mycop",""+pictureImagePath);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
    //untuk membuka intent mengambil foto dari gallery
    public void loadImagefromGallery() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, REQUEST_IMAGE_GALLERY);
    }

    //membuat nama file baru
    private String getNewImagePath() {
        String folder = Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_DCIM + File.separator + GlobalConfig.FOLDER_NAMA;
        pictureImagePath = new File(folder).getAbsolutePath() + File.separator + (new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".jpg");
        File file = new File(pictureImagePath);
        myScanFile(this.pictureImagePath);
        return this.pictureImagePath;
    }
    //membuat folder cop
    private void createFolder() {
        String folder = Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_DCIM + File.separator + GlobalConfig.FOLDER_NAMA;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File directory = new File(folder);
            if (!directory.exists()) {
                directory.mkdirs();
                Log.d(GlobalConfig.TAG+"mycopcamera", "" + directory.toString());
                return;
            }
            return;
        }
    }
    //memasukan file kedalam gallery
    private void myScanFile(String path) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.DATA, path);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        getApplication().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }
    public void showdialogCariKasus() {           //untuk mencari kasus
        LayoutInflater layoutInflater = (LayoutInflater) getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.dialog_cari_kasus, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Interogasi.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title  = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final RadioGroup jenis  = (RadioGroup)promptView.findViewById(R.id.radiojenis);
        final EditText t_cari   = (EditText) promptView.findViewById(R.id.et_ip);

        final RadioButton rb_no_lp  = (RadioButton)promptView.findViewById(R.id.radio_nomor_lp);
        final RadioButton rb_nama_pelapor  = (RadioButton)promptView.findViewById(R.id.radio_nama_pelapor);

        t_title.setText("Cari Kasus");

        // setup a dialog window
        alertDialogBuilder.setCancelable(true)
                .setPositiveButton("Cari",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                int selectedId  = jenis.getCheckedRadioButtonId();
                                String sjenis   ="";       //untuk menyimpan jenis pencarian

                                if(selectedId==rb_no_lp.getId()){
                                    sjenis = CInterogasi.NO_LP;
                                }else
                                    sjenis = CInterogasi.NAMA_PELAPOR;

                                Intent iCariKasus = new Intent(Interogasi.this,CariKasus.class);
                                iCariKasus.putExtra(CInterogasi.KEYWORD_JENIS,sjenis);
                                iCariKasus.putExtra(CInterogasi.KEYWORD, t_cari.getText().toString());
                                startActivityForResult(iCariKasus, CInterogasi.KODE_BERKAS1_CARIKASUS);
                            }
                        })
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        //jika ingin menghapus kasus
        if(!tv_kasus_nama.getText().toString().trim().equals("Cari Kasus")) {
            alertDialogBuilder.setNeutralButton("Hapus Kasus",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            tv_kasus_id.setText("");

                            tv_kasus_nama.setText("Cari Kasus");
                            tv_kasus_nama.setTextColor(Color.RED);
                            img_kasus_nama.setImageDrawable(ContextCompat.getDrawable(Interogasi.this, R.drawable.ic_error));
                        }
                    });
        }
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CInterogasi.KODE_BERKAS1_CARIKASUS && resultCode == RESULT_OK && data != null) {
            String kasus_nama = data.getStringExtra(CInterogasi.KASUS_NAMA);
            String kasus_id = data.getStringExtra(CInterogasi.KASUS_ID);

            tv_kasus_id.setText(""+kasus_id);
            tv_kasus_nama.setText(""+kasus_nama);

            img_kasus_nama.setImageDrawable(ContextCompat.getDrawable(Interogasi.this, R.drawable.ic_done));
            tv_warning_nama_kasus.setVisibility(View.GONE);
        }else if (requestCode == CInterogasi.KODE_BERKAS1_CARILOKASI && resultCode == RESULT_OK && data != null) {
            float longitude = (float) Math.round(data.getDoubleExtra(CInterogasi.LONGITUDE,0) * 1000000) / 1000000;
            float latitude = (float) Math.round(data.getDoubleExtra(CInterogasi.LATITUDE,0) * 1000000) / 1000000;

            mLatLongLabel.setText(latitude+";"+longitude);
            mLatLong.setText(latitude+";"+longitude);
            ll_koordinat.setVisibility(View.VISIBLE);
            tv_warning_koordinat.setText("Temukan lokasi dengan koordinat.");
            tv_warning_koordinat.setTextColor(Color.GRAY);
            tv_warning_koordinat.setFocusable(true);

            Log.d(GlobalConfig.TAG+"lokasiku", longitude+";"+latitude);
        }
        //untuk menangkap image dari kamera
        else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bitmap bitmapForImage = null;
            File file = new File(pictureImagePath);
            Log.d(GlobalConfig.TAG+"mycopcamera", pictureImagePath);
            if (file.exists()) {
                bitmapForImage = decodeSampledBitmapFromFile(pictureImagePath, 300, 300);
            }
            add_layout_gambar(bitmapForImage, pictureImagePath);
        }
        //untuk menagkap image dari gallery
        else if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK&& null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imgDecodableString = cursor.getString(columnIndex);

            String lokasi_baru = getNewImagePath();
            try {
                File source = new File(imgDecodableString);
                File destination = new File(lokasi_baru);
                if (source.exists()) {
                    FileChannel src = new FileInputStream(source).getChannel();
                    FileChannel dst = new FileOutputStream(destination).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            } catch (Exception e) {
                Log.d(GlobalConfig.TAG+"COPerror", "" + e.getMessage());
            }
            Bitmap bitmapForImage = decodeSampledBitmapFromFile(lokasi_baru, 300, 300);
            cursor.close();
            add_layout_gambar(bitmapForImage, lokasi_baru);
        }else if(requestCode == CInterogasi.KODE_ZOOM_IMAGE && resultCode == RESULT_OK&& null != data){
            if (data.getIntExtra(CInterogasi.KODE_ZOOM_STATUS, CInterogasi.KODE_ZOOM_HAPUS) == CInterogasi.KODE_ZOOM_HAPUS) {
                hapus_gambar(data.getIntExtra(CInterogasi.IMG_VIEW_ID, 0));
            }
        }
    }
    //untuk menghapus gambar
    private void hapus_gambar(int view_id) {
        Log.d(GlobalConfig.TAG + "mycop", "view_id " + view_id);
        for (int i = 0; i < this.llGambar.getChildCount(); i++) {
            View view = llGambar.getChildAt(i);
            if(view instanceof LinearLayout){
                View vChild2 = ((LinearLayout) view).getChildAt(1); //index 1 karena berda pada posisi ke dua
                Log.d(getLocalClassName(), view_id+"get id : "+vChild2.getId());
                if(vChild2 instanceof ImageView && view_id==vChild2.getId()){
                    Log.d(GlobalConfig.TAG + "mycop", vChild2.getId()+"hapus view " + view_id);
                    if (view instanceof LinearLayout) {
                        ((ViewGroup) view.getParent()).removeView(view);
                        update_keterangan();
                    }
                }

            }
//            Log.d(GlobalConfig.TAG + "mycop", "target view " + view.getId());
//            if (view.getId() == view_id) {
//                Log.d(GlobalConfig.TAG + "mycop", "hapus view " + view_id);
//                if (view instanceof LinearLayout) {
//                    ((ViewGroup) view.getParent()).removeView(view);
//                    update_keterangan();
//                }
//            }
        }
//        vChild = llGambar.getChildAt(i);
//        if (vChild instanceof LinearLayout) {
//            total1 = ((LinearLayout) vChild).getChildCount();
//            Log.d(GlobalConfig.TAG+"view dalam : total ", "" + total1);
//
//            //get child indeks ke 0 karena path berada pada awal
//            View vChild2 = ((LinearLayout) vChild).getChildAt(0);
//            if(vChild2 instanceof TextView){
//                Log.d(GlobalConfig.TAG+"img_path", "" + ((TextView) vChild2).getText().toString().trim());
//                img_path.add(((TextView) vChild2).getText().toString().trim());
//            }
//        }

    }
    //memperkecil ukuran gambar
    public static Bitmap decodeSampledBitmapFromFile(String imagePath, int reqWidth, int reqHeight) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(imagePath, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        int min = width;
        if (width > height) {
            int temp = reqHeight;
            reqHeight = reqWidth;
            reqWidth = temp;
        }
        Log.d(GlobalConfig.TAG+"copcamera1/", "height : " + height + ", width : " + width);
        if (height > reqHeight || width > reqWidth) {
            int halfHeight = height;
            int halfWidth = width;
            while (true) {
                if (halfHeight / inSampleSize <= reqHeight && halfWidth / inSampleSize <= reqWidth) {
                    break;
                }
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }
    public void add_layout_gambar(Bitmap imageBitmap, final String path){
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        final View inflatedLayout= inflater.inflate(R.layout.view_gambar, llGambar, false);

        final ImageView img_berkas  = (ImageView) inflatedLayout.findViewById(R.id.img_berkas);
        final TextView img_path     = (TextView) inflatedLayout.findViewById(R.id.img_path);

        img_berkas.setImageBitmap(imageBitmap);
        img_berkas.setClickable(true);
        img_berkas.setId(id_gambar);
        id_gambar++;
        img_path.setText(path);

        img_berkas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImage(v, path, (llGambar.getChildCount() - 1));
            }
        });

        llGambar.addView(inflatedLayout);
        update_keterangan();
    }
    public void update_keterangan(){
        int count = llGambar.getChildCount();
        String keterangan="";
        if(count>0){
            keterangan = "Ditambahkan "+count+" dari "+MAX_FOTO+" foto";
        }else{
            keterangan = "Pilih "+MAX_FOTO+" foto";
        }
        tv_keterangan_gambar.setText(keterangan);
    }
    public void showImage(View v, String path, int idChild) {
        Intent img_fullscreen = new Intent(getApplicationContext(), MainZoomImage.class);
        img_fullscreen.putExtra(CInterogasi.IMG_VIEW_ID, v.getId());
        img_fullscreen.putExtra(CInterogasi.IMG_PATH, path);
        img_fullscreen.putExtra(CInterogasi.IMG_IDCHILD, idChild);
        Log.d(GlobalConfig.TAG+"copcamera0", path);
        startActivityForResult(img_fullscreen, CInterogasi.KODE_ZOOM_IMAGE);
    }
    public void showdialogFoto(){
        // get prompts.xml view
        LayoutInflater layoutInflater = (LayoutInflater)getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.dialog_pilih_foto, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Interogasi.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title  = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final LinearLayout t_kamera   = (LinearLayout) promptView.findViewById(R.id.t_kamera);
        final LinearLayout t_galeri   = (LinearLayout) promptView.findViewById(R.id.t_galeri);

        t_title.setText("Pilih Foto");


        final AlertDialog alert = alertDialogBuilder.create();

        t_kamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadImagefromKamera();
                alert.cancel();
            }
        });
        t_galeri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadImagefromGallery();
                alert.cancel();
            }
        });
        alert.show();
    }
    //untuk menambah pertanyaan
    public void showdialogTambahPertanyaan() {
        LayoutInflater layoutInflater = (LayoutInflater) getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.dialog_pertanyaan, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Interogasi.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final EditText et_pertanyaan= (EditText) promptView.findViewById(R.id.et_dialog_pertanyaan);
        final EditText et_jawaban   = (EditText) promptView.findViewById(R.id.et_dialog_jawaban);

        t_title.setText("Pertanyaan");

        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Simpan",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String s_jawab  = et_jawaban.getText().toString().trim();
                                String s_tanya  = et_pertanyaan.getText().toString().trim();
                                if (!s_jawab.trim().equals("") && !s_tanya.trim().equals("")) {
                                    //menambahkan ke layout
                                    add_layout_pertanyaan(s_tanya, s_jawab);
                                }else{
                                    Toast.makeText(Interogasi.this, "Pertanyaan dan jawaban tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        ;
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    public void add_layout_pertanyaan(final String tanya, final String jawab){
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        final View inflatedLayout= inflater.inflate(R.layout.view_pertanyaan, llPertanyaan, false);

        final TextView t_tanya  = (TextView) inflatedLayout.findViewById(R.id.et_pertanyaan);
        final TextView t_jawab  = (TextView) inflatedLayout.findViewById(R.id.et_jawaban);

        final ImageButton btnDelete = (ImageButton) inflatedLayout.findViewById(R.id.img_hapus);
        final ImageButton btnEdit   = (ImageButton) inflatedLayout.findViewById(R.id.img_edit);

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showdialogHapusPertanyaan(tanya, inflatedLayout);
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showdialogEditPertanyaan(tanya,t_tanya,jawab,t_jawab);
                //Toast.makeText(Interogasi.this, "Edit : "+tanya, Toast.LENGTH_SHORT).show();
            }
        });

        t_tanya.setText(tanya);
        t_jawab.setText(jawab);

        llPertanyaan.addView(inflatedLayout);
        if(llPertanyaan.getChildCount()<=0){
            tv_warning_pertanyaan.setTextColor(Color.RED);
        }else{
            tv_warning_pertanyaan.setText("Minimal 1 pertanyaan dan jawaban");
            tv_warning_pertanyaan.setTextColor(Color.GRAY);
        }
    }
    public void showdialogEditPertanyaan(String tanya, final TextView t_tanya, String jawab, final TextView t_jawab) {
        LayoutInflater layoutInflater = (LayoutInflater) getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.dialog_pertanyaan, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Interogasi.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final EditText et_pertanyaan= (EditText) promptView.findViewById(R.id.et_dialog_pertanyaan);
        final EditText et_jawaban   = (EditText) promptView.findViewById(R.id.et_dialog_jawaban);

        t_title.setText("Pertanyaan");
        et_pertanyaan.setText(tanya);
        et_jawaban.setText(jawab);

        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Simpan",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String s_jawab  = et_jawaban.getText().toString().trim();
                                String s_tanya  = et_pertanyaan.getText().toString().trim();
                                if (!s_jawab.trim().equals("") && !s_tanya.trim().equals("")) {
                                    //mengganti ke layout
                                    t_tanya.setText(s_tanya);
                                    t_jawab.setText(s_jawab);
                                }else{
                                    Toast.makeText(Interogasi.this, "Pertanyaan dan jawaban tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    private void showdialogHapusPertanyaan(String text, final View view){            //untuk menghapus pertanyaan pada view
        // get prompts.xml view
        LayoutInflater layoutInflater = (LayoutInflater)getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.notif_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Interogasi.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title  = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final TextView t_text   = (TextView) promptView.findViewById(R.id.t_text_dialog);

        //untuk membuat ... jika lebih dari 25
        if(text.length() >= 25){
            text = text.substring(0,25)+"...";
        }
        t_title.setText("Hapus Pertanyaan");
        t_text.setText("Apakah anda yakin menghapus pertanyaan \""+text+"\" ?");
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //hapus view pertanyaan

                        ((ViewGroup) view.getParent()).removeView(view);
                    }
                })
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
