package com.imamudin.cop;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.imamudin.cop.app.MyAppController;
import com.imamudin.cop.berita_acara.ObservasiNotif;
import com.imamudin.cop.config.CLogin;
import com.imamudin.cop.config.CObservasi;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.location.CariLokasi;
import com.imamudin.cop.mysp.ObscuredSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

/**
 * Created by imamudin on 22/11/16.
 */
public class FragmentKejadianMenonjol extends android.support.v4.app.Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    LinearLayout ll_main;
    EditText et_nama_kejadian, et_alamat, et_nama_pelaku, et_nama_korban, et_jumlah_kerugian;
    EditText et_saksi, et_keterangan;
    TextView tv_koordinat, tv_notif_nama_kejadian, tv_notif_alamat, tv_warning_koordinat;
    Button btn_koordinat, btn_simpan;
    TextView mLatLongLabel;
    TextView mLatLong;
    LinearLayout ll_koordinat;
    ImageView img_alamat, img_nama_kejadian;

    private OnFragmentInteractionListener mListener;
    public JsonObjectRequest request =null;
    ObscuredSharedPreferences pref;

    public FragmentKejadianMenonjol() {

    }
    public static FragmentKejadianMenonjol newInstance(String param1, String param2) {
        FragmentKejadianMenonjol fragment = new FragmentKejadianMenonjol();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        ((MainActivityOpsnal) getActivity()).setActionBarTitle("Kejadian Menonjol");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =inflater.inflate(R.layout.fragment_kejadian_menonjol,container,false);

        ll_main = (LinearLayout)v.findViewById(R.id.ll_main);
        et_nama_kejadian    = (EditText) v.findViewById(R.id.et_nama_kejadian);
        et_alamat           = (EditText) v.findViewById(R.id.et_lokasi);
        et_nama_pelaku      = (EditText) v.findViewById(R.id.et_pelaku);
        et_nama_korban      = (EditText) v.findViewById(R.id.et_nama_korban);
        et_jumlah_kerugian  = (EditText) v.findViewById(R.id.et_jumlah_kerugian);
        et_saksi            = (EditText) v.findViewById(R.id.et_saksi);
        et_keterangan       = (EditText) v.findViewById(R.id.et_keterangan);
        btn_koordinat       = (Button) v.findViewById(R.id.btnLokasi);
        btn_simpan          = (Button) v.findViewById(R.id.btn_simpan);
        tv_koordinat        = (TextView) v.findViewById(R.id.tv_latlong);

        img_alamat          = (ImageView) v.findViewById(R.id.img_alamat);
        img_nama_kejadian   = (ImageView) v.findViewById(R.id.img_nama_kejadian);

        tv_notif_alamat     = (TextView) v.findViewById(R.id.tv_notif_alamat);
        tv_notif_nama_kejadian= (TextView) v.findViewById(R.id.tv_notif_nama_kejadian);
        tv_warning_koordinat= (TextView)v.findViewById(R.id.tv_notif_koordinate);

        mLatLongLabel       = (TextView)v.findViewById(R.id.tv_latlong_label);
        mLatLong            = (TextView)v.findViewById(R.id.tv_latlong);
        ll_koordinat        = (LinearLayout)v.findViewById(R.id.ll_koordinat);

        et_nama_kejadian.setOnFocusChangeListener(NoFocus);
        et_alamat.setOnFocusChangeListener(NoFocus);

        btn_koordinat.setOnClickListener(onClick);
        btn_simpan.setOnClickListener(onClick);

        et_nama_kejadian.setText("");
        et_alamat.setText("");
        et_nama_pelaku.setText("");
        et_nama_korban.setText("");
        et_jumlah_kerugian.setText("");
        et_saksi.setText("");
        et_keterangan.setText("");

        pref = new ObscuredSharedPreferences(getActivity(),
                getActivity().getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        return v;
    }

    EditText.OnFocusChangeListener NoFocus= new View.OnFocusChangeListener(){
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(v==et_nama_kejadian){
                if(!hasFocus){
                    showMessage(et_nama_kejadian, img_nama_kejadian, tv_notif_nama_kejadian);
                }
            }else if(v==et_alamat){
                if(!hasFocus){
                    showMessage(et_alamat, img_alamat, tv_notif_alamat);
                }
            }
        }
    };
    public void showMessage(final EditText et, final ImageView iv, final TextView tv){
        if(et.getText().toString().trim().length()<=0){
            iv.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_error));
            tv.setVisibility(View.VISIBLE);
        }else{
            iv.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_done));
            tv.setVisibility(View.GONE);
        }
    }

    View.OnClickListener  onClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view ==  btn_koordinat){
                Intent cariLokasi = new Intent(getActivity(), CariLokasi.class);
                startActivityForResult(cariLokasi, CObservasi.KODE_BERKAS1_CARILOKASI);
            }if(view == btn_simpan){
                simpan_kejadian();
            }
        }
    };
    private void simpan_kejadian(){
        boolean success     = true;

        final String s_nama_kejadian= et_nama_kejadian.getText().toString().trim();
        final String s_alamat       = et_alamat.getText().toString().trim();
        final String s_nama_pelaku  = et_nama_pelaku.getText().toString().trim();
        final String s_nama_korban  = et_nama_korban.getText().toString().trim();
        final String s_kerugian     = et_jumlah_kerugian.getText().toString().trim();
        final String s_saksi        = et_saksi.getText().toString().trim();
        final String s_keterangan   = et_keterangan.getText().toString().trim();
        final String latlong        = mLatLong.getText().toString().trim();

        if(s_nama_kejadian.equals("")){
            showMessage(et_nama_kejadian, img_nama_kejadian, tv_notif_nama_kejadian);
            success = false;
        }
        if(s_alamat.equals("")){
            showMessage(et_alamat, img_alamat, tv_notif_alamat);
            success = false;
        }
        if(mLatLong.getText().toString().trim().equals("")){
            tv_warning_koordinat.setText("Koordinat tidak boleh kosong.");
            tv_warning_koordinat.setTextColor(Color.RED);
            tv_warning_koordinat.setFocusable(true);

            success = false;
        }

        if(success){
            final ProgressDialog loading = new ProgressDialog(getActivity());
            loading.setTitle("Mengirim berkas");
            loading.setMessage("Mohon tunggu...");
            loading.setCancelable(false);
            loading.setButton(DialogInterface.BUTTON_NEGATIVE, "Batal", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cancelRequest();
                    dialog.dismiss();
                }
            });
            loading.show();
            String url ="";
            if(pref.getString(GlobalConfig.IP_KEY, null) != null){
                url = "http://"+pref.getString(GlobalConfig.IP_KEY, "")+GlobalConfig.WEB_URL+GlobalConfig.URL_UPLOAD_KEJADIAN_BARU;
            }else{
                url = "http://"+GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_UPLOAD_KEJADIAN_BARU;
            }
            //upload dokumen
            JSONObject jsonBody;
            Log.d(GlobalConfig.TAG, url);
            try {
                jsonBody = new JSONObject();
                jsonBody.put(CObservasi.USER_ID, ""+pref.getString(CLogin.G_ID_USER,""));
                jsonBody.put(CObservasi.USER_REGID, ""+pref.getString(GlobalConfig.gcmregId,""));

                jsonBody.put(GlobalConfig.UP_NAMA_KEJADIAN, s_nama_kejadian);
                jsonBody.put(GlobalConfig.UP_ALAMAT, s_alamat);
                jsonBody.put(GlobalConfig.UP_NAMA_PELAKU, s_nama_pelaku);
                jsonBody.put(GlobalConfig.UP_NAMA_KORBAN, s_nama_korban);
                jsonBody.put(GlobalConfig.UP_KERUGIAN, s_kerugian);
                jsonBody.put(GlobalConfig.UP_SAKSI, s_saksi);
                jsonBody.put(GlobalConfig.UP_KETERANGAN, s_keterangan);
                jsonBody.put(GlobalConfig.UP_KOORDINAT, latlong);

                Log.d(GlobalConfig.TAG, jsonBody.toString());
                request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            loading.dismiss();
                            int status  = response.getInt("status");
                            String pesan= response.getString("pesan");
                            Log.d(GlobalConfig.TAG+" result ", ""+response.toString());
                            if(status==1){
                                //buka halaman notifikasi Kejadian Terkirim
                                Intent notif = new Intent(getActivity(), Notifikasi.class);
                                notif.putExtra("message", "Kejadian Menonjol berhasil disimpan.");
                                startActivityForResult(notif, GlobalConfig.KODE_INTENT_NOTIFIKASI);
                            }else{
                                notifikasi(pesan);
                            }
                        } catch (JSONException e) {
                            loading.dismiss();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // do something
                        loading.dismiss();
                        notifikasi(GlobalConfig.MSG_KESALAHAN);
                        //Log.d("respons",error.getMessage().toString());
                    }
                }){
                    public Map<String, String> getHeaders() {
                        Map<String,String> headers = new Hashtable<String, String>();

                        //Adding parameters
                        headers.put(GlobalConfig.APP_NAME, GlobalConfig.APP_ID);
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }};

                request.setRetryPolicy(new DefaultRetryPolicy(
                        GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                //Adding request to the queue
                // Adding request to request queue
                MyAppController.getInstance().addToRequestQueue(request);

            } catch (JSONException e) {
                loading.dismiss();
                e.printStackTrace();
            }
        }else{
            notifikasi("Isi form yang dibutuhkan.");
        }

    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Log.d(getTag(),"menerima intent baru");
        if (requestCode == CObservasi.KODE_BERKAS1_CARILOKASI && data != null) {
            float longitude = (float) Math.round(data.getDoubleExtra(CObservasi.LONGITUDE, 0) * 1000000) / 1000000;
            float latitude = (float) Math.round(data.getDoubleExtra(CObservasi.LATITUDE, 0) * 1000000) / 1000000;

            mLatLongLabel.setText(latitude + ";" + longitude);
            mLatLong.setText(latitude + ";" + longitude);
            ll_koordinat.setVisibility(View.VISIBLE);
            tv_warning_koordinat.setText("Temukan lokasi dengan koordinat.");
            tv_warning_koordinat.setTextColor(Color.GRAY);
            tv_warning_koordinat.setFocusable(true);

            Log.d(GlobalConfig.TAG + "lokasiku", longitude + ";" + latitude);
        }else if (requestCode == GlobalConfig.KODE_INTENT_NOTIFIKASI){
            Log.d(GlobalConfig.TAG,"menerima intent  notifikasi");

            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = this.getClass();
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.detach(this).attach(this);
            ft.replace(R.id.flContent, fragment);
            ft.commit();

            et_nama_kejadian.setText("");
            et_alamat.setText("");
            et_nama_pelaku.setText("");
            et_nama_korban.setText("");
            et_jumlah_kerugian.setText("");
            et_saksi.setText("");
            et_keterangan.setText("");
        }
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        cancelRequest();
    }
    @Override
    public void onResume() {
        super.onResume();
//        et_nama_kejadian.setText("");
//        et_alamat.setText("");
//        et_nama_pelaku.setText("");
//        et_nama_korban.setText("");
//        et_jumlah_kerugian.setText("");
//        et_saksi.setText("");
//        et_keterangan.setText("");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
