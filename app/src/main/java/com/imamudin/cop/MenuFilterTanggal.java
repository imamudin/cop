package com.imamudin.cop;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.imamudin.cop.config.CMonitor;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.mysp.ObscuredSharedPreferences;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by imamudin on 19/11/16.
 */
public class MenuFilterTanggal extends AppCompatActivity {
    LinearLayout ll_main;
    boolean firt_time;

    EditText et_tgl_awal, et_tgl_akhir;
    Calendar c_awal, c_akhir;

    ObscuredSharedPreferences pref;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tanggal_berkas);

        init();
    }
    private void init(){
        firt_time = true;
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Pilih");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        ll_main     = (LinearLayout)findViewById(R.id.ll_main);
        et_tgl_akhir= (EditText)findViewById(R.id.et_tgl_akhir);
        et_tgl_awal = (EditText)findViewById(R.id.et_tgl_mulai);

        et_tgl_akhir.setOnClickListener(myListen);
        et_tgl_awal.setOnClickListener(myListen);
//        if(!pref.getString(CMonitor.TGL_MULAI,"").equals("") && !pref.getString(CMonitor.TGL_AKHIR,"").equals("")){
//            et_tgl_awal.setText(rubahFormatTanggal("yyyy-MM-dd",pref.getString(CMonitor.TGL_MULAI,""), "dd MMM yyyy"));
//            et_tgl_akhir.setText(rubahFormatTanggal("yyyy-MM-dd",pref.getString(CMonitor.TGL_AKHIR,""), "dd MMM yyyy"));
//        }
    }
    View.OnClickListener myListen   = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            if(v == et_tgl_awal){
                dateTimePick(et_tgl_awal);
            }else if (v == et_tgl_akhir){
                dateTimePick(et_tgl_akhir);
            }
        }
    };
    private void dateTimePick(final EditText editText){
        final View dialogView = View.inflate(this, R.layout.date_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        final DatePicker dp_awal = (DatePicker) dialogView.findViewById(R.id.date_picker);
        dp_awal.setMaxDate(System.currentTimeMillis());
        if(editText==et_tgl_awal){
            dp_awal.setMinDate(0);
        }else {
            if (!et_tgl_awal.getText().toString().trim().equals("")) {
                String mytime = et_tgl_awal.getText().toString().trim();
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "dd MMM yyyy");
                Date myDate = null;
                try {
                    myDate = dateFormat.parse(mytime);
                    dp_awal.setMinDate(myDate.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = new GregorianCalendar(dp_awal.getYear(),
                        dp_awal.getMonth(),
                        dp_awal.getDayOfMonth());

                if(editText==et_tgl_awal){
                    c_awal = cal;
                }else{
                    c_akhir = cal;
                }

                SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
                Log.d(GlobalConfig.TAG, format.format(cal.getTime()));
                editText.setText(format.format(cal.getTime()));
                alertDialog.dismiss();
            }});
        alertDialog.setView(dialogView);
        alertDialog.show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(1, 1, 1, "Simpan").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    private Date stringToDate(String format, String tgl){
        String mytime = tgl;
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                format);
        Date myDate = null;
        try {
            myDate = dateFormat.parse(mytime);
            return myDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                //mengecek apabila tanggal awal lebih besar dari akhir
                if(et_tgl_awal.getText().toString().trim().equals("") || et_tgl_akhir.getText().toString().trim().equals("")){
                    notifikasi("Masukan tanggal mulai dan akhir.");
                }else {
                    if (c_awal.getTimeInMillis() > c_akhir.getTimeInMillis()) {
                        notifikasi("Tanggal mulai harus lebih rendah.");
                    } else {
                        Intent output = new Intent();
                        output.putExtra(CMonitor.TGL_MULAI, et_tgl_awal.getText().toString());
                        output.putExtra(CMonitor.TGL_AKHIR, et_tgl_akhir.getText().toString());
                        setResult(RESULT_OK, output);
                        finish();
                    }
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
}