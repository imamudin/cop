package com.imamudin.cop.listAdapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.imamudin.cop.R;
import com.imamudin.cop.app.MyAppController;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.model.MonitoringItem;
import com.imamudin.cop.model.NotifikasiItem;

import org.apache.commons.lang3.text.WordUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by agung on 13/04/2016.
 */
public class ListAdapterMonitor extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<MonitoringItem> monitoringItems;
    ImageLoader imageLoader = MyAppController.getInstance().getImageLoader();

    public ListAdapterMonitor(Activity activity, List<MonitoringItem> MonitiItems) {
        this.activity = activity;
        this.monitoringItems = MonitiItems;
    }

    @Override
    public int getCount() {
        return monitoringItems.size();
    }

    @Override
    public Object getItem(int location) {
        return monitoringItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_monitor, null);

        TextView title      = (TextView) convertView.findViewById(R.id.tv_monitor_title);
        TextView waktu      = (TextView) convertView.findViewById(R.id.tv_monitor_waktu);
        TextView no_lp      = (TextView) convertView.findViewById(R.id.tv_no_lp);
        TextView pelapor    = (TextView) convertView.findViewById(R.id.tv_nama_pelapor);
        ImageView img_doc   = (ImageView) convertView.findViewById(R.id.img_monitor);

        // getting billionaires data for the row
        MonitoringItem n = monitoringItems.get(position);

        //untuk waktu
        String mytime=n.getTgl_ba();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(mytime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("dd MMM yyyy");
        String finalDate = timeFormat.format(myDate);

        waktu.setText(finalDate);

        String nama_kasus = n.getNama_kasus().toLowerCase(); // Make all lowercase if you have caps

        String n_title = "<b>"+n.getNama_penyidik()+"</b> menambahkan berkas <b>"+ GlobalConfig.namaDokumen.get(n.getId_doc_opsnal())+"</b> " +
                "pada kasus <b><i>"+ WordUtils.capitalize(nama_kasus)+"</i></b>";
        title.setText(Html.fromHtml(n_title));

        int img = R.drawable.ic_surveillance;
        if(GlobalConfig.imgDokumen.get(n.getId_doc_opsnal())!=null){
            img = GlobalConfig.imgDokumen.get(n.getId_doc_opsnal());
        }
        img_doc.setImageDrawable(activity.getResources().getDrawable(img));
        no_lp.setText(n.getNo_lp());
        pelapor.setText(n.getNama_pelapor());

        return convertView;
    }

}
