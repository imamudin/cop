package com.imamudin.cop.listAdapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.imamudin.cop.R;
import com.imamudin.cop.app.MyAppController;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.model.NotifikasiItem;

import org.apache.commons.lang3.text.WordUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by imamudin on 11/11/16.
 */
public class ListAdapterNotifikasi extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<NotifikasiItem> notifikasiItems;
    ImageLoader imageLoader = MyAppController.getInstance().getImageLoader();

    public ListAdapterNotifikasi(Activity activity, List<NotifikasiItem> notifikasiItems) {
        this.activity = activity;
        this.notifikasiItems = notifikasiItems;
    }

    @Override
    public int getCount() {
        return notifikasiItems.size();
    }

    @Override
    public Object getItem(int location) {
        return notifikasiItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_notifikasi, null);


        TextView title      = (TextView) convertView.findViewById(R.id.tv_notif_judul);
        TextView waktu      = (TextView) convertView.findViewById(R.id.tv_notif_waktu);
        ImageView img_doc   = (ImageView) convertView.findViewById(R.id.img_notifikasi);
        // getting billionaires data for the row
        NotifikasiItem n = notifikasiItems.get(position);

        //untuk waktu
        String mytime=n.getWaktu();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(mytime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm dd MMM yyyy");
        String finalDate = timeFormat.format(myDate);
        waktu.setText(finalDate);

        int img = R.mipmap.ic_launcher_cop;
        if(n.getBerkas_ke() == 0){
            //berkas ke -1 untuk notifikasi jenis kejadian baru
            String n_title = "<b>"+n.getNamaPolisi()+"</b> menambahkan kejadian <b><i>"+n.getNamaKasus()+"</i></b>";
            title.setText(Html.fromHtml(n_title));
            img = R.drawable.ic_kejadian_baru_48;
        }else {
            String nama_kasus = n.getNamaKasus().toLowerCase(); // Make all lowercase if you have caps

            String n_title = "<b>" + n.getNamaPolisi() + "</b> menambahkan berkas <b>" + GlobalConfig.namaDokumen.get(n.getJenisBerkas()) + "</b> " +
                    "pada kasus <b><i>" + WordUtils.capitalize(nama_kasus) + "</i></b>";
            title.setText(Html.fromHtml(n_title));

            if(GlobalConfig.imgDokumen.get(n.getJenisBerkas())!=null){
                img = GlobalConfig.imgDokumen.get(n.getJenisBerkas());
            }
        }
        img_doc.setImageDrawable(activity.getResources().getDrawable(img));

        return convertView;
    }

}