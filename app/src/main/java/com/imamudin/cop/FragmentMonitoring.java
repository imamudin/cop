package com.imamudin.cop;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imamudin.cop.app.MyAppController;
import com.imamudin.cop.berita_acara.InterogasiNotif;
import com.imamudin.cop.berita_acara.LITDokumenNotif;
import com.imamudin.cop.berita_acara.ObservasiNotif;
import com.imamudin.cop.berita_acara.SurveillanceNotif;
import com.imamudin.cop.config.CDatabase;
import com.imamudin.cop.config.CInterogasi;
import com.imamudin.cop.config.CLogin;
import com.imamudin.cop.config.CMonitor;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.db.DBHelper;
import com.imamudin.cop.listAdapter.ListAdapterMonitor;
import com.imamudin.cop.listAdapter.ListAdapterNotifikasi;
import com.imamudin.cop.model.CariKasusItem;
import com.imamudin.cop.model.MonitoringItem;
import com.imamudin.cop.model.NotifikasiItem;
import com.imamudin.cop.mysp.ObscuredSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by imamudin on 10/11/16.
 */
public class FragmentMonitoring extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    DBHelper mydb;
    List<MonitoringItem> monitoringItems = new ArrayList<MonitoringItem>();
    ListView lv1;
    ListAdapterMonitor adapter;

    private SwipeRefreshLayout swipeContainer;

    int offSet=0;

    Runnable runnable;
    Boolean disableSwipeDown = false;       //untuk mendisable swipe down list view
    JsonObjectRequest request;

    LinearLayout ll_sort, ll_filter, ll_main;

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private ObscuredSharedPreferences pref;

    public FragmentMonitoring() {
        // Required empty public constructor
    }
    public static FragmentMonitoring newInstance(String param1, String param2) {
        FragmentMonitoring fragment = new FragmentMonitoring();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        ((MainActivityMonitor) getActivity()).setActionBarTitle("Monitoring");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_monitoring, container, false);

        init(rootView);

        lv1 = (ListView)rootView.findViewById(R.id.custom_list);
        monitoringItems.clear();
        adapter = new ListAdapterMonitor(getActivity(), monitoringItems);
        lv1.setAdapter(adapter);

        //untuk swipelist
        swipeContainer = (SwipeRefreshLayout)rootView.findViewById(R.id.swipeContainer);

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callNews(0);
                Toast.makeText(getActivity(),"memperbarui",Toast.LENGTH_LONG).show();
            }
        });

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected item text from ListView
                Object o = lv1.getItemAtPosition(position);
                MonitoringItem monitoringItem = (MonitoringItem) o;

                Intent output = null;

                if(monitoringItem.getId_doc_opsnal().equals("DOC11")){        //untuk observasi
                    output = new Intent(getActivity(), ObservasiNotif.class);
                }else if(monitoringItem.getId_doc_opsnal().equals("DOC12")) {        //untuk interogasi
                    output = new Intent(getActivity(), InterogasiNotif.class);
                }else if(monitoringItem.getId_doc_opsnal().equals("DOC13")) {        //untuk surveillance
                    output = new Intent(getActivity(), SurveillanceNotif.class);
                }else if(monitoringItem.getId_doc_opsnal().equals("DOC16")) {        //untuk LIT Dokumen
                    output = new Intent(getActivity(), LITDokumenNotif.class);
                }else if(monitoringItem.getId_doc_opsnal().equals("DOC12")) {        //untuk interogasi
                    output = new Intent(getActivity(), InterogasiNotif.class);
                }

                if(output!=null) {
                    output.putExtra("dari_notifikasi", true);
                    output.putExtra(CDatabase.N_NRP, monitoringItem.getNrp());
                    output.putExtra(CDatabase.N_KASUS_ID, monitoringItem.getId_kasus());
                    output.putExtra(CDatabase.N_BERKAS_KE, monitoringItem.getBerkas_ke());

                    startActivity(output);
                }
            }
        });

        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                callNews(0);
            }
        });

        lv1.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }
            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {
                    if(!disableSwipeDown) {
                        Log.d(GlobalConfig.TAG, "start refresh");
                        runnable = new Runnable() {
                            public void run() {
                                Log.d(GlobalConfig.TAG, "start refresh");
                                callNews(offSet);
                            }
                        };
                        runnable.run();
                    }else{
                        Log.d(GlobalConfig.TAG, "disabled");
                        //Toast.makeText(CariKasus.this,"Data telah ditampilkan semua.",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        return rootView;
    }
    private void init(View rootView){
        ll_filter   = (LinearLayout)rootView.findViewById(R.id.ll_filter);
        ll_sort     = (LinearLayout)rootView.findViewById(R.id.ll_sort);
        ll_main     = (LinearLayout)rootView.findViewById(R.id.ll_main);

        ll_filter.setOnClickListener(viewClick);
        ll_sort.setOnClickListener(viewClick);

        pref = new ObscuredSharedPreferences(getActivity(),
                this.getActivity().getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
    }
    private void callNews(int page) {
        swipeContainer.setRefreshing(true);
        if(page==0){
            monitoringItems.clear();
            adapter.notifyDataSetChanged();
        }
        // Creating volley request obj
        String url = "";
        if (pref.getString(GlobalConfig.IP_KEY, null) != null) {
            url = "http://" + pref.getString(GlobalConfig.IP_KEY, "") + GlobalConfig.WEB_URL + CMonitor.URL_MONITORING;
        } else {
            url = "http://" + GlobalConfig.IP + GlobalConfig.WEB_URL + CMonitor.URL_MONITORING;
        }
        Log.d(GlobalConfig.TAG, "" + url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(CMonitor.OFFSET, "" + page);
            jsonBody.put(CMonitor.USER_ID, "" + pref.getString(CLogin.G_ID_USER, ""));
            jsonBody.put(CMonitor.USER_REGID, "" + pref.getString(GlobalConfig.gcmregId, ""));
            jsonBody.put(CMonitor.SORT_BY, "" + CMonitor.SORT_TABLE[pref.getInt(CMonitor.SORT_BY, 0)]);
            jsonBody.put(CMonitor.FILTER_BY, "" + pref.getString(CMonitor.FILTER_BY, ""));
            jsonBody.put(CMonitor.FILTER_VALUE, "" + pref.getString(CMonitor.FILTER_VALUE, ""));

            jsonBody.put(CMonitor.TGL_MULAI, "" + pref.getString(CMonitor.TGL_MULAI, ""));
            jsonBody.put(CMonitor.TGL_AKHIR, "" + pref.getString(CMonitor.TGL_AKHIR, ""));

            Log.d(GlobalConfig.TAG, "sort id "+pref.getInt(CMonitor.SORT_BY, 0));

            Log.d(GlobalConfig.TAG, jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(GlobalConfig.TAG, response.toString());
                    try {
                        int status = response.getInt("status");
                        String pesan = response.getString("pesan");
                        Log.d(GlobalConfig.TAG, "" + status);
                        if (status == 1) {
                            Log.d(GlobalConfig.TAG, "" + status);
                            JSONArray monitors = response.getJSONArray("data");
                            if (monitors.length() < 50) {         //50 dari jumlah limit di web
                                disableSwipeDown = true;
                            }
                            offSet = offSet + monitors.length();
                            if (monitors.length() > 0) {
                                for (int i = 0; i < monitors.length(); i++) {
                                    JSONObject monitor = monitors.getJSONObject(i);

                                    MonitoringItem mi = new MonitoringItem();
                                    mi.setId_kasus(monitor.getString(CMonitor.G_ID_KASUS));
                                    mi.setId_doc_opsnal(monitor.getString(CMonitor.G_ID_DOC));
                                    mi.setBerkas_ke(Integer.parseInt(monitor.getString(CMonitor.G_BERKAS_KE)));
                                    mi.setTgl_ba(monitor.getString(CMonitor.G_TGL_BA));
                                    mi.setNrp(monitor.getString(CMonitor.G_NRP));
                                    mi.setUser_nama(monitor.getString(CMonitor.G_USER_NAMA));
                                    mi.setNama_kasus(monitor.getString(CMonitor.G_NAMA_KASUS));
                                    mi.setNo_lp(monitor.getString(CMonitor.G_NO_LP));
                                    mi.setNama_pelapor(monitor.getString(CMonitor.G_NAMA_PELAPOR));
                                    mi.setNama_penyidik(monitor.getString(CMonitor.G_NAMA_PENYIDIK));

                                    monitoringItems.add(mi);

                                    swipeContainer.setRefreshing(false);

                                    adapter.notifyDataSetChanged();
                                }
                            } else {
                                notifikasi("Data tidak ditemukan.");
                                swipeContainer.setRefreshing(false);
                            }
                        } else {
                            notifikasi(pesan);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        swipeContainer.setRefreshing(false);
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    swipeContainer.setRefreshing(false);
                    //Log.d("respons",error.getMessage().toString());
                }
            }) {
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_NAME, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    View.OnClickListener viewClick = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            if(v == ll_filter){
                Intent filter = new Intent(getActivity(), MenuFilter.class);
                startActivityForResult(filter, CMonitor.REQUEST_CODE_MENU_FILTER);
            }else if(v == ll_sort){
                showdialogSortBy();
            }
        }
    };
    public void showdialogSortBy() {
        LayoutInflater layoutInflater = (LayoutInflater)getActivity().getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.menu_sort, null);
        final  AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(promptView);

        final TextView t_title = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final LinearLayout ll_tgl          = (LinearLayout) promptView.findViewById(R.id.ll_tgl);
        final LinearLayout ll_nama_kasus   = (LinearLayout) promptView.findViewById(R.id.ll_nama_kasus);
        final LinearLayout ll_nama_pelapor = (LinearLayout) promptView.findViewById(R.id.ll_nama_pelapor);
        final LinearLayout ll_no_lp        = (LinearLayout) promptView.findViewById(R.id.ll_no_lp);

        t_title.setText("Urutkan");

        //mengatur menu awal
        int sort = pref.getInt(CMonitor.SORT_BY, 0);
        if(sort == 0 || sort == CMonitor.SORT_BY_VALUE[0]){     //untuk tanggal
            setColorTV(ll_tgl, ll_nama_kasus, ll_nama_pelapor, ll_no_lp);
        }else if(sort == CMonitor.SORT_BY_VALUE[1]) {             //untuk nama kasus
            setColorTV(ll_nama_kasus, ll_nama_pelapor, ll_tgl, ll_no_lp);
        }else if(sort == CMonitor.SORT_BY_VALUE[2]) {             //untuk nama pelopor
            setColorTV(ll_nama_pelapor, ll_nama_kasus, ll_tgl, ll_no_lp);
        }else if(sort == CMonitor.SORT_BY_VALUE[3]) {             //untuk no lp
            setColorTV(ll_no_lp, ll_nama_kasus, ll_tgl, ll_nama_pelapor);
        }

        View.OnClickListener menu   = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(v == ll_tgl){
                    setColorTV(ll_tgl, ll_nama_kasus, ll_nama_pelapor, ll_no_lp);
                    pref.edit().putInt(CMonitor.SORT_BY_TEMP, CMonitor.SORT_BY_VALUE[0]).commit();
                }else if (v == ll_nama_kasus){
                    setColorTV(ll_nama_kasus, ll_nama_pelapor, ll_tgl, ll_no_lp);
                    pref.edit().putInt(CMonitor.SORT_BY_TEMP, CMonitor.SORT_BY_VALUE[1]).commit();
                }else if (v == ll_nama_pelapor){
                    setColorTV(ll_nama_pelapor, ll_nama_kasus, ll_tgl, ll_no_lp);
                    pref.edit().putInt(CMonitor.SORT_BY_TEMP, CMonitor.SORT_BY_VALUE[2]).commit();
                }else if (v == ll_no_lp){
                    setColorTV(ll_no_lp, ll_nama_kasus, ll_tgl, ll_nama_pelapor);
                    pref.edit().putInt(CMonitor.SORT_BY_TEMP, CMonitor.SORT_BY_VALUE[3]).commit();
                }
            }
        };
        // setup a dialog window
        alertDialogBuilder.setCancelable(true)
            .setPositiveButton("Terapkan",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            pref.edit().putInt(CMonitor.SORT_BY, pref.getInt(CMonitor.SORT_BY_TEMP, 0)).commit();
                            callNews(0);
                        }
                    });

        ll_tgl.setOnClickListener(menu);
        ll_nama_kasus.setOnClickListener(menu);
        ll_nama_pelapor.setOnClickListener(menu);
        ll_no_lp.setOnClickListener(menu);
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
    private void setColorTV(LinearLayout ll_tgl, LinearLayout ll_nama_kasus, LinearLayout ll_nama_pelapor, LinearLayout ll_no_lp){
        if(ll_tgl.getChildAt(0) instanceof TextView){
            ((TextView) ll_tgl.getChildAt(0)).setTextColor(Color.BLACK);
            ((TextView) ll_nama_kasus.getChildAt(0)).setTextColor(Color.BLACK);
            ((TextView) ll_nama_pelapor.getChildAt(0)).setTextColor(Color.BLACK);
            ((TextView) ll_no_lp.getChildAt(0)).setTextColor(Color.BLACK);
        }
        //diawal ll_tgl yang pertama
        TextView a = ((TextView) ll_tgl.getChildAt(0));
        a.setTextColor(getResources().getColor(R.color.colorSuccess));
    }
    private void tutup_menu(AlertDialog menu){
        menu.cancel();
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CMonitor.REQUEST_CODE_MENU_FILTER) {
            Log.d(GlobalConfig.TAG, "on Activity Result");
            callNews(0);
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}