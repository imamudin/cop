package com.imamudin.cop;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.JsonObjectRequest;
import com.imamudin.cop.app.MyAppController;
import com.imamudin.cop.config.GlobalConfig;

/**
 * Created by imamudin on 23/11/16.
 */
public class Notifikasi extends AppCompatActivity {

    LinearLayout ll_main;
    TextView tv_msg;
    Intent old;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notifikasi);

        //set Toolbar
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Kejadian Menonjol");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        tv_msg      = (TextView) findViewById(R.id.tv_msg_notifikasi);

        old     = getIntent();
        String msg  = old.getStringExtra("message");

        tv_msg.setText(msg);
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    public void closeActivity(){
        super.onBackPressed();
    }
    @Override
    public void onBackPressed() {
        Log.d(getLocalClassName(),"back");
        setResult(RESULT_OK);
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
