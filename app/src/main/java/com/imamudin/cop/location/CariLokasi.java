package com.imamudin.cop.location;

/**
 * Created by imamudin on 24/10/16.
 */
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.imamudin.cop.R;
import com.imamudin.cop.config.CInterogasi;
import com.imamudin.cop.config.GlobalConfig;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by imamudin on 24/10/16.
 */
public class CariLokasi extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        ResultCallback<LocationSettingsResult> {

    GoogleMap Mmap;
    //GPSTracker gps;
    Marker marker;

    Boolean firts_time = null;
    Double longitude, latitude;

    LinearLayout ll_kirim_lokasi;

    //untuk mendapatakan lokasi
    protected static final int REQUEST_CHECK_SETTINGS           = 20;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS    = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected Location mCurrentLocation;

    // UI Widgets.
    protected TextView mLastUpdateTimeTextView;
    protected TextView mLatLongLabel;
    protected TextView mLatLong;

    ProgressDialog pDialog=null;

    protected Boolean mRequestingLocationUpdates;
    protected String mLastUpdateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cari_lokasi);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();

        }
        //gps=new GPSTracker(CariLokasi.this);
        firts_time=true;

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Cari Lokasi");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        ll_kirim_lokasi     = (LinearLayout)findViewById(R.id.ll_kirim_lokasi);
        ll_kirim_lokasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(latitude, longitude);
            }
        });

        getLokasiInit();
        checkLocationSettings();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        public PlaceholderFragment() {

        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.cari_lokasi_fragment, container,
                    false);

            return rootView;
        }
    }

    /*-----------------------------------untuk dapat lokasi---------------------------*/
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }
    private void getLokasiInit(){
        //mStartUpdatesButton = (Button) findViewById(R.id.start_updates_button);
        //mStopUpdatesButton = (Button) findViewById(R.id.stop_updates_button);
//        mLatLongLabel   = (TextView) findViewById(R.id.tv_latlong_label);
//        mLatLong        = (TextView) findViewById(R.id.tv_latlong);
//        mLastUpdateTimeTextView = (TextView) findViewById(R.id.tv_alamat_lokasi);

        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";

        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
    }
    protected synchronized void buildGoogleApiClient() {
        Log.i(getLocalClassName(), "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }
    protected void checkLocationSettings() {
        Log.d(getLocalClassName(),"checkLocationSettings");
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }
    @Override
    public void onConnected(Bundle bundle) {
        Log.i(getLocalClassName(), "Connected to GoogleApiClient");
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

            //updateLocationUI();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(getLocalClassName(), "Location onConnectionSuspended");
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(getLocalClassName(), "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(getLocalClassName(), "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

        updateLocationUI();

//        Toast.makeText(this, "Lokasi telah diperbarui",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(getLocalClassName(), "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(getLocalClassName(), "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(getLocalClassName(), "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(CariLokasi.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i(getLocalClassName(), "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i(getLocalClassName(), "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stopLocationUpdates();
    }

    private void setpDialog(String log){
        Log.d(GlobalConfig.TAG, log);
        pDialog = new ProgressDialog(CariLokasi.this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Mohon tunggu…");
        pDialog.show();
    }
    protected void startLocationUpdates() {
        setpDialog("startLocationUpdates");
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = true;
            }
        });
    }
    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
            latitude   = loc.latitude;
            longitude  = loc.longitude;
            //Log.d(GlobalConfig.TAG, latitude+"|"+longitude+" updated");
        }
    };
    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            String yourAddress = "";
            String yourCity = "";
            String yourCountry = "";
            //get address
            Geocoder geocoder;
            List<Address> yourAddresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            try {
                yourAddresses = geocoder.getFromLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), 1);

                if (yourAddresses.size() > 0) {
                    yourAddress = yourAddresses.get(0).getAddressLine(0);
                    yourCity = yourAddresses.get(0).getAddressLine(1);
                }
            }catch (Exception e){
                Log.d(getLocalClassName(), e.getMessage().toString());
            }

            LatLng currentpos=new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            latitude   = currentpos.latitude;
            longitude  = currentpos.longitude;

            //mengilangkan progress dialog
            pDialog.dismiss();
            if((MapFragment) getFragmentManager().findFragmentById(R.id.map)!=null) {
                Mmap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
                Mmap.getUiSettings().setZoomControlsEnabled(true);
                Mmap.getUiSettings().setCompassEnabled(true);
                Mmap.getUiSettings().setMapToolbarEnabled(true);
                if(firts_time) {
                    firts_time=false;

                    Mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentpos, 15));
                    if (marker != null)
                        marker.remove();
                    Mmap.setMyLocationEnabled(true);
                    Mmap.setOnMyLocationChangeListener(myLocationChangeListener);
                    marker = Mmap.addMarker(new MarkerOptions().position(currentpos)
                            .title("Pilih lokasi ini")
                            .snippet(yourAddress+", "+yourCity)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_png))
                            .draggable(true));

                    Mmap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            LatLng markerLocation = marker.getPosition();

                            sendResult(markerLocation.latitude, markerLocation.longitude);
                        }
                    });
                    Mmap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

                        @Override
                        public void onMarkerDrag(Marker arg0) {
                            // TODO Auto-generated method stub
                            Log.d("Marker", "Dragging");
                        }

                        @Override
                        public void onMarkerDragEnd(Marker arg0) {
                            // TODO Auto-generated method stub
                            LatLng markerLocation = arg0.getPosition();
                            String yourAddress = "";
                            String yourCity = "";
                            //get address
                            Geocoder geocoder;
                            List<Address> yourAddresses;
                            geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            try {
                                yourAddresses = geocoder.getFromLocation(markerLocation.latitude, markerLocation.longitude, 1);

                                if (yourAddresses.size() > 0) {
                                    yourAddress = yourAddresses.get(0).getAddressLine(0);
                                    yourCity = yourAddresses.get(0).getAddressLine(1);
                                }
                            }catch (Exception e){

                            }

                            if (marker != null)
                                marker.remove();
                            marker = Mmap.addMarker(new MarkerOptions().position(marker.getPosition())
                                    .title("Pilih lokasi ini")
                                    .snippet(yourAddress+", "+yourCity)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_png))
                                    .draggable(true));

                            //untuk text menjadi multi line
                            Mmap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                                @Override
                                public View getInfoWindow(Marker arg0) {
                                    return null;
                                }

                                @Override
                                public View getInfoContents(Marker marker) {

                                    LinearLayout info = new LinearLayout(getApplicationContext());
                                    info.setOrientation(LinearLayout.VERTICAL);

                                    TextView title = new TextView(getApplicationContext());
                                    title.setTextColor(Color.BLACK);
                                    title.setGravity(Gravity.CENTER);
                                    title.setTypeface(null, Typeface.BOLD);
                                    title.setText(marker.getTitle());

                                    TextView snippet = new TextView(getApplicationContext());
                                    snippet.setTextColor(Color.GRAY);
                                    snippet.setGravity(Gravity.CENTER);
                                    snippet.setText(marker.getSnippet());

                                    info.addView(title);
                                    info.addView(snippet);

                                    return info;
                                }
                            });
                            Mmap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                                @Override
                                public void onInfoWindowClick(Marker marker) {
                                    LatLng markerLocation = marker.getPosition();
                                    String lokasi = "Kirim lokasi koordinat"+markerLocation.toString();
                                    Log.d(getLocalClassName(), lokasi);
                                    //Toast.makeText(getApplicationContext(), lokasi, Toast.LENGTH_SHORT);

                                    sendResult(markerLocation.latitude, markerLocation.longitude);
                                }
                            });
                            Log.d("Marker", "finishede");
                        }

                        @Override
                        public void onMarkerDragStart(Marker arg0) {
                            // TODO Auto-generated method stub
                            Log.d("Marker", "Started");

                        }
                    });
                }
            }
            stopLocationUpdates();
        }
    }
    private void sendResult(Double lati, Double longi){
        Intent output = new Intent();
        output.putExtra(CInterogasi.LONGITUDE, longi);
        output.putExtra(CInterogasi.LATITUDE, lati);
        setResult(RESULT_OK, output);
        stopLocationUpdates();
        finish();
    }
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
            }
        });
    }
    /*-----------------------------------untuk dapat lokasi---------------------------*/

}
