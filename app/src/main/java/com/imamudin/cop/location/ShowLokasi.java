package com.imamudin.cop.location;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.imamudin.cop.R;
import com.imamudin.cop.config.CInterogasi;
import com.imamudin.cop.config.GlobalConfig;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by imamudin on 15/11/16.
 */
public class ShowLokasi extends AppCompatActivity{

    GoogleMap Mmap;
    //GPSTracker gps;
    Marker marker;

    Boolean firts_time = null;

    LinearLayout ll_kirim_lokasi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cari_lokasi);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();

        }
        firts_time=true;

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Lokasi");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        ll_kirim_lokasi     = (LinearLayout)findViewById(R.id.ll_kirim_lokasi);
        ll_kirim_lokasi.setVisibility(View.GONE);

        Intent get = getIntent();

        Log.d(GlobalConfig.TAG, get.getStringExtra("LATITUDE") + " "+get.getStringExtra("LONGITUDE"));

        double latitude = Double.parseDouble(get.getStringExtra("LATITUDE"));
        double longitude = Double.parseDouble(get.getStringExtra("LONGITUDE"));
        LatLng currentpos=new LatLng(latitude, longitude);

        String yourAddress = "";
        String yourCity = "";
        //get address
        Geocoder geocoder;
        List<Address> yourAddresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            yourAddresses = geocoder.getFromLocation(latitude, longitude, 1);

            if (yourAddresses.size() > 0) {
                yourAddress = yourAddresses.get(0).getAddressLine(0);
                yourCity = yourAddresses.get(0).getAddressLine(1);
            }
        }catch (Exception e){
            Log.d(getLocalClassName(), e.getMessage().toString());
        }

        if((MapFragment) getFragmentManager().findFragmentById(R.id.map)!=null) {
            Mmap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            Mmap.getUiSettings().setZoomControlsEnabled(true);
            Mmap.getUiSettings().setCompassEnabled(true);
            //Mmap.getUiSettings().setMapToolbarEnabled(true);
            if(firts_time) {
                firts_time=false;

                Mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentpos, 15));
                if (marker != null)
                    marker.remove();
                Mmap.setMyLocationEnabled(true);
                marker = Mmap.addMarker(new MarkerOptions().position(currentpos)
                        .title("Lokasi")
                        .snippet(yourAddress+", "+yourCity)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_png))
                        .draggable(false));
            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment {
        public PlaceholderFragment() {

        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.cari_lokasi_fragment, container,
                    false);

            return rootView;
        }
    }
}