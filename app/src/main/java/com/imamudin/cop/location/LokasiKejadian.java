package com.imamudin.cop.location;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.imamudin.cop.R;
import com.imamudin.cop.app.MyAppController;
import com.imamudin.cop.config.CLogin;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.mysp.ObscuredSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by imamudin on 24/11/16.
 */
public class LokasiKejadian extends AppCompatActivity{

    GoogleMap Mmap;
    //GPSTracker gps;
    Marker marker;

    Boolean firts_time = null;
    boolean status_data_kejadian = true;
    ObscuredSharedPreferences pref;
    JsonObjectRequest request;
    TextView tv_nama_kejadian, tv_nama_penyidik, tv_lokasi, tv_nama_pelaku, tv_korban, tv_kerugian;
    TextView tv_saksi, tv_keterangan;

    LinearLayout ll_data_kejadian, ll_btn_data_kejadian, ll_main;
    String id_kejadian;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_kejadian);

        firts_time=true;

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Kejadian Menonjol");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        ll_main     = (LinearLayout)findViewById(R.id.ll_main);
        ll_btn_data_kejadian = (LinearLayout)findViewById(R.id.ll_btn_data_kejadian);
        ll_data_kejadian     = (LinearLayout)findViewById(R.id.ll_data_kejadian);

        tv_nama_kejadian    = (TextView)findViewById(R.id.tv_nama_kejadian);
        tv_nama_penyidik    = (TextView)findViewById(R.id.tv_nama_penyidik);
        tv_lokasi           = (TextView)findViewById(R.id.tv_lokasi);
        tv_nama_pelaku      = (TextView)findViewById(R.id.tv_nama_pelaku);
        tv_korban           = (TextView)findViewById(R.id.tv_nama_korban);
        tv_kerugian         = (TextView)findViewById(R.id.tv_kerugian);
        tv_saksi            = (TextView)findViewById(R.id.tv_saksi);
        tv_keterangan       = (TextView)findViewById(R.id.tv_keterangan);

        ll_btn_data_kejadian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status_data_kejadian = false;
                //data_kejadian set height wracpcontent
            }
        });

        pref = new ObscuredSharedPreferences(this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        ll_data_kejadian.setVisibility(View.GONE);
        Intent get = getIntent();

        id_kejadian = get.getStringExtra(GlobalConfig.G_ID_KEJADIAN);

        getKejadian(id_kejadian);

        //Log.d(GlobalConfig.TAG, get.getStringExtra("LATITUDE") + " "+get.getStringExtra("LONGITUDE"));


//        double latitude = Double.parseDouble(get.getStringExtra("LATITUDE"));
//        double longitude = Double.parseDouble(get.getStringExtra("LONGITUDE"));
//        double latitude = Double.parseDouble("-7.288277");
//        double longitude = Double.parseDouble("112.80917");
//        LatLng currentpos=new LatLng(latitude, longitude);
//
//        String yourAddress = "";
//        String yourCity = "";
//        //get address
//        Geocoder geocoder;
//        List<Address> yourAddresses;
//        geocoder = new Geocoder(this, Locale.getDefault());
//        try {
//            yourAddresses = geocoder.getFromLocation(latitude, longitude, 1);
//
//            if (yourAddresses.size() > 0) {
//                yourAddress = yourAddresses.get(0).getAddressLine(0);
//                yourCity = yourAddresses.get(0).getAddressLine(1);
//            }
//        }catch (Exception e){
//            Log.d(getLocalClassName(), e.getMessage().toString());
//        }
//
//        if((MapFragment) getFragmentManager().findFragmentById(R.id.map)!=null) {
//            Mmap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
//            Mmap.getUiSettings().setZoomControlsEnabled(true);
//            Mmap.getUiSettings().setCompassEnabled(true);
//            Mmap.getUiSettings().setMapToolbarEnabled(true);
//            if(firts_time) {
//                firts_time=false;
//
//                Mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentpos, 15));
//                if (marker != null)
//                    marker.remove();
//                Mmap.setMyLocationEnabled(true);
//                marker = Mmap.addMarker(new MarkerOptions().position(currentpos)
//                        .title("Lokasi")
//                        .snippet(yourAddress+", "+yourCity)
//                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_png))
//                        .draggable(false));
//            }
//        }
    }
    private void getKejadian(String id_kejadian){
        final ProgressDialog loading = new ProgressDialog(this);
        loading.setTitle("Mencari berkas");
        loading.setMessage("Mohon tunggu...");
        loading.setCancelable(false);
        loading.setButton(DialogInterface.BUTTON_NEGATIVE, "Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cancelRequest();
                dialog.dismiss();
            }
        });
        loading.show();
        String url ="";
        if(pref.getString(GlobalConfig.IP_KEY, null) != null){
            url = "http://"+pref.getString(GlobalConfig.IP_KEY, "")+GlobalConfig.WEB_URL+ GlobalConfig.URL_get_KEJADIAN_BARU;
        }else{
            url = "http://"+GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_get_KEJADIAN_BARU;
        }
        //upload dokumen
        JSONObject jsonBody;
        Log.d(GlobalConfig.TAG, url);
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, ""+pref.getString(CLogin.G_ID_USER,""));
            jsonBody.put(GlobalConfig.USER_REGID, ""+pref.getString(GlobalConfig.gcmregId,""));

            jsonBody.put(GlobalConfig.UP_KEJADIAN_ID, id_kejadian);
            Log.d(GlobalConfig.TAG, jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        loading.dismiss();
                        int status  = response.getInt("status");
                        String pesan= response.getString("pesan");
                        Log.d(GlobalConfig.TAG+" result ", ""+response.toString());
                        if(status==1){
                            ll_data_kejadian.setVisibility(View.VISIBLE);
                            JSONArray berkass = response.getJSONArray("data");
                            if(berkass.length()==1){
                                JSONObject berkas = berkass.getJSONObject(0);

                                String nama_kejadian = berkas.getString(GlobalConfig.G_NAMA_KEJADIAN);
                                String lokasi        = berkas.getString(GlobalConfig.G_ALAMAT);
                                String nama_pelaku   = berkas.getString(GlobalConfig.G_NAMA_PELAKU);
                                String korban        = berkas.getString(GlobalConfig.G_NAMA_KORBAN);
                                String kerugian      = berkas.getString(GlobalConfig.G_KERUGIAN);
                                String saksi         = berkas.getString(GlobalConfig.G_SAKSI);
                                String keterangan    = berkas.getString(GlobalConfig.G_KETERANGAN);
                                String nama_penyidik = berkas.getString(GlobalConfig.G_NAMA_PENYIDIK);

                                tv_nama_kejadian.setText(nama_kejadian);
                                tv_lokasi.setText(lokasi);
                                tv_nama_pelaku.setText(nama_pelaku);
                                tv_korban.setText(korban);
                                tv_kerugian.setText(kerugian);
                                tv_saksi.setText(saksi);
                                tv_keterangan.setText(keterangan);
                                tv_nama_penyidik.setText(nama_penyidik);

                                double latitude      = Double.parseDouble(berkas.getString(GlobalConfig.G_LATITUDE));
                                double longitude     = Double.parseDouble(berkas.getString(GlobalConfig.G_LONGITUDE));

                                LatLng currentpos=new LatLng(latitude, longitude);
                                String yourAddress = "";
                                String yourCity = "";
                                //get address
                                Geocoder geocoder;
                                List<Address> yourAddresses;
                                geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                                try {
                                    yourAddresses = geocoder.getFromLocation(latitude, longitude, 1);

                                    if (yourAddresses.size() > 0) {
                                        yourAddress = yourAddresses.get(0).getAddressLine(0);
                                        yourCity = yourAddresses.get(0).getAddressLine(1);
                                    }
                                }catch (Exception e){
                                    Log.d(getLocalClassName(), e.getMessage().toString());
                                }

                                if((MapFragment) getFragmentManager().findFragmentById(R.id.map)!=null) {
                                    Mmap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
                                    Mmap.getUiSettings().setZoomControlsEnabled(true);
                                    Mmap.getUiSettings().setCompassEnabled(true);
                                    Mmap.getUiSettings().setMapToolbarEnabled(true);
                                    if(firts_time) {
                                        firts_time=false;

                                        Mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentpos, 15));
                                        if (marker != null)
                                            marker.remove();
                                        Mmap.setMyLocationEnabled(true);
                                        marker = Mmap.addMarker(new MarkerOptions().position(currentpos)
                                                .title("Lokasi")
                                                .snippet(yourAddress+", "+yourCity)
                                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_png))
                                                .draggable(false));
                                    }
                                }
                            }
                        }else{
                            notifikasi(pesan);
                        }
                    } catch (JSONException e) {
                        loading.dismiss();
                        notifikasi(GlobalConfig.MSG_KESALAHAN);
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    loading.dismiss();
                    notifikasi(GlobalConfig.MSG_KESALAHAN);
                    //Log.d("respons",error.getMessage().toString());
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_NAME, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);
        } catch (JSONException e) {
            loading.dismiss();
            e.printStackTrace();
        }
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            ll_main.setOrientation(LinearLayout.HORIZONTAL);
            //Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            ll_main.setOrientation(LinearLayout.VERTICAL);
//            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
        closeActivity();
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    public void closeActivity(){
        super.onBackPressed();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment {
        public PlaceholderFragment() {

        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.cari_lokasi_fragment, container,
                    false);

            return rootView;
        }
    }
}
