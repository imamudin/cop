package com.imamudin.cop;

/**
 * Created by agung on 19/02/2016.
 */
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.imamudin.cop.berita_acara.Interogasi;
import com.imamudin.cop.berita_acara.LITDokumen;
import com.imamudin.cop.berita_acara.Observasi;
import com.imamudin.cop.berita_acara.Surveillance;
import com.imamudin.cop.berita_acara.UnderCover;

public class FragmentUploadBerkas extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    LinearLayout ll_interogasi, ll_lit_dokumen, ll_observasi, ll_surveillance, ll_tracking_cash;
    LinearLayout ll_tracking_hp, ll_tracking_sms, ll_undercovera, ll_undercover_buy;

    private OnFragmentInteractionListener mListener;

    public FragmentUploadBerkas() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentOne.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentUploadBerkas newInstance(String param1, String param2) {
        FragmentUploadBerkas fragment = new FragmentUploadBerkas();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        ((MainActivityOpsnal) getActivity()).setActionBarTitle("Upload Berkas");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =inflater.inflate(R.layout.fragment_upload_berkas,container,false);

        ll_interogasi       = (LinearLayout)v.findViewById(R.id.ll_interogasi);
        ll_interogasi.setOnClickListener(LayoutListener);
        ll_lit_dokumen      = (LinearLayout)v.findViewById(R.id.ll_lit_dokumen);
        ll_lit_dokumen.setOnClickListener(LayoutListener);
        ll_observasi        = (LinearLayout)v.findViewById(R.id.ll_observasi);
        ll_observasi.setOnClickListener(LayoutListener);

        ll_surveillance     = (LinearLayout)v.findViewById(R.id.ll_surveillancce);
        ll_surveillance.setOnClickListener(LayoutListener);

        ll_tracking_cash    = (LinearLayout)v.findViewById(R.id.ll_tracking_cash_flow);
        ll_tracking_cash.setOnClickListener(LayoutListener);

        ll_tracking_hp      = (LinearLayout)v.findViewById(R.id.ll_tracking_hp);
        ll_tracking_hp.setOnClickListener(LayoutListener);

        ll_tracking_sms      = (LinearLayout)v.findViewById(R.id.ll_tracking_SMS);
        ll_tracking_sms.setOnClickListener(LayoutListener);

        ll_undercovera      = (LinearLayout)v.findViewById(R.id.ll_undercover);
        ll_undercovera.setOnClickListener(LayoutListener);

        ll_undercover_buy    = (LinearLayout)v.findViewById(R.id.ll_undercover_buy);
        ll_undercover_buy.setOnClickListener(LayoutListener);

        return v;
    }

    View.OnClickListener  LayoutListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent berkas = null;
            if(view==ll_interogasi){
                berkas = new Intent(getContext(), Interogasi.class);
            }else if(view==ll_lit_dokumen){
                berkas = new Intent(getContext(), LITDokumen.class);
            }else if(view==ll_observasi){
                berkas = new Intent(getContext(), Observasi.class);
            }else if(view==ll_surveillance){
                berkas = new Intent(getContext(), Surveillance.class);
            }else if(view==ll_tracking_cash){
                Toast.makeText(getContext(),"ll_tracking cash",Toast.LENGTH_LONG).show();
            }else if(view==ll_tracking_hp){
                Toast.makeText(getContext(),"ll_tracking hp",Toast.LENGTH_LONG).show();
            }else if(view==ll_tracking_sms){
                Toast.makeText(getContext(),"ll_tracking sms",Toast.LENGTH_LONG).show();
            }else if(view==ll_undercovera){
                berkas = new Intent(getContext(), UnderCover.class);
            }else if(view==ll_undercover_buy){
                Toast.makeText(getContext(),"ll_undercover buy",Toast.LENGTH_LONG).show();
            }
            if(berkas!=null)
                startActivity(berkas);
        }
    };

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}