package com.imamudin.cop.gcm;

/**
 * Created by agung on 05/04/2016.
 */
import android.annotation.TargetApi;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.imamudin.cop.MainActivityMonitor;
import com.imamudin.cop.MainActivityOpsnal;
import com.imamudin.cop.R;
import com.imamudin.cop.config.CGCM;
import com.imamudin.cop.config.CMain;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.db.DBHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class GCMIntentService extends IntentService {

    int numMessages;
    public static String GROUP_KEY_NOTIFICATION = "berkas_acara";

    NotificationManager mNotificationManager;
    public GCMIntentService() {
        super(GCMIntentService.class.getName());
    }
    protected void onRegistered(Context context, String regId) {
        // Send the regId to your server.
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        Log.d(GlobalConfig.TAG,""+extras.toString());
        if (!extras.isEmpty()) {
            try {
                Log.d(GlobalConfig.TAG,""+extras.toString());

                String id_kejadian      = extras.getString(CGCM.G_ID_KEJADIAN, "");
                if(!id_kejadian.equals("")){
                    //notifikasi dari kejadian baru
                    Log.d(GlobalConfig.TAG,"tanggal kejadina"+extras.getString(CGCM.G_TGL_KEJADIAN, ""));

                    String nama_polisi      = extras.getString(CGCM.G_NAMA_PENYIDIK, "");
                    String jenis_berkas     = "KEJ00";                  // ==> lihat variabel namaDokumen pada global config
                    String nama_kasus       = extras.getString(CGCM.G_NAMA_KEJADIAN, "");
                    String nrp              = extras.getString(CGCM.G_NRP, "");
                    String waktu            = extras.getString(CGCM.G_TGL_KEJADIAN, "");

                    //berkas ke dibuat -1 karena untuk notifikasi kejadian baru, bukan dari berkas acara
                    int berkas_ke           = 0;

                    DBHelper mydb = new DBHelper(GCMIntentService.this);
                    mydb.insertNotifikasi (nama_polisi, jenis_berkas,
                            nama_kasus, nrp, id_kejadian, waktu, berkas_ke);

                    sendNotification("Kejadian Menonjol", nama_polisi+
                            " menambahkan kejadian \" "+nama_kasus+" \"", 10);
                }else{
                    Log.d(GlobalConfig.TAG,"tanggal berkas"+extras.getString(CGCM.G_WAKTU, ""));
                    String nama_polisi      = extras.getString(CGCM.G_NAMA_PENYIDIK, "");
                    String jenis_berkas     = extras.getString(CGCM.G_ID_DOC_OPSNAL, "");
                    String nama_kasus       = extras.getString(CGCM.G_NAMA_KASUS, "");
                    String nrp              = extras.getString(CGCM.G_NRP, "");
                    String kasus_id         = extras.getString(CGCM.G_KASUS_ID, "");
                    String waktu            = extras.getString(CGCM.G_WAKTU, "");
                    int berkas_ke           = Integer.parseInt(extras.getString(CGCM.G_BERKAS_KE, "0"));

                    if(!nama_polisi.equals("") || !jenis_berkas.equals("") || !nama_kasus.equals("")
                            || !nrp.equals("") || !kasus_id.equals("") || !waktu.equals("")
                            || berkas_ke!=0){

                        DBHelper mydb = new DBHelper(GCMIntentService.this);
                        mydb.insertNotifikasi (nama_polisi, jenis_berkas,
                                nama_kasus, nrp, kasus_id, waktu, berkas_ke);

                        sendNotification("Data Berkas Baru", nama_polisi+
                                " menambahkan berkas " + GlobalConfig.namaDokumen.get(jenis_berkas), 10);
                    }else{
                        Log.d(GlobalConfig.TAG, "GCM Message null.");
                    }
                }
            }catch (Exception e){
                Log.d(GlobalConfig.TAG,""+e.getMessage().toString());
            }
        }else{
            Log.d(GlobalConfig.TAG,"extra kosong");
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GCMBroadcastReceiver.completeWakefulIntent(intent);
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void sendNotification(String title,String msg, int id) {
        Random random = new Random();
        int id_notifikasi = random.nextInt(9999 - 1000) + 1000;
        mNotificationManager = (NotificationManager) getApplicationContext()
                .getSystemService(getApplicationContext().NOTIFICATION_SERVICE);

        Intent intent1 = new Intent(this.getApplicationContext(),
                MainActivityMonitor.class);
        intent1.putExtra(CMain.IS_MONITORING, true);
        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(
                this.getApplicationContext(), id_notifikasi, intent1,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        Uri alarmSound = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        numMessages = 0;                                                     //untuk mengupdate notification
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_add)
                        .setContentTitle(title)
                        .setContentText(msg)
                        .setGroup(GROUP_KEY_NOTIFICATION)
                        .setSound(alarmSound)
                        .setLights(Color.WHITE, 500, 500)
                        .setAutoCancel(true).setTicker(title)
                        .setStyle(new NotificationCompat.InboxStyle()
                                .addLine(msg))
                        .setVibrate(new long[]{100, 250, 100, 250, 100, 250})
                        .setContentIntent(pendingNotificationIntent)
                        .setNumber(++numMessages);
        ///                        .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))

        mNotificationManager.notify(id_notifikasi, mBuilder.build());
    }
}