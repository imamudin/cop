package com.imamudin.cop;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.imamudin.cop.app.MyAppController;
import com.imamudin.cop.config.CLogin;
import com.imamudin.cop.config.CMain;
import com.imamudin.cop.config.CMonitor;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.db.DBHelper;
import com.imamudin.cop.gcm.GCMIntentService;
import com.imamudin.cop.mysp.ObscuredSharedPreferences;

public class MainActivityMonitor extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        FragmentMonitoring.OnFragmentInteractionListener, FragmentNotifikasi.OnFragmentInteractionListener{

    ObscuredSharedPreferences pref;
    TextView t_user_name;
    android.support.v7.app.ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_monitor);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle("COP");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        t_user_name    = (TextView)findViewById(R.id.t_user_name);
        //inisialisasi
        pref = new ObscuredSharedPreferences(this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        //mengatur profil pada navigation view
        if(pref.getBoolean(CMain.IS_LOGIN, false)){
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            View header=navigationView.getHeaderView(0);

            t_user_name = (TextView)header.findViewById(R.id.t_user_name);
            t_user_name.setText(pref.getString(CLogin.G_USER_NAME, ""));
        }

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if(getIntent().getBooleanExtra(CMain.IS_MONITORING, false)){
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = FragmentNotifikasi.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            navigationView.setCheckedItem(R.id.nav_notifikasi);
        }else if (savedInstanceState == null) {
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = FragmentMonitoring.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            navigationView.setCheckedItem(R.id.nav_monitoring);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        Class fragmentClass = null;

        if (id == R.id.nav_monitoring) {
            fragmentClass = FragmentMonitoring.class;
        } else if (id == R.id.nav_notifikasi) {
            fragmentClass = FragmentNotifikasi.class;
        }
//        else if(id == R.id.nav_total_notifikasi){
//            DBHelper mydb = new DBHelper(MainActivityMonitor.this);
//            //mydb = new DBHelper(this);
//            //mydb.deleteAllNotifikasi();
//            Toast.makeText(MainActivityMonitor.this,"total "+mydb.totalNotifikasi(), Toast.LENGTH_SHORT).show();
//            return true;
//        }
        else{
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }

        if(fragmentClass!=null) {
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }

        if (id == R.id.nav_logout) {
            ConnectivityManager cm =
                    (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting();
            if(isConnected) {
                logout();
            }else{
                Toast.makeText(MainActivityMonitor.this, CLogin.notif_butuh_koneksi, Toast.LENGTH_LONG).show();
            }
            return true;
        }
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
    private void logout(){
        final ProgressDialog loading = ProgressDialog.show(this,"Logout...","Mohon tunggu...",false,false);
        loading.show();
        String url ="";
        if(pref.getString(GlobalConfig.IP_KEY, null) != null){
            url = "http://"+pref.getString(GlobalConfig.IP_KEY, "")+GlobalConfig.WEB_URL+CMain.URL_LOGOUT;
        }else{
            url = "http://"+GlobalConfig.IP+GlobalConfig.WEB_URL+CMain.URL_LOGOUT;
        }
        Log.d(GlobalConfig.TAG,""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(CLogin.USER_ID, pref.getString(CLogin.G_ID_USER,""));
            jsonBody.put(CLogin.USER_REGID, pref.getString(GlobalConfig.gcmregId,""));
            // other key-value pairs...

            Log.d(GlobalConfig.TAG, jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();

                    try {
                        int status = response.getInt("status");
                        if(status==1){
                            hapusPreferences();
                        }else{
                            Toast.makeText(getApplicationContext(), CLogin.notif_form_tidak_cocok, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    loading.dismiss();
                    Log.d("respons",error.getMessage().toString());
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_NAME, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            //Adding request to the queue
            // Adding request to request queue
            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            super.onActivityResult(requestCode, resultCode, data);
            Log.d("receive  result", "result c : " + resultCode + ", definition" + RESULT_OK);
        }
//        if(requestCode == CMonitor.REQUEST_CODE_MENU_FILTER && resultCode == RESULT_OK) {
//            Log.d(GlobalConfig.TAG, "dari menu filter");
//            android.app.Fragment fragment = getFragmentManager().findFragmentById(R.id.flContent);
//            fragment.onActivityResult(requestCode, resultCode, data);
//        }
    }
    public void setActionBarTitle(String title){
        actionBar = getSupportActionBar();
        actionBar.setTitle(title);
    }
    private void hapusPreferences(){
        pref.edit().clear().commit();

        Intent login = new Intent(MainActivityMonitor.this, Login.class);

        this.startActivity(login);
        this.finish();
        Log.d("logout", "open login");
    }
}
