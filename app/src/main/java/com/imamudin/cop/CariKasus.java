package com.imamudin.cop;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.imamudin.cop.config.CInterogasi;
import com.imamudin.cop.config.CLogin;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.mysp.ObscuredSharedPreferences;
import com.imamudin.cop.app.MyAppController;
import com.imamudin.cop.listAdapter.ListAdapterCariKasus;
import com.imamudin.cop.model.CariKasusItem;

/**
 * Created by agung on 12/03/2016.
 */
public class CariKasus extends AppCompatActivity
{
    // Movies json url
    String keyword, keyword_jenis;
    ObscuredSharedPreferences pref;

    //variabel untuk list view
    private List<CariKasusItem> cariKasusList = new ArrayList<CariKasusItem>();
    private ListView listView;
    private ListAdapterCariKasus adapter;

    private SwipeRefreshLayout swipeContainer;

    int offSet=0;

    Runnable runnable;
    Boolean disableSwipeDown = false;       //untuk mendisable swipe down list view

    //untuk handler AsyncTask
    protected static final int MSG_REGISTER_WEB_SERVER_SUCCESS = 103;
    protected static final int MSG_REGISTER_WEB_SERVER_FAILURE = 104;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cari_kasus);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Pilih Kasus");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        Intent intent = getIntent();
        keyword         = intent.getStringExtra(CInterogasi.KEYWORD);
        keyword_jenis   = intent.getStringExtra(CInterogasi.KEYWORD_JENIS);

        //untuk list view
        listView = (ListView)findViewById(R.id.lv_cari_kasus);
        cariKasusList.clear();
        adapter = new ListAdapterCariKasus(CariKasus.this, cariKasusList);
        listView.setAdapter(adapter);

        //untuk swipelist
        swipeContainer = (SwipeRefreshLayout)findViewById(R.id.swipeContainer);

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                //fetchTimelineAsync(0);
                cariKasusList.clear();
                adapter.notifyDataSetChanged();
                callNews(0);
                Toast.makeText(CariKasus.this,"refresh",Toast.LENGTH_LONG).show();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected item text from ListView
                Object o = listView.getItemAtPosition(position);
                CariKasusItem kasusItem = (CariKasusItem) o;

                //return value to before Activity
                Intent output = new Intent();
                output.putExtra(CInterogasi.KASUS_ID, kasusItem.getKasus_id());
                output.putExtra(CInterogasi.KASUS_NAMA, kasusItem.getNama_kasus());
                setResult(RESULT_OK, output);
                finish();
            }
        });

        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
                cariKasusList.clear();
                adapter.notifyDataSetChanged();
                callNews(0);
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }
            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {

                    if(!disableSwipeDown) {
                        swipeContainer.setRefreshing(true);
                        handler = new Handler();

                        runnable = new Runnable() {
                            public void run() {
                                callNews(offSet);

                            }
                        };
                        //untuk menerlambatkan 1 detik
                        handler.postDelayed(runnable, 1000);
                    }else{
                        //Toast.makeText(CariKasus.this,"Data telah ditampilkan semua.",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }
    private void callNews(int page){
        swipeContainer.setRefreshing(true);

        // Creating volley request obj
        String url ="";
        if(pref.getString(GlobalConfig.IP_KEY, null) != null){
            url = "http://"+pref.getString(GlobalConfig.IP_KEY, "")+GlobalConfig.WEB_URL+ CInterogasi.URL_CARI_KASUS;
        }else{
            url = "http://"+GlobalConfig.IP+GlobalConfig.WEB_URL+ CInterogasi.URL_CARI_KASUS;
        }
        Log.d(GlobalConfig.TAG, url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(CInterogasi.KEYWORD, keyword);
            jsonBody.put(CInterogasi.KEYWORD_JENIS, keyword_jenis);
            jsonBody.put(CInterogasi.OFFSET, ""+page);
            jsonBody.put(CInterogasi.USER_ID, ""+pref.getString(CLogin.G_ID_USER,""));
            jsonBody.put(CInterogasi.USER_REGID, ""+pref.getString(GlobalConfig.gcmregId,""));

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        int status  = response.getInt("status");
                        String pesan= response.getString("pesan");
                        Log.d(GlobalConfig.TAG, ""+status);
                        if(status==1){
                            Log.d(GlobalConfig.TAG, ""+status);
                            JSONArray kasuss = response.getJSONArray("data");
                            if(kasuss.length()<50){         //50 dari jumlah limit di web
                                disableSwipeDown = true;
                            }

                            offSet = offSet+kasuss.length();

                            if(kasuss.length()>0) {
                                for (int i = 0; i < kasuss.length(); i++) {
                                    JSONObject kasus = kasuss.getJSONObject(i);
                                    //Log.d(TAG + "kasus_nama", "" + kasus.get("kasus_nama"));

                                    CariKasusItem cariKasusItem = new CariKasusItem();
                                    cariKasusItem.setNama_kasus(kasus.getString(CInterogasi.KASUS_NAMA));
                                    cariKasusItem.setKasus_id(kasus.getString(CInterogasi.KASUS_ID));
                                    cariKasusItem.setNo_lp(kasus.getString(CInterogasi.KASUS_NO_LP));
                                    cariKasusItem.setTgl_kasus(kasus.getString(CInterogasi.KASUS_TANGGAL));
                                    cariKasusItem.setNama_pelapor(kasus.getString(CInterogasi.KASUS_NAMA_PELAPOR));

                                    // adding news to news array
                                    cariKasusList.add(cariKasusItem);

                                    swipeContainer.setRefreshing(false);

                                    adapter.notifyDataSetChanged();
                                }
                            }else{
                                Toast.makeText(getApplicationContext(),"Kasus tidak ditemukan.", Toast.LENGTH_SHORT).show();
                                swipeContainer.setRefreshing(false);
                            }
                        }else{
                            swipeContainer.setRefreshing(false);
                            Toast.makeText(getApplicationContext(),pesan , Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        swipeContainer.setRefreshing(false);
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    swipeContainer.setRefreshing(false);
                    Log.d("respons",error.getMessage().toString());
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_NAME, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            //Creating a Request Queue
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            //Adding request to the queue
            // Adding request to request queue
            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_WEB_SERVER_SUCCESS:
                    Toast.makeText(getApplicationContext(),
                            "Tidak mendapatkan respon.", Toast.LENGTH_LONG).show();
                    break;
                case MSG_REGISTER_WEB_SERVER_FAILURE:
                    Toast.makeText(getApplicationContext(),
                            "Tidak dapat mengakses server.",
                            Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };
}