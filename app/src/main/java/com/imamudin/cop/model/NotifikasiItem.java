package com.imamudin.cop.model;

/**
 * Created by imamudin on 11/11/16.
 */
public class NotifikasiItem {
    int id, berkas_ke;
    String namaPolisi, jenisBerkas, namaKasus, nrp, kasus_id, waktu;

    public NotifikasiItem(int id, String namaPolisi, String jenisBerkas, String namaKasus, String nrp, String kasus_id, String waktu, int berkas_ke) {
        this.id = id;
        this.namaPolisi = namaPolisi;
        this.jenisBerkas = jenisBerkas;
        this.namaKasus = namaKasus;
        this.nrp = nrp;
        this.kasus_id = kasus_id;
        this.waktu = waktu;
        this.berkas_ke = berkas_ke;
    }

    public int getId() {
        return id;
    }

    public int getBerkas_ke() {
        return berkas_ke;
    }

    public String getNamaPolisi() {
        return namaPolisi;
    }

    public String getJenisBerkas() {
        return jenisBerkas;
    }

    public String getNamaKasus() {
        return namaKasus;
    }

    public String getNrp() {
        return nrp;
    }

    public String getKasus_id() {
        return kasus_id;
    }

    public String getWaktu() {
        return waktu;
    }
}
