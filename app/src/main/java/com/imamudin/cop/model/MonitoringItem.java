package com.imamudin.cop.model;

/**
 * Created by agung on 13/04/2016.
 */
public class MonitoringItem {
    private String id_kasus, id_doc_opsnal, tgl_ba, nrp, user_nama, nama_kasus, no_lp, nama_pelapor, nama_penyidik;
    private int berkas_ke;

    public String getId_kasus() {
        return id_kasus;
    }

    public void setId_kasus(String id_kasus) {
        this.id_kasus = id_kasus;
    }

    public String getId_doc_opsnal() {
        return id_doc_opsnal;
    }

    public void setId_doc_opsnal(String id_doc_opsnal) {
        this.id_doc_opsnal = id_doc_opsnal;
    }

    public String getTgl_ba() {
        return tgl_ba;
    }

    public void setTgl_ba(String tgl_ba) {
        this.tgl_ba = tgl_ba;
    }

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public String getUser_nama() {
        return user_nama;
    }

    public void setUser_nama(String user_nama) {
        this.user_nama = user_nama;
    }

    public String getNama_kasus() {
        return nama_kasus;
    }

    public void setNama_kasus(String nama_kasus) {
        this.nama_kasus = nama_kasus;
    }

    public String getNo_lp() {
        return no_lp;
    }

    public void setNo_lp(String no_lp) {
        this.no_lp = no_lp;
    }

    public String getNama_penyidik() {
        return nama_penyidik;
    }

    public void setNama_penyidik(String nama_penyidik) {
        this.nama_penyidik = nama_penyidik;
    }

    public String getNama_pelapor() {
        return nama_pelapor;

    }

    public void setNama_pelapor(String nama_pelapor) {
        this.nama_pelapor = nama_pelapor;
    }

    public int getBerkas_ke() {
        return berkas_ke;
    }

    public void setBerkas_ke(int berkas_ke) {
        this.berkas_ke = berkas_ke;
    }
}
