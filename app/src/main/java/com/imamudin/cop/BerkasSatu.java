package com.imamudin.cop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import com.imamudin.cop.config.CInterogasi;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.zoomimage.MainZoomImage;

import org.json.JSONObject;

/**
 * Created by agung on 23/02/2016.
 */
public class BerkasSatu extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        ResultCallback<LocationSettingsResult> {

    Button btnTambahPertanyaan, btnSimpan, btnLokasi;
    ImageButton btnTambahGambar;
    LinearLayout llPertanyaan, llGambar;
    TextView tv_kasus_id, tv_kasus_nama, tv_keterangan_gambar;
    ImageView img_kasus_nama, img_nama, img_alamat, img_lokasi;
    EditText et_nama, et_lokasi, et_alamat;

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_GALLERY = 2;
    static final int MAX_FOTO = 5;
    String imgDecodableString;                                              //untuk mengambil image dari gallery

    SharedPreferences prefs;

    //untuk TextView warning
    TextView tv_warning_nama_kasus, tv_warning_lokasi, tv_warning_nama, tv_warning_alamat, tv_warning_pertanyaan, tv_warning_koordinat;

    protected static final int MY_SOCKET_TIMEOUT_MS             = 10000;

    //untuk mendapatakan lokasi
    protected static final int REQUEST_CHECK_SETTINGS           = 20;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS    = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    protected final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    protected final static String KEY_LOCATION = "location";
    protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";

    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected Location mCurrentLocation;

    // UI Widgets.
    protected TextView mLatLongLabel;
    protected TextView mLatLong;
    ProgressDialog pDialog=null;

    protected Boolean mRequestingLocationUpdates;
    protected String mLastUpdateTime;

    public String pictureImagePath="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.berkas_interogasi);

        init();

        //inisialisasi getLokasi
        getLokasiInit();
    }

    private void init(){
        //inisialisasi variabel
        btnTambahPertanyaan = (Button) findViewById(R.id.btn_tambah_pertanyaan);
        btnTambahGambar = (ImageButton) findViewById(R.id.btn_tambah_gambar);
        btnSimpan = (Button) findViewById(R.id.btn_simpan);
        btnLokasi = (Button) findViewById(R.id.btnLokasi);

        llPertanyaan = (LinearLayout) findViewById(R.id.ll_pertanyaan);
        llGambar = (LinearLayout) findViewById(R.id.ll_tambah_gambar_dalam);

        tv_kasus_id = (TextView) findViewById(R.id.tv_kasus_id);
        tv_kasus_nama = (TextView) findViewById(R.id.tv_kasus_nama);
        tv_keterangan_gambar = (TextView) findViewById(R.id.tv_keterangan_gambar);

        img_kasus_nama = (ImageView) findViewById(R.id.img_kasus_nama);
        img_lokasi = (ImageView) findViewById(R.id.img_kasus_lokasi);
        img_alamat = (ImageView) findViewById(R.id.img_alamat);
        img_nama = (ImageView) findViewById(R.id.img_nama);

        et_nama = (EditText) findViewById(R.id.et_nama);
        et_lokasi = (EditText) findViewById(R.id.et_lokasi);
        et_alamat = (EditText) findViewById(R.id.et_alamat);

        et_nama.setOnFocusChangeListener(NoFocus);
        et_lokasi.setOnFocusChangeListener(NoFocus);
        et_alamat.setOnFocusChangeListener(NoFocus);


        tv_warning_nama_kasus   = (TextView)findViewById(R.id.tv_notif_nama_kasus);
        tv_warning_lokasi       = (TextView)findViewById(R.id.tv_notif_lokasi);
        tv_warning_nama         = (TextView)findViewById(R.id.tv_notif_nama);
        tv_warning_alamat       = (TextView)findViewById(R.id.tv_notif_alamat);
        tv_warning_pertanyaan   = (TextView)findViewById(R.id.tv_keterangan_pertanyaan);
        tv_warning_koordinat    = (TextView)findViewById(R.id.tv_notif_koordinate);

        //konfigurasi toolbar
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Form Investigasi");
        //actionBar.setIcon(R.drawable.search_24);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);
        //toolbar.setTitleTextColor(getResources().getColor(R.color.putih));

        btnTambahPertanyaan.setOnClickListener(btnClick);
        btnLokasi.setText("Cari Lokasi");
        btnLokasi.setOnClickListener(btnClick);
        btnTambahGambar.setOnClickListener(btnClick);
        btnSimpan.setOnClickListener(btnClick);
    }

    EditText.OnFocusChangeListener NoFocus= new View.OnFocusChangeListener(){
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(v==et_nama){
                if(!hasFocus){
                    showMessage(et_nama, img_nama, tv_warning_nama);
                    //Toast.makeText(getApplicationContext(),""+et_nama.getText().toString(),Toast.LENGTH_SHORT).show();
                }
            }else if(v==et_alamat){
                if(!hasFocus){
                    showMessage(et_alamat, img_alamat, tv_warning_alamat);
                    //Toast.makeText(getApplicationContext(),""+et.getText().toString(),Toast.LENGTH_SHORT).show();
                }
            }else if(v==et_lokasi){
                if(!hasFocus){
                    showMessage(et_lokasi, img_lokasi, tv_warning_lokasi);
                    //Toast.makeText(getApplicationContext(),""+et_nama.getText().toString(),Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    public void showMessage(final EditText et, final ImageView iv, final TextView tv){
        if(et.getText().toString().length()<=0){
            iv.setImageDrawable(ContextCompat.getDrawable(BerkasSatu.this, R.drawable.ic_error));
            tv.setVisibility(View.VISIBLE);
        }else{
            iv.setImageDrawable(ContextCompat.getDrawable(BerkasSatu.this, R.drawable.ic_done));
            tv.setVisibility(View.GONE);
        }
    }


    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //untuk menambah gambar
            if (v == btnTambahGambar) {
                if (llGambar.getChildCount() < MAX_FOTO) {
                    showdialogFoto();
                } else {
                    Toast.makeText(getApplicationContext(), "Jumlah foto maksimal " + MAX_FOTO, Toast.LENGTH_SHORT).show();
                }
            }
            //untuk menyimpan semua data
            else if(v==btnSimpan){
                btnSimpan.setEnabled(false);
                simpanForm();
            }
            //untuk mendapatkan lokasi
            else if(v==btnLokasi){
                //getLokasi();
                //startActivity(new Intent(BerkasSatu.this, MainLocation.class));
                checkLocationSettings();
            }
            //untuk menambah pertanyaan
            else if(v==btnTambahPertanyaan){
                showdialogTambahPertanyaan();
            }
        }
    };
    public void simpanForm(){
        //untuk kasus id
        final String kasus_id = tv_kasus_id.getText().toString().trim();

        //untuk lokasi TKP, nama, alamat
        final String lokasi   = et_lokasi.getText().toString().trim();
        final String nama     = et_nama.getText().toString().trim();
        final String alamat   = et_alamat.getText().toString().trim();

        //inisialisai pertanyaan jawab
        final List<String> tanya = new ArrayList<String>();
        final List<String> jawab = new ArrayList<String>();
        jawab.clear();
        tanya.clear();

        //untuk mendapatkan pertanyaan
        //linear layout level 1
        int total1 = 0, total2 = 0, total3 = 0;
        int count = llPertanyaan.getChildCount();
        View vChild = null;
        Log.d(GlobalConfig.TAG+"tanya : total ", "" + count);
        for (int i = 0; i < count; i++) {
            vChild = llPertanyaan.getChildAt(i);
            //linear layout level2
            if (vChild instanceof LinearLayout) {
                View vChild2 = null;
                total2 = ((LinearLayout) vChild).getChildCount();
                //Log.d(GlobalConfig.TAG+"tanya : total2 ",""+total2);
                for (int j = 0; j < ((LinearLayout) vChild).getChildCount(); j++) {
                    vChild2 = ((LinearLayout) vChild).getChildAt(j);
                    //linear layout level3
                    if (vChild2 instanceof LinearLayout) {
                        View vChild3 = null;
                        total3 = ((LinearLayout) vChild2).getChildCount();
                        //Log.d(GlobalConfig.TAG+"tanya : total3 ",""+total3);

                        //disini pada vchild2(1) lokasi text view pertanyaan dan jawab
                        View vchild_tanya = null;
                        vchild_tanya = ((LinearLayout) vChild2).getChildAt(1);

                        //tanya
                        vChild3 = ((LinearLayout) vchild_tanya).getChildAt(0);
                        tanya.add(((TextView) vChild3).getText().toString().trim());
                        Log.d(GlobalConfig.TAG+"tanya : ", "" + tanya.get(0));
                        //jawab
                        vChild3 = ((LinearLayout) vchild_tanya).getChildAt(1);
                        jawab.add(((TextView) vChild3).getText().toString().trim());
                        Log.d(GlobalConfig.TAG+"jawab : ", "" + jawab.get(0));
                    }
                }
            }
        }
        //untuk gambar
        List<String> img_path = new ArrayList<String>();
        img_path.clear();
        Log.d(GlobalConfig.TAG+"gambar : total ", "" + count);
        for (int i = 0; i < llGambar.getChildCount(); i++) {
            vChild = llGambar.getChildAt(i);
            if (vChild instanceof LinearLayout) {
                total1 = ((LinearLayout) vChild).getChildCount();
                Log.d(GlobalConfig.TAG+"view dalam : total ", "" + total1);

                //get child indeks ke 0 karena path berada pada awal
                View vChild2 = ((LinearLayout) vChild).getChildAt(0);
                if(vChild2 instanceof TextView){
                    Log.d(GlobalConfig.TAG+"img_path", "" + ((TextView) vChild2).getText().toString().trim());
                    img_path.add(((TextView) vChild2).getText().toString().trim());
                }
            }
        }

        final List<String> img_string = new ArrayList<String>();
        img_string.clear();
        for(int i=0;i<img_path.size();i++){
            Bitmap bmp = decodeSampledBitmapFromFile(img_path.get(i), 1680, 960);
            img_string.add(getStringImage(bmp));
        }

        final String latlong = mLatLong.getText().toString().trim();

        Log.d(GlobalConfig.TAG+"kasus_id",kasus_id);
        Log.d(GlobalConfig.TAG+"nama",nama);
        Log.d(GlobalConfig.TAG+"lokasi",lokasi);
        Log.d(GlobalConfig.TAG+"alamat",alamat);
        for(int i=0;i<tanya.size();i++){
            Log.d(GlobalConfig.TAG+"tanya"+i,tanya.get(i));
            Log.d(GlobalConfig.TAG+"jawab"+i,jawab.get(i));
        }
        for(int i =0; i<img_string.size();i++){
            Log.d(GlobalConfig.TAG+"gambar"+i,img_string.get(i));
        }
        Log.d(GlobalConfig.TAG+"latlong :",latlong);

        //cek require form
        boolean success = true;

        if(llPertanyaan.getChildCount()<1){
            tv_warning_pertanyaan.setText("Minimal 1 pertanyaan!");
            tv_warning_pertanyaan.setTextColor(Color.RED);
            tv_warning_pertanyaan.setFocusable(true);
            success = false;
        }
        if(alamat.length()<=0){
            tv_warning_alamat.setVisibility(View.VISIBLE);
            et_alamat.setFocusable(true);
            img_alamat.setImageDrawable(ContextCompat.getDrawable(BerkasSatu.this, R.drawable.ic_error));
            success = false;
        }
        if(nama.length()<=0){
            tv_warning_nama.setVisibility(View.VISIBLE);
            et_nama.setFocusable(true);
            img_nama.setImageDrawable(ContextCompat.getDrawable(BerkasSatu.this, R.drawable.ic_error));
            success = false;
        }
        if(lokasi.length()<=0){
            tv_warning_lokasi.setVisibility(View.VISIBLE);
            et_lokasi.setFocusable(true);
            img_lokasi.setImageDrawable(ContextCompat.getDrawable(BerkasSatu.this, R.drawable.ic_error));
            success = false;
        }
        if(kasus_id.length()<=0){
            tv_warning_nama_kasus.setVisibility(View.VISIBLE);
            tv_kasus_nama.setFocusable(true);
            img_kasus_nama.setImageDrawable(ContextCompat.getDrawable(BerkasSatu.this, R.drawable.ic_error));
            success = false;
        }
        if(mLatLong.getText().toString().trim().length()<=0){
            tv_warning_koordinat.setVisibility(View.VISIBLE);
            success = false;
        }

        if(success){
            //upload
            final ProgressDialog loading = ProgressDialog.show(this,"Mengirim berkas...","Mohon tunggu...",false,false);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, getUrl(CInterogasi.URL_SIMPAN),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            //Disimissing the progress dialog
                            loading.dismiss();

                            Log.d(GlobalConfig.TAG,""+s);
                            try {
                                JSONObject jsonResponse = new JSONObject(s);

                                int status = jsonResponse.optInt(CInterogasi.STATUS);
                                String message = jsonResponse.optString(CInterogasi.MESSAGE);

                                if (status == 1) {
                                    closeActivity();
                                }
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            }catch (Exception e){
                                Toast.makeText(getApplicationContext(),""+e.toString(),Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            //Dismissing the progress dialog
                            loading.dismiss();

                            if(volleyError != null){
                                Log.d(GlobalConfig.TAG,""+volleyError.toString());
                            }
                            //Showing toasts
                            //Toast.makeText(getApplicationContext(), volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                        }
                    }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    //Creating parameters
                    Map<String,String> params = new Hashtable<String, String>();

                    //Adding parameters
                    params.put(CInterogasi.USER_ID, ""+getSharedPreferences().getString(CInterogasi.USER_ID,""));
                    params.put(CInterogasi.USER_REGID, ""+getSharedPreferences().getString(CInterogasi.USER_REGID,""));

                    params.put(CInterogasi.KEYUP_KASUS_ID, ""+ kasus_id);
                    params.put(CInterogasi.KEYUP_TKP, ""+ lokasi);
                    params.put(CInterogasi.KEYUP_NAMA, ""+ nama);
                    params.put(CInterogasi.KEYUP_ALAMAT, ""+ alamat);
                    for(int i=0; i<tanya.size();i++){
                        params.put(CInterogasi.KEYUP_TANYA+"["+i+"]", ""+ tanya.get(i));
                        params.put(CInterogasi.KEYUP_JAWAB+"["+i+"]", ""+ jawab.get(i));
                    }
                    for(int i=0; i<img_string.size();i++){
                        params.put(CInterogasi.KEYUP_GAMBAR+"["+i+"]", ""+ img_string.get(i));
                    }
                    params.put(CInterogasi.KEYUP_KOORDINAT, ""+latlong);

                    //returning parameters
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            //Creating a Request Queue
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            //Adding request to the queue
            requestQueue.add(stringRequest);
        }else{
            Toast.makeText(getApplicationContext(),"Isi semua form!", Toast.LENGTH_SHORT).show();
        }
        btnSimpan.setEnabled(true);
    }
    public String getUrl(String url){
        String uri = getSharedPreferences().getString(GlobalConfig.IP_KEY, "");
        if(uri.length()==0){
            uri = GlobalConfig.IP;
        }
        uri = "http://" + uri + "" + url;
        return uri;
    }
    private SharedPreferences getSharedPreferences() {
        if (prefs == null) {
            prefs = getApplicationContext().getSharedPreferences(
                    GlobalConfig.KEY_PREFERENCES, Context.MODE_PRIVATE);
        }
        return prefs;
    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
    @Override
    public void onBackPressed() {
        // Write your code here
        Log.d(GlobalConfig.TAG,llPertanyaan.getChildCount()+"|"+llGambar.getChildCount());
        if(llPertanyaan.getChildCount()>0 || et_alamat.getText().toString().trim().length()>0 || et_lokasi.getText().toString().trim().length()>0
                || et_nama.getText().toString().trim().length()>0 || tv_kasus_nama.getText().toString().trim().length()>0
                || llGambar.getChildCount()>0 || mLatLong.getText().toString().length()>0){
            showdialogBackPress();
        }else{
            closeActivity();
        }
    }
    public void closeActivity(){
        super.onBackPressed();
    }
    //untuk menampilkan notifikasi keluar
    private void showdialogBackPress(){
        LayoutInflater layoutInflater = (LayoutInflater)getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.notif_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BerkasSatu.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title  = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final TextView t_text   = (TextView) promptView.findViewById(R.id.t_text_dialog);
        t_title.setText("Peringatan");
        t_text.setText("Data belum tersimpan!.\nApakah anda yakin tidak menyimpannya?");
        // setup a dialog window
        alertDialogBuilder.setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        closeActivity();
                    }
                })
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    //untuk mendapatakan lokasi longitude latitude
    public void getLocation() {
        // Get the location manager
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(bestProvider);
        Double lat,lon;
        try {
            lat = location.getLatitude ();
            lon = location.getLongitude ();
            Toast.makeText(getApplicationContext(), lat+" "+lon,Toast.LENGTH_SHORT).show();
            //return new LatLng(lat, lon);
        }
        catch (NullPointerException e){
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), " "+e.getMessage(),Toast.LENGTH_SHORT).show();
            //return null;
        }
    }
    //untuk membuka intent mengambil foto dari kamera
    private void loadImagefromKamera() {
        createFolder();
        pictureImagePath = getNewImagePath();
        File file = new File(pictureImagePath);
        myScanFile(this.pictureImagePath);
        Uri outputFileUri = Uri.fromFile(file);
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        Log.d(GlobalConfig.TAG+"mycop",""+pictureImagePath);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
    //untuk membuka intent mengambil foto dari gallery
    public void loadImagefromGallery() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, REQUEST_IMAGE_GALLERY);
    }

    //membuat nama file baru
    private String getNewImagePath() {
        String folder = Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_DCIM + File.separator + GlobalConfig.FOLDER_NAMA;
        pictureImagePath = new File(folder).getAbsolutePath() + File.separator + (new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".jpg");
        File file = new File(pictureImagePath);
        myScanFile(this.pictureImagePath);
        return this.pictureImagePath;
    }
    //membuat folder cop
    private void createFolder() {
        String folder = Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_DCIM + File.separator + GlobalConfig.FOLDER_NAMA;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File directory = new File(folder);
            if (!directory.exists()) {
                directory.mkdirs();
                Log.d(GlobalConfig.TAG+"mycopcamera", "" + directory.toString());
                return;
            }
            return;
        }
    }
    //memasukan file kedalam gallery
    private void myScanFile(String path) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.DATA, path);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        getApplication().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }
    public void showdialogCariKasus() {           //untuk mencari kasus
        LayoutInflater layoutInflater = (LayoutInflater) getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.dialog_cari_kasus, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BerkasSatu.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title  = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final RadioGroup jenis  = (RadioGroup)promptView.findViewById(R.id.radiojenis);
        final EditText t_cari   = (EditText) promptView.findViewById(R.id.et_ip);

        final RadioButton rb_no_lp  = (RadioButton)promptView.findViewById(R.id.radio_nomor_lp);
        final RadioButton rb_nama_pelapor  = (RadioButton)promptView.findViewById(R.id.radio_nama_pelapor);

        t_title.setText("Cari Kasus");

        // setup a dialog window
        alertDialogBuilder.setCancelable(true)
                .setPositiveButton("Cari",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                int selectedId  = jenis.getCheckedRadioButtonId();
                                String sjenis   ="";       //untuk menyimpan jenis pencarian

                                if(selectedId==rb_no_lp.getId()){
                                    sjenis = CInterogasi.NO_LP;
                                }else
                                    sjenis = CInterogasi.NAMA_PELAPOR;

                                Intent iCariKasus = new Intent(BerkasSatu.this,CariKasus.class);
                                iCariKasus.putExtra(CInterogasi.KEYWORD_JENIS,sjenis);
                                iCariKasus.putExtra(CInterogasi.KEYWORD, t_cari.getText().toString());
                                startActivityForResult(iCariKasus, CInterogasi.KODE_BERKAS1_CARIKASUS);

//                                if (!t_cari.getText().toString().trim().equals("")) {
//                                    //buka activity cari kasus
//                                    //melihat jenis pencarian
//                                }else{
//                                    Toast.makeText(BerkasSatu.this, "Masukan kata kunci!" + t_cari.getText(), Toast.LENGTH_LONG).show();
//                                }
                            }
                        })
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        //jika ingin menghapus kasus
        if(tv_kasus_nama.getText().toString().trim()!="") {
            alertDialogBuilder.setNeutralButton("Hapus Kasus",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            tv_kasus_id.setText("");

                            tv_kasus_nama.setText("Pilih kasus");
                            tv_kasus_nama.setTextColor(Color.RED);
                            img_kasus_nama.setImageDrawable(ContextCompat.getDrawable(BerkasSatu.this, R.drawable.ic_error));
                        }
                    });
        }
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CInterogasi.KODE_BERKAS1_CARIKASUS && resultCode == RESULT_OK && data != null) {
            String kasus_nama = data.getStringExtra(CInterogasi.KASUS_NAMA);
            String kasus_id = data.getStringExtra(CInterogasi.KASUS_ID);

            tv_kasus_id.setText(""+kasus_id);
            tv_kasus_nama.setText(""+kasus_nama);

            img_kasus_nama.setImageDrawable(ContextCompat.getDrawable(BerkasSatu.this, R.drawable.ic_done));
            tv_warning_nama_kasus.setVisibility(View.GONE);
        }
        //untuk menangkap image dari kamera
        else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bitmap bitmapForImage = null;
            File file = new File(pictureImagePath);
            Log.d(GlobalConfig.TAG+"mycopcamera", pictureImagePath);
            if (file.exists()) {
                bitmapForImage = decodeSampledBitmapFromFile(pictureImagePath, 300, 300);
            }
            add_layout_gambar(bitmapForImage, pictureImagePath);
        }
        //untuk menagkap image dari gallery
        else if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK&& null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imgDecodableString = cursor.getString(columnIndex);

            String lokasi_baru = getNewImagePath();
            //copy image to folder cop
            try {
                File source = new File(imgDecodableString);
                File destination = new File(lokasi_baru);
                if (source.exists()) {
                    FileChannel src = new FileInputStream(source).getChannel();
                    FileChannel dst = new FileOutputStream(destination).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            } catch (Exception e) {
                Log.d(GlobalConfig.TAG+"COPerror", "" + e.getMessage());
            }
            Bitmap bitmapForImage = decodeSampledBitmapFromFile(lokasi_baru, 300, 300);
            cursor.close();
            add_layout_gambar(bitmapForImage, lokasi_baru);
        }else if(requestCode == CInterogasi.KODE_ZOOM_IMAGE && resultCode == RESULT_OK&& null != data){
            if (data.getIntExtra(CInterogasi.KODE_ZOOM_STATUS, CInterogasi.KODE_ZOOM_HAPUS) == CInterogasi.KODE_ZOOM_HAPUS) {
                hapus_gambar(data.getIntExtra(CInterogasi.IMG_IDCHILD, 0));
            }
        }else if(requestCode == REQUEST_CHECK_SETTINGS ){
            if(resultCode == Activity.RESULT_OK){
                Log.i(getLocalClassName(), "User agreed to make required location settings changes.");
                startLocationUpdates();
            }else if(resultCode==Activity.RESULT_CANCELED){
                Log.i(getLocalClassName(), "User chose not to make required location settings changes.");
            }

        }
    }
    //untuk menghapus gambar
    private void hapus_gambar(int idChild) {
        Log.d(GlobalConfig.TAG+"mycop", "idchild " + idChild);
        for (int i = 0; i < this.llGambar.getChildCount(); i ++) {
            if (i == idChild) {
                Log.d(GlobalConfig.TAG+"mycop", "hapus view " + idChild);
                View view = llGambar.getChildAt(i);
                if (view instanceof LinearLayout) {
                    ((ViewGroup) view.getParent()).removeView(view);
                    update_keterangan();
                }
            }
        }
    }
    //memperkecil ukuran gambar
    public static Bitmap decodeSampledBitmapFromFile(String imagePath, int reqWidth, int reqHeight) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(imagePath, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        int min = width;
        if (width > height) {
            int temp = reqHeight;
            reqHeight = reqWidth;
            reqWidth = temp;
        }
        Log.d(GlobalConfig.TAG+"copcamera1/", "height : " + height + ", width : " + width);
        if (height > reqHeight || width > reqWidth) {
            int halfHeight = height;
            int halfWidth = width;
            while (true) {
                if (halfHeight / inSampleSize <= reqHeight && halfWidth / inSampleSize <= reqWidth) {
                    break;
                }
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }
    public void add_layout_gambar(Bitmap imageBitmap, final String path){
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        final View inflatedLayout= inflater.inflate(R.layout.view_gambar, llGambar, false);

        final ImageView img_berkas  = (ImageView) inflatedLayout.findViewById(R.id.img_berkas);
        final TextView img_path     = (TextView) inflatedLayout.findViewById(R.id.img_path);

        img_berkas.setImageBitmap(imageBitmap);
        img_berkas.setClickable(true);
        img_path.setText(path);

        img_berkas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImage(v, path, (llGambar.getChildCount() - 1));
            }
        });

        llGambar.addView(inflatedLayout);
        update_keterangan();
    }
    public void update_keterangan(){
        int count = llGambar.getChildCount();
        String keterangan="";
        if(count>0){
            keterangan = "Ditambahkan "+count+" dari "+MAX_FOTO+" foto";
        }else{
            keterangan = "Pilih "+MAX_FOTO+" foto";
        }
        tv_keterangan_gambar.setText(keterangan);
    }
    public void showImage(View v, String path, int idChild) {
        Intent img_fullscreen = new Intent(getApplicationContext(), MainZoomImage.class);
        img_fullscreen.putExtra(CInterogasi.IMG_PATH, path);
        img_fullscreen.putExtra(CInterogasi.IMG_IDCHILD, idChild);
        Log.d(GlobalConfig.TAG+"copcamera0", path);
        startActivityForResult(img_fullscreen, CInterogasi.KODE_ZOOM_IMAGE);
    }
    public void showdialogFoto(){
        // get prompts.xml view
        LayoutInflater layoutInflater = (LayoutInflater)getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.dialog_pilih_foto, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BerkasSatu.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title  = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final LinearLayout t_kamera   = (LinearLayout) promptView.findViewById(R.id.t_kamera);
        final LinearLayout t_galeri   = (LinearLayout) promptView.findViewById(R.id.t_galeri);

        t_title.setText("Pilih Foto");


        final AlertDialog alert = alertDialogBuilder.create();

        t_kamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadImagefromKamera();
                alert.cancel();
            }
        });
        t_galeri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadImagefromGallery();
                alert.cancel();
            }
        });
        alert.show();
    }
    //untuk mengambil foto dari galery

    public void showdialogTambahPertanyaan() {
        LayoutInflater layoutInflater = (LayoutInflater) getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.dialog_pertanyaan, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BerkasSatu.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final EditText et_pertanyaan= (EditText) promptView.findViewById(R.id.et_dialog_pertanyaan);
        final EditText et_jawaban   = (EditText) promptView.findViewById(R.id.et_dialog_jawaban);

        t_title.setText("Pertanyaan");

        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Simpan",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String s_jawab  = et_jawaban.getText().toString().trim();
                                String s_tanya  = et_pertanyaan.getText().toString().trim();
                                if (!s_jawab.trim().equals("") && !s_tanya.trim().equals("")) {
                                    //menambahkan ke layout
                                    add_layout_pertanyaan(s_tanya, s_jawab);
                                }else{
                                    Toast.makeText(BerkasSatu.this, "Pertanyaan dan jawaban tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        ;
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    public void add_layout_pertanyaan(final String tanya, final String jawab){
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        final View inflatedLayout= inflater.inflate(R.layout.view_pertanyaan, llPertanyaan, false);

        final TextView t_tanya  = (TextView) inflatedLayout.findViewById(R.id.et_pertanyaan);
        final TextView t_jawab  = (TextView) inflatedLayout.findViewById(R.id.et_jawaban);

        final ImageButton btnDelete = (ImageButton) inflatedLayout.findViewById(R.id.img_hapus);
        final ImageButton btnEdit   = (ImageButton) inflatedLayout.findViewById(R.id.img_edit);

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showdialogHapusPertanyaan(tanya, inflatedLayout);
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showdialogEditPertanyaan(tanya,t_tanya,jawab,t_jawab);
                //Toast.makeText(BerkasSatu.this, "Edit : "+tanya, Toast.LENGTH_SHORT).show();
            }
        });

        t_tanya.setText(tanya);
        t_jawab.setText(jawab);

        llPertanyaan.addView(inflatedLayout);
        if(llPertanyaan.getChildCount()<=0){
            tv_warning_pertanyaan.setTextColor(Color.RED);
        }else{
            tv_warning_pertanyaan.setText("Minimal 1 pertanyaan dan jawaban");
            tv_warning_pertanyaan.setTextColor(Color.GRAY);
        }
    }
    public void showdialogEditPertanyaan(String tanya, final TextView t_tanya, String jawab, final TextView t_jawab) {
        LayoutInflater layoutInflater = (LayoutInflater) getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.dialog_pertanyaan, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BerkasSatu.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final EditText et_pertanyaan= (EditText) promptView.findViewById(R.id.et_dialog_pertanyaan);
        final EditText et_jawaban   = (EditText) promptView.findViewById(R.id.et_dialog_jawaban);

        t_title.setText("Pertanyaan");
        et_pertanyaan.setText(tanya);
        et_jawaban.setText(jawab);

        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Simpan",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String s_jawab  = et_jawaban.getText().toString();
                                String s_tanya  = et_pertanyaan.getText().toString();
                                if (!s_jawab.trim().equals("") && !s_tanya.trim().equals("")) {
                                    //mengganti ke layout
                                    t_tanya.setText(s_tanya);
                                    t_jawab.setText(s_jawab);
                                }else{
                                    Toast.makeText(BerkasSatu.this, "Pertanyaan dan jawaban tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        ;
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    private void showdialogHapusPertanyaan(String text, final View view){            //untuk menghapus pertanyaan pada view
        // get prompts.xml view
        LayoutInflater layoutInflater = (LayoutInflater)getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.notif_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BerkasSatu.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title  = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final TextView t_text   = (TextView) promptView.findViewById(R.id.t_text_dialog);

        //untuk membuat ... jika lebih dari 25
        if(text.length() >= 25){
            text = text.substring(0,25)+"...";
        }
        t_title.setText("Hapus Pertanyaan");
        t_text.setText("Apakah anda yakin menghapus pertanyaan \""+text+"\"?");
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //hapus view pertanyaan

                        ((ViewGroup) view.getParent()).removeView(view);
                    }
                })
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //***************************************************untuk menddapatkan lokasi*****************************
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }
    private void getLokasiInit(){
        //mStartUpdatesButton = (Button) findViewById(R.id.start_updates_button);
        //mStopUpdatesButton = (Button) findViewById(R.id.stop_updates_button);
        mLatLongLabel   = (TextView) findViewById(R.id.tv_latlong_label);
        mLatLong        = (TextView) findViewById(R.id.tv_latlong);

        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";

        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
    }
    protected synchronized void buildGoogleApiClient() {
        Log.i(getLocalClassName(), "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }
    protected void checkLocationSettings() {
        Log.d(GlobalConfig.TAG,"checkLocationSettings");
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }
    @Override
    public void onConnected(Bundle bundle) {
        Log.i(getLocalClassName(), "Connected to GoogleApiClient");
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
            //updateLocationUI();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(getLocalClassName(), "Location onConnectionSuspended");
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

        updateLocationUI();

        Toast.makeText(this, "Lokasi telah diperbarui",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(getLocalClassName(), "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(getLocalClassName(), "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(getLocalClassName(), "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(BerkasSatu.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i(getLocalClassName(), "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i(getLocalClassName(), "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }
    protected void startLocationUpdates() {
        pDialog = new ProgressDialog(BerkasSatu.this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Mohon tunggu…");
        pDialog.show();
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = true;
            }
        });

    }
    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            btnLokasi.setText("Perbarui Lokasi");
            String yourAddress = "";
            String yourCity = "";
            String yourCountry = "";
            //get address
            Geocoder geocoder;
            List<Address> yourAddresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            try {
                yourAddresses = geocoder.getFromLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), 1);

                if (yourAddresses.size() > 0) {
                    yourAddress = yourAddresses.get(0).getAddressLine(0);
                    yourCity = yourAddresses.get(0).getAddressLine(1);
                    yourCountry = yourAddresses.get(0).getAddressLine(2);
                }
            }catch (Exception e){

            }

            float lat  = (float) (mCurrentLocation.getLatitude());
            float lon = (float) (mCurrentLocation.getLongitude());

            //mengilangkan progress dialog
            pDialog.dismiss();

            String latlong = "Koordinat : <a href=\"http://maps.google.com/?q=<"+lat+">,<"+lon+">\">"+lat+";"+lon+"</a>";

            mLatLong.setText(lat+";"+lon);
            tv_warning_koordinat.setVisibility(View.GONE);
            mLatLongLabel.setText(Html.fromHtml(latlong));
            mLatLongLabel.setMovementMethod(LinkMovementMethod.getInstance());

            stopLocationUpdates();
        }
    }
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
            }
        });
    }
    //***************************************************untuk menddapatkan lokasi*****************************
}
