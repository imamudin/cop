package com.imamudin.cop;

/**
 * Created by agung on 19/02/2016.
 */
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

//untuk google cloud messaging
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;

import com.imamudin.cop.app.MyAppController;
import com.imamudin.cop.config.CLogin;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.mysp.ObscuredSharedPreferences;

public class Login extends AppCompatActivity {
    Button btn_login;
    EditText et_user_name, et_password;
    String s_user_name, s_password;

    ObscuredSharedPreferences pref;
    GoogleCloudMessaging gcm;

    //untuk handler GCM
    private static final int ACTION_PLAY_SERVICES_DIALOG = 100;
    protected static final int MSG_REGISTER_WITH_GCM = 101;
    protected static final int MSG_REGISTER_WEB_SERVER = 102;
    protected static final int MSG_REGISTER_WEB_SERVER_SUCCESS = 103;
    protected static final int MSG_REGISTER_WEB_SERVER_FAILURE = 104;

    private String gcmRegId;
    ProgressDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_form);

        init();

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //cek apakah hp konek internet
                ConnectivityManager cm =
                        (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if(isConnected){
                    //cek email dan passwod tidak boleh kosong
                    s_user_name     = et_user_name.getText().toString().trim();
                    s_password      = et_password.getText().toString().trim();

                    Log.d(GlobalConfig.TAG, "make progress dialog");
                    //loading = ProgressDialog.show(Login.this, "Login...", "Mohon tunggu...", false, false);
                    loading = ProgressDialog.show(Login.this, "Login...", "Mohon tunggu...", true);
                    //loading.show();

                    if(s_user_name.length()>0 && s_password.length()>0){
                        //melakukan login
                        if (isGoogelPlayInstalled()) {
                            //membutuhkan key untuk CGM
                            gcm = GoogleCloudMessaging.getInstance(getApplicationContext());

                            if (TextUtils.isEmpty(gcmRegId)) {
                                handler.sendEmptyMessage(MSG_REGISTER_WITH_GCM);
                            } else {
                                handler.sendEmptyMessage(MSG_REGISTER_WEB_SERVER);
                            }
                        }
                    }else{
                        Toast.makeText(Login.this, CLogin.notif_form_tidak_kosong, Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(Login.this,CLogin.notif_butuh_koneksi, Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    @Override
    protected void onDestroy() {
        try {
            if (loading != null && loading.isShowing()) {
                loading.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }
    private void init(){
        btn_login       = (Button)findViewById(R.id.btn_login);
        et_user_name    = (EditText)findViewById(R.id.et_user_name);
        et_password     = (EditText)findViewById(R.id.et_password);


        pref = new ObscuredSharedPreferences(this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        if(pref.getBoolean(CLogin.IS_LOGIN, false)){
            openMainActivity(pref.getInt(CLogin.G_TIPE_USER, 0));
            Log.d("login", "buka main");
        }
    }
    private boolean isGoogelPlayInstalled() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        ACTION_PLAY_SERVICES_DIALOG).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "Google Play Service is not installed",
                        Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;

    }
    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_WITH_GCM:
                    new GCMRegistrationTask().execute();
                    break;
                case MSG_REGISTER_WEB_SERVER:
                    login();

                    //new WebServerRegistrationTask().execute();
                    break;
                case MSG_REGISTER_WEB_SERVER_SUCCESS:
                    Toast.makeText(getApplicationContext(),
                            "registered with web server", Toast.LENGTH_LONG).show();
                    break;
                case MSG_REGISTER_WEB_SERVER_FAILURE:
                    Toast.makeText(getApplicationContext(),
                            "registration with web server failed",
                            Toast.LENGTH_LONG).show();
                    break;
            }
        }

        ;
    };
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.action_ip:
                showdialog_settingip();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void showdialog_settingip() {           //1 untuk pasang, 2 untuk cabut
        LayoutInflater layoutInflater = (LayoutInflater) getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.dialog_ip, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Login.this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final EditText et_ip = (EditText) promptView.findViewById(R.id.et_ip);

        t_title.setText("Masukan Nomor IP");
        //get session on sharepreferences
        String uri = pref.getString(GlobalConfig.IP_KEY,"");
        et_ip.setText(uri);

        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Simpan",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (et_ip.getText().toString() != "") {
                                    //save ip to shared preferences
                                    pref.edit().putString(GlobalConfig.IP_KEY, et_ip.getText().toString().trim()).commit();

                                    Toast.makeText(Login.this, "IP set : " + et_ip.getText().toString().trim(), Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        ;
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    private class GCMRegistrationTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            // TODO Auto-generated method stub
            if (gcm == null && isGoogelPlayInstalled()) {
                gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
            }
            try {
                gcmRegId = gcm.register(GlobalConfig.GCM_SENDER_ID);
                Log.d("gcmregId",""+gcmRegId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return gcmRegId;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                //Toast.makeText(getApplicationContext(), "registered with GCM",Toast.LENGTH_LONG).show();
                //saveInSharedPref(result);
                handler.sendEmptyMessage(MSG_REGISTER_WEB_SERVER);
            }
        }
    }
    private void login(){
        if(loading == null) {
            Log.d(GlobalConfig.TAG, "make progress dialog2");
            loading = ProgressDialog.show(Login.this, "Login...", "Mohon tunggu...", true);
        }
        String url ="";
        if(pref.getString(GlobalConfig.IP_KEY, null) != null){
            url = "http://"+pref.getString(GlobalConfig.IP_KEY, "")+GlobalConfig.WEB_URL+CLogin.URL_LOGIN;
        }else{
            url = "http://"+GlobalConfig.IP+GlobalConfig.WEB_URL+CLogin.URL_LOGIN;
        }
        Log.d(GlobalConfig.TAG,""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(CLogin.USER_NAME, s_user_name);
            jsonBody.put(CLogin.USER_PASWORD, s_password);
            jsonBody.put(CLogin.USER_REGID, gcmRegId);
            // other key-value pairs...

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    Log.d(GlobalConfig.TAG, "tutup loading");


                    try {
                        int status = response.getInt("status");
                        int role = 0;
                        if(status==1){
                            JSONArray data = response.getJSONArray("data");
                            for(int i=0;i<data.length();i++){
                                JSONObject object   = data.getJSONObject(i);

                                role = Integer.parseInt(object.getString(CLogin.G_IS_OPSNAL));

                                pref.edit().putBoolean(CLogin.IS_LOGIN, true).commit();
                                pref.edit().putString(CLogin.G_ID_USER, object.getString(CLogin.G_ID_USER)).commit();
                                pref.edit().putString(CLogin.G_USER_NAME, object.getString(CLogin.G_USER_NAME)).commit();
                                pref.edit().putInt(CLogin.G_TIPE_USER, role).commit();
                                pref.edit().putString(CLogin.G_NAMA_SATWIL, object.getString(CLogin.G_NAMA_SATWIL)).commit();
                                pref.edit().putString(CLogin.G_NAMA_SATKER, object.getString(CLogin.G_NAMA_SATKER)).commit();
                                pref.edit().putString(CLogin.G_ID_SATKER, object.getString(CLogin.G_ID_SATKER)).commit();
                                pref.edit().putString(CLogin.G_NAMA_POLSEK, object.getString(CLogin.G_NAMA_POLSEK)).commit();
                                pref.edit().putString(CLogin.G_tipe_satwil, object.getString(CLogin.G_tipe_satwil)).commit();
                                pref.edit().putInt(CLogin.G_IS_OPSNAL, Integer.parseInt(object.getString(CLogin.G_IS_OPSNAL))).commit();
                                pref.edit().putString(GlobalConfig.gcmregId, gcmRegId).commit();
                            }
                            openMainActivity(role);
                        }else{
                            Toast.makeText(getApplicationContext(), CLogin.notif_form_tidak_cocok, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    loading.dismiss();
                    Log.d("respons",error.getMessage().toString());
                }
            }){
                public Map<String, String> getHeaders() {
                Map<String,String> headers = new Hashtable<String, String>();

                //Adding parameters
                headers.put(GlobalConfig.APP_NAME, GlobalConfig.APP_ID);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void openMainActivity(int role){
        Intent Opsnal = new Intent(Login.this, MainActivityOpsnal.class);
        Intent Monitor = new Intent(Login.this, MainActivityMonitor.class);

        if(role==GlobalConfig.ROLE_OPSNAL){
            startActivity(Opsnal);
        }else{
            startActivity(Monitor);
        }
        Login.this.finish();
    }
}