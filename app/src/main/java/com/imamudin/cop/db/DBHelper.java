package com.imamudin.cop.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.imamudin.cop.config.CDatabase;

public class DBHelper extends SQLiteOpenHelper {
    private static String DB_PATH = "/data/data/com.imamudin.cop/databases/";
    private static String DB_NAME = "cop.db";

    public static final String DATABASE_NAME            = "cop.db";
    public static final String TABLE_NOTIFIKASI         = "notifikasi";

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        String tb_notifikasi ="create table "+TABLE_NOTIFIKASI+
                " ("+ CDatabase.N_ID+" integer primary key, "+
                CDatabase.N_NAMA_POLISI+" text,"+
                CDatabase.N_JENIS_BERKAS+" text,"+
                CDatabase.N_NAMA_KASUS+" text,"+
                CDatabase.N_NRP+" text,"+
                CDatabase.N_KASUS_ID+" text,"+
                CDatabase.N_BERKAS_KE+" integer,"+
                CDatabase.N_WAKTU+" text)";
        db.execSQL(tb_notifikasi);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NOTIFIKASI);
        onCreate(db);
    }

    public void dropAlltable(){
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("DROP TABLE " + TABLE_NOTIFIKASI);
    }
    public  void backup(){
        try {
            File file = new File(DB_PATH + DB_NAME); //Uri.toString());
            //FileInputStream myInpsut = FileInputStream(file); // myContext.getAssets().open(DB_NAME);
            FileInputStream myInput;

            myInput = new FileInputStream(file);


            // Path to the just created empty db
            String outFileName = "/sdcard/pln.db"; // DB_PATH + DB_NAME;

            //Open the empty db as the output stream
            OutputStream myOutput = new FileOutputStream(outFileName);

            //transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer))>0){
                myOutput.write(buffer, 0, length);
            }

            //Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            //Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
    public boolean insertNotifikasi (String nama_polisi, String jenis_berkas,
                                     String nama_kasus, String nrp, String kasus_id,
                                     String waktu, int berkas_ke)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("" + CDatabase.N_NAMA_POLISI, nama_polisi);
        contentValues.put("" + CDatabase.N_JENIS_BERKAS, jenis_berkas);
        contentValues.put("" + CDatabase.N_NAMA_KASUS, nama_kasus);
        contentValues.put("" + CDatabase.N_NRP, nrp);
        contentValues.put("" + CDatabase.N_KASUS_ID, kasus_id);
        contentValues.put("" + CDatabase.N_BERKAS_KE, berkas_ke);
        contentValues.put("" + CDatabase.N_WAKTU, waktu);

        db.insert("" + TABLE_NOTIFIKASI, null, contentValues);
        return true;
    }
    public ArrayList<String> getNotifikasiById(int m_id){
        ArrayList<String> array_list = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery("select * from " + TABLE_NOTIFIKASI + " where " + CDatabase.N_ID + "=" + m_id + "", null);
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndex(CDatabase.N_ID)));
            array_list.add(res.getString(res.getColumnIndex(CDatabase.N_NAMA_POLISI)));
            array_list.add(res.getString(res.getColumnIndex(CDatabase.N_JENIS_BERKAS)));
            array_list.add(res.getString(res.getColumnIndex(CDatabase.N_NAMA_KASUS)));
            array_list.add(res.getString(res.getColumnIndex(CDatabase.N_NRP)));
            array_list.add(res.getString(res.getColumnIndex(CDatabase.N_KASUS_ID)));
            array_list.add(res.getString(res.getColumnIndex(CDatabase.N_BERKAS_KE)));
            array_list.add(res.getString(res.getColumnIndex(CDatabase.N_WAKTU)));
            res.moveToNext();
        }
        return array_list;
    }
    public ArrayList<HashMap<String, String>> getAllNotifikasi()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery("select * from " + TABLE_NOTIFIKASI +
                        " order by "+CDatabase.N_ID+" DESC"
                , null);
        cursor.moveToFirst();

        ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for(int i=0; i<cursor.getColumnCount();i++)
                {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                maplist.add(map);
            } while (cursor.moveToNext());
        }
        return maplist;
    }
    public ArrayList<HashMap<String, String>> getLimitNotifikasi(int offset)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery("select * from " + TABLE_NOTIFIKASI +
                        " order by "+CDatabase.N_ID+" DESC limit "+CDatabase.LIMIT+" offset " +offset
                , null);
        cursor.moveToFirst();

        ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for(int i=0; i<cursor.getColumnCount();i++)
                {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                maplist.add(map);
            } while (cursor.moveToNext());
        }
        return maplist;
    }
    public boolean deleteNotifikasiById(int m_id){
        SQLiteDatabase db = this.getWritableDatabase();
        if( db.delete(TABLE_NOTIFIKASI+"",
                CDatabase.N_ID+" =  "+m_id,
                null) ==1)
            return true;
        else
            return false;
    }
    public void deleteAllNotifikasi(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_NOTIFIKASI);
    }
    public int totalNotifikasi(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_NOTIFIKASI);
        return numRows;
    }
}
