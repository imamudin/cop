package com.imamudin.cop;

/**
 * Created by agung on 19/02/2016.
 */
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.imamudin.cop.app.MyAppController;
import com.imamudin.cop.berita_acara.InterogasiNotif;
import com.imamudin.cop.berita_acara.LITDokumenNotif;
import com.imamudin.cop.berita_acara.ObservasiNotif;
import com.imamudin.cop.berita_acara.SurveillanceNotif;
import com.imamudin.cop.config.CDatabase;
import com.imamudin.cop.config.CLogin;
import com.imamudin.cop.config.CBerkasSaya;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.listAdapter.ListAdapterMonitor;
import com.imamudin.cop.model.MonitoringItem;
import com.imamudin.cop.mysp.ObscuredSharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class FragmentMyBerkas extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    List<MonitoringItem> monitoringItems = new ArrayList<MonitoringItem>();
    ListView lv1;
    ListAdapterMonitor adapter;

    private SwipeRefreshLayout swipeContainer;

    int offSet=0;

    Runnable runnable;
    Boolean disableSwipeDown = false;       //untuk mendisable swipe down list view

    LinearLayout ll_main;

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    JsonObjectRequest request;
    private ObscuredSharedPreferences pref;

    public FragmentMyBerkas() {
        // Required empty public constructor
    }
    public static FragmentMyBerkas newInstance(String param1, String param2) {
        FragmentMyBerkas fragment = new FragmentMyBerkas();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        ((MainActivityOpsnal) getActivity()).setActionBarTitle("Berkas Saya");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_berkas, container, false);

        init(rootView);

        lv1 = (ListView)rootView.findViewById(R.id.custom_list);
        monitoringItems.clear();
        adapter = new ListAdapterMonitor(getActivity(), monitoringItems);
        lv1.setAdapter(adapter);

        //untuk swipelist
        swipeContainer = (SwipeRefreshLayout)rootView.findViewById(R.id.swipeContainer);

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callNews(0);
                Toast.makeText(getActivity(),"memperbarui",Toast.LENGTH_LONG).show();
            }
        });

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected item text from ListView
                Object o = lv1.getItemAtPosition(position);
                MonitoringItem monitoringItem = (MonitoringItem) o;

                Intent output = null;

                if(monitoringItem.getId_doc_opsnal().equals("DOC11")){        //untuk observasi
                    output = new Intent(getActivity(), ObservasiNotif.class);
                }else if(monitoringItem.getId_doc_opsnal().equals("DOC12")) {        //untuk interogasi
                    output = new Intent(getActivity(), InterogasiNotif.class);
                }else if(monitoringItem.getId_doc_opsnal().equals("DOC13")) {        //untuk surveillance
                    output = new Intent(getActivity(), SurveillanceNotif.class);
                }else if(monitoringItem.getId_doc_opsnal().equals("DOC16")) {        //untuk LIT Dokumen
                    output = new Intent(getActivity(), LITDokumenNotif.class);
                }else if(monitoringItem.getId_doc_opsnal().equals("DOC12")) {        //untuk interogasi
                    output = new Intent(getActivity(), InterogasiNotif.class);
                }

                if(output!=null) {
                    output.putExtra("dari_notifikasi", true);
                    output.putExtra(CDatabase.N_NRP, monitoringItem.getNrp());
                    output.putExtra(CDatabase.N_KASUS_ID, monitoringItem.getId_kasus());
                    output.putExtra(CDatabase.N_BERKAS_KE, monitoringItem.getBerkas_ke());

                    startActivity(output);
                }
            }
        });

        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                callNews(0);
            }
        });

        lv1.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }
            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {
                    if(!disableSwipeDown) {
                        Log.d(GlobalConfig.TAG, "start refresh");
                        runnable = new Runnable() {
                            public void run() {
                                Log.d(GlobalConfig.TAG, "start refresh");
                                callNews(offSet);
                            }
                        };
                        runnable.run();
                    }else{
                        Log.d(GlobalConfig.TAG, "disabled");
                        //Toast.makeText(CariKasus.this,"Data telah ditampilkan semua.",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        return rootView;
    }
    private void init(View rootView){
        ll_main     = (LinearLayout)rootView.findViewById(R.id.ll_main);

        pref = new ObscuredSharedPreferences(getActivity(),
                this.getActivity().getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
    }
    private void callNews(int page) {
        swipeContainer.setRefreshing(true);
        if(page==0){
            monitoringItems.clear();
            adapter.notifyDataSetChanged();
        }
        // Creating volley request obj
        String url = "";
        if (pref.getString(GlobalConfig.IP_KEY, null) != null) {
            url = "http://" + pref.getString(GlobalConfig.IP_KEY, "") + GlobalConfig.WEB_URL + CBerkasSaya.URL_BERKAS_SAYA;
        } else {
            url = "http://" + GlobalConfig.IP + GlobalConfig.WEB_URL + CBerkasSaya.URL_BERKAS_SAYA;
        }
        Log.d(GlobalConfig.TAG, "" + url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(CBerkasSaya.OFFSET, "" + page);
            jsonBody.put(CBerkasSaya.USER_ID, "" + pref.getString(CLogin.G_ID_USER, ""));
            jsonBody.put(CBerkasSaya.USER_REGID, "" + pref.getString(GlobalConfig.gcmregId, ""));

            Log.d(GlobalConfig.TAG, jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(GlobalConfig.TAG, response.toString());
                    try {
                        swipeContainer.setRefreshing(false);
                        int status = response.getInt("status");
                        String pesan = response.getString("pesan");
                        Log.d(GlobalConfig.TAG, "" + status);
                        if (status == 1) {
                            Log.d(GlobalConfig.TAG, "" + status);
                            JSONArray monitors = response.getJSONArray("data");
                            if (monitors.length() < 50) {         //50 dari jumlah limit di web
                                disableSwipeDown = true;
                            }
                            offSet = offSet + monitors.length();
                            if (monitors.length() > 0) {
                                for (int i = 0; i < monitors.length(); i++) {
                                    JSONObject monitor = monitors.getJSONObject(i);

                                    MonitoringItem mi = new MonitoringItem();
                                    mi.setId_kasus(monitor.getString(CBerkasSaya.G_ID_KASUS));
                                    mi.setId_doc_opsnal(monitor.getString(CBerkasSaya.G_ID_DOC));
                                    mi.setBerkas_ke(Integer.parseInt(monitor.getString(CBerkasSaya.G_BERKAS_KE)));
                                    mi.setTgl_ba(monitor.getString(CBerkasSaya.G_TGL_BA));
                                    mi.setNrp(monitor.getString(CBerkasSaya.G_NRP));
                                    mi.setUser_nama(monitor.getString(CBerkasSaya.G_USER_NAMA));
                                    mi.setNama_kasus(monitor.getString(CBerkasSaya.G_NAMA_KASUS));
                                    mi.setNo_lp(monitor.getString(CBerkasSaya.G_NO_LP));
                                    mi.setNama_pelapor(monitor.getString(CBerkasSaya.G_NAMA_PELAPOR));
                                    mi.setNama_penyidik(monitor.getString(CBerkasSaya.G_NAMA_PENYIDIK));

                                    monitoringItems.add(mi);

                                    swipeContainer.setRefreshing(false);

                                    adapter.notifyDataSetChanged();
                                }
                            } else {
                                notifikasi("Data tidak ditemukan.");
                            }
                        } else {
                            notifikasi(pesan);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        swipeContainer.setRefreshing(false);
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    swipeContainer.setRefreshing(false);
                    //Log.d("respons",error.getMessage().toString());
                }
            }) {
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_NAME, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void setColorTV(LinearLayout ll_tgl, LinearLayout ll_nama_kasus, LinearLayout ll_nama_pelapor, LinearLayout ll_no_lp){
        if(ll_tgl.getChildAt(0) instanceof TextView){
            ((TextView) ll_tgl.getChildAt(0)).setTextColor(Color.BLACK);
            ((TextView) ll_nama_kasus.getChildAt(0)).setTextColor(Color.BLACK);
            ((TextView) ll_nama_pelapor.getChildAt(0)).setTextColor(Color.BLACK);
            ((TextView) ll_no_lp.getChildAt(0)).setTextColor(Color.BLACK);
        }
        //diawal ll_tgl yang pertama
        TextView a = ((TextView) ll_tgl.getChildAt(0));
        a.setTextColor(getResources().getColor(R.color.colorSuccess));
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}