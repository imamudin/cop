package com.imamudin.cop;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.imamudin.cop.config.CMonitor;
import com.imamudin.cop.config.GlobalConfig;
import com.imamudin.cop.mysp.ObscuredSharedPreferences;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by imamudin on 17/11/16.
 */
public class MenuFilter extends AppCompatActivity {
    Spinner sp_filter;
    LinearLayout ll_main, ll_tgl;
    Button btn_terapkan;
    EditText et_kata_kunci;

    boolean firt_time;

    TextView tv_tgl_value;

    ObscuredSharedPreferences pref;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_filter);

        init();
    }
    private void init(){
        firt_time = true;
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Filter");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(MenuFilter.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        ll_main     = (LinearLayout)findViewById(R.id.ll_main);
        ll_tgl      = (LinearLayout)findViewById(R.id.ll_tgl);

        sp_filter   = (Spinner)findViewById(R.id.sp_filter);
        btn_terapkan= (Button)findViewById(R.id.btn_terapkan);
        et_kata_kunci= (EditText)findViewById(R.id.et_filter);
        tv_tgl_value = (TextView)findViewById(R.id.tv_tgl_value);
        tv_tgl_value.setText("");

        ll_tgl.setOnClickListener(myListen);
        btn_terapkan.setOnClickListener(myListen);

        String [] menu_sorts = {
                "Nama Kasus","Nama Pelapor","NO LP",
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MenuFilter.this,
                android.R.layout.simple_spinner_item, menu_sorts);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_filter.setAdapter(adapter);
        sp_filter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View selectedItemView, int pos, long id) {
                et_kata_kunci.setText("");
                et_kata_kunci.setHint("Masukan "+parent.getItemAtPosition(pos).toString());

                if(firt_time) {
                    //jika pernah melakukan pencarian, menampilkan kembali
                    et_kata_kunci.setText(pref.getString(CMonitor.FILTER_VALUE, ""));
                    firt_time = false;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        //jika pernah melakukan pencarian, menampilkan kembali
        if (!pref.getString(CMonitor.FILTER_BY,"").equals("")) {
            int spinnerPosition = adapter.getPosition(CMonitor.menu_filter_to_spinner.get(pref.getString(CMonitor.FILTER_BY,"'")));
            sp_filter.setSelection(spinnerPosition);
        }
        if(!pref.getString(CMonitor.TGL_MULAI,"").equals("") && !pref.getString(CMonitor.TGL_AKHIR,"").equals("")){
            tv_tgl_value.setText(rubahFormatTanggal("yyyy-MM-dd",pref.getString(CMonitor.TGL_MULAI,""), "dd MMM yyyy")+
                    "-"+rubahFormatTanggal("yyyy-MM-dd",pref.getString(CMonitor.TGL_AKHIR,""), "dd MMM yyyy"));
        }
        Log.d(GlobalConfig.TAG, pref.getString(CMonitor.FILTER_VALUE, ""));
        et_kata_kunci.setText(pref.getString(CMonitor.FILTER_VALUE, ""));
    }
    View.OnClickListener myListen   = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            if(v == ll_tgl){
                Intent intent_tgl = new Intent(MenuFilter.this, MenuFilterTanggal.class);
                startActivityForResult(intent_tgl, CMonitor.REQUEST_CODE_MENU_FILTER_TGL);
            }else if(v == btn_terapkan){
                //terapkan
                pref.edit().putString(CMonitor.FILTER_BY, CMonitor.menu_filter_to_sql.get(sp_filter.getSelectedItem().toString())).commit();
                pref.edit().putString(CMonitor.FILTER_VALUE, et_kata_kunci.getText().toString().trim()).commit();

                String [] dates = tv_tgl_value.getText().toString().split("-");

                if(dates.length==2) {
                    pref.edit().putString(CMonitor.TGL_MULAI, rubahFormatTanggal("dd MMM yyyy",dates[0], "yyyy-MM-dd")).commit();
                    pref.edit().putString(CMonitor.TGL_AKHIR, rubahFormatTanggal("dd MMM yyyy",dates[1], "yyyy-MM-dd")).commit();
                }
                setResult(RESULT_OK);
                finish();
            }
        }
    };
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CMonitor.REQUEST_CODE_MENU_FILTER_TGL && resultCode == RESULT_OK && data != null) {
            String tgl_awal     = data.getStringExtra(CMonitor.TGL_MULAI);
            String tgl_akhir    = data.getStringExtra(CMonitor.TGL_AKHIR);

            tv_tgl_value.setText(tgl_awal+"-"+tgl_akhir);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(1, 2, 1, "Hapus Filter").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    private String rubahFormatTanggal(String format_asal, String tanggal, String format_tujuan){
//        SimpleDateFormat dateFormat = new SimpleDateFormat(
//                "yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat(format_asal);
        Date myDate = null;
        try {
            myDate = dateFormat.parse(tanggal);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat(format_tujuan);
        return timeFormat.format(myDate);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case 2:
                //hapus sharedpreferenc
                pref.edit().remove(CMonitor.FILTER_BY).commit();
                pref.edit().remove(CMonitor.FILTER_VALUE).commit();
                pref.edit().remove(CMonitor.TGL_MULAI).commit();
                pref.edit().remove(CMonitor.TGL_AKHIR).commit();
                tv_tgl_value.setText("");
                et_kata_kunci.setText("");
                setResult(RESULT_OK);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
}
