package com.imamudin.cop.config;

/**
 * Created by agung on 05/04/2016.
 */
public class CGCM {
    //UNTUK DEFINISI JENIS BERKAS

    //VARIABLE PASS
    public static final String N_ID                 = "n_id";
    public static final String G_NAMA_PENYIDIK      = "NAMA_PENYIDIK";
    public static final String G_ID_DOC_OPSNAL      = "ID_DOC_OPSNAL";
    public static final String G_NAMA_KASUS         = "NAMA_KASUS";
    public static final String G_NRP                = "NRP";
    public static final String G_KASUS_ID           = "ID_KASUS";
    public static final String G_WAKTU              = "tgl_masuk_data";
    public static final String G_BERKAS_KE          = "BERKAS_KE";

    public static final String G_NAMA_KEJADIAN     = "NAMA_KEJADIAN";
    public static final String G_ALAMAT            = "LOKASI";
    public static final String G_NAMA_PELAKU       = "PELAKU";
    public static final String G_NAMA_KORBAN       = "KORBAN";
    public static final String G_KERUGIAN          = "KERUGIAN";
    public static final String G_SAKSI             = "SAKSI";
    public static final String G_KETERANGAN        = "KETERANGAN";
    public static final String G_LATITUDE          = "LATITUDE";
    public static final String G_LONGITUDE         = "LONGITUDE";
    public static final String G_TGL_KEJADIAN      = "TGL_KEJADIAN";
    public static final String G_ID_KEJADIAN       = "ID_KEJADIAN";
}
