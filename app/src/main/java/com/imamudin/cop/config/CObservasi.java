package com.imamudin.cop.config;

/**
 * Created by imamudin on 06/11/16.
 */
public class CObservasi {
    public static final String KODE_BERKAS      = "DOC11";

    public static final String STATUS   = "status";
    public static final String MESSAGE  = "message";
    public static final String KASUS    = "kasus";

    public static final String URL_CARI_KASUS       = "/observasi/carikasus";
    public static final String URL_UPLOAD_DOKUMEN   = "/observasi/simpanberkas";
    public static final String URL_GET_BERKAS       = "/observasi/getObservasi";

    public static final String OFFSET    = "offset";

    //LEVEL 2 (DATA)
    public static final String USER_ID      = "user_id";
    public static final String USER_NAMA    = "user_nama";
    public static final String USER_REGID   = "user_regId";
    public static final String USER_ROLE    = "user_role";

    //DATABASE KASUS
    public static final String KASUS_ID             = "ID_KASUS";
    public static final String KASUS_NAMA           = "NAMA_KASUS";
    public static final String KASUS_TANGGAL        = "TGL_KEJADIAN";
    public static final String KASUS_NO_LP          = "NO_LP";
    public static final String KASUS_NAMA_PELAPOR   = "NAMA_PELAPOR";
    public static final String KASUS_URUT           = "kasus_urut";

    public static final int KODE_BERKAS1_CARIKASUS   = 100;
    public static final int KODE_BERKAS1_CARILOKASI  = 102;
    public static final int KODE_ZOOM_HAPUS     = 0;
    public static final int KODE_ZOOM_IMAGE     = 101;
    public static final int KODE_ZOOM_TETAP     = 1;


    public static final String LONGITUDE    = "longitude";
    public static final String LATITUDE     = "latitude";


    //untuk jenis cari kasus
    public static final String NO_LP        = "NO_LP";
    public static final String NAMA_PELAPOR = "NAMA_PELAPOR";
    public static final String KEYWORD      = "keyword";
    public static final String KEYWORD_JENIS= "keyword_jenis";

    public static final String IMG_IDCHILD      = "child_id";
    public static final String IMG_PATH         = "img_path";
    public static final String IMG_VIEW_ID      = "img_view_id";


    public static final String KODE_ZOOM_STATUS = "HAPUS";
    public static final String KEYUP_KASUS_ID   = "kasus_id";
    public static final String KEYUP_KASUS_NAMA = "kasus_nama";
    public static final String KEYUP_DASAR      = "dasar";
    public static final String KEYUP_SASARAN_TEMPAT= "sasaran_tempat";
    public static final String KEYUP_TEMPAT     = "tempats";
    public static final String KEYUP_GAGASAN    = "gagasans";
    public static final String KEYUP_GAMBAR     = "gambar";
    public static final String KEYUP_KOORDINAT  = "koordinat";
    public static final String KEYUP_NRP        = "nrp";
    public static final String KEYUP_BERKAS_KE  = "berkas_ke";

    public static final String G_BERKAS_KE      = "berkas_ke";
    public static final String G_TGL_BA         = "TGL_BA";
    public static final String G_SASARAN_TEMPAT = "SASARAN_TEMPAT";
    public static final String G_DASAR          = "DASAR";
    public static final String G_LATITUDE       = "LATITUDE";
    public static final String G_LONGITUDE      = "LONGITUDE";
    public static final String G_NAMA_KASUS     = "NAMA_KASUS";

    public static final String G_INFORMASI      = "INFORMASI";
    public static final String G_KELENGKAPAN    = "KELENGKAPAN";
    public static final String G_GAMBAR         = "GAMBAR";
}
