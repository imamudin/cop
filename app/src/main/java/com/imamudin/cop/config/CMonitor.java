package com.imamudin.cop.config;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by imamudin on 18/11/16.
 */
public class CMonitor {
    public static final String SORT_BY          = "sort_by";
    public static final String SORT_BY_TEMP     = "sort_by_temp";
    public static final int [] SORT_BY_VALUE    = {0, 1, 2, 3};

    public static final String FILTER_BY        = "filter_by";
    public static final String FILTER_VALUE     = "filter_value";

    public static final String TGL_MULAI        = "tgl_mulai";
    public static final String TGL_AKHIR        = "tgl_akhir";

    public static final int REQUEST_CODE_MENU_FILTER     = 101;
    public static final int REQUEST_CODE_MENU_FILTER_TGL = 130;

    public static final String [] SORT_TABLE    = {
            "tgl_ba desc",
            "nama_kasus asc",
            "nama_pelapor asc",
            "no_Lp asc",
    };

    public static final Map<String, String> menu_filter_to_sql;
    static
    {
        menu_filter_to_sql = new HashMap<String, String>();
        menu_filter_to_sql.put("Nama Kasus", "nama_kasus");
        menu_filter_to_sql.put("Nama Pelapor","nama_pelapor");
        menu_filter_to_sql.put("NO LP", "no_lp");
    }
    public static final Map<String, String> menu_filter_to_spinner;
    static
    {
        menu_filter_to_spinner = new HashMap<String, String>();
        menu_filter_to_spinner.put("nama_kasus", "Nama Kasus");
        menu_filter_to_spinner.put("nama_pelapor","Nama Pelapor");
        menu_filter_to_spinner.put("no_lp", "NO LP");
    }


    public static final String URL_MONITORING   = "/monitor";
    public static final String USER_ID          = "user_id";
    public static final String USER_REGID       = "user_regId";
    public static final String OFFSET           = "offset";

    public static final String G_ID_KASUS       = "id_kasus";
    public static final String G_ID_DOC         = "id_doc_opsnal";
    public static final String G_BERKAS_KE      = "berkas_ke";
    public static final String G_TGL_BA         = "tgl_ba";
    public static final String G_NRP            = "nrp";
    public static final String G_USER_NAMA      = "USER_NAME";
    public static final String G_NAMA_KASUS     = "NAMA_KASUS";
    public static final String G_NO_LP          = "NO_LP";
    public static final String G_NAMA_PELAPOR   = "NAMA_PELAPOR";
    public static final String G_NAMA_PENYIDIK  = "NAMA_PENYIDIK";
}
