package com.imamudin.cop.config;

/**
 * Created by agung on 13/04/2016.
 */
public class CDatabase {
    public static int LIMIT                             = 25;
    public static final String N_ID                     = "n_id";
    public static final String N_NAMA_POLISI            = "n_nama_polisi";
    public static final String N_JENIS_BERKAS           = "n_jenis_berkas";
    public static final String N_NAMA_KASUS             = "n_nama_kasus";
    public static final String N_NRP                    = "n_nrp";
    public static final String N_KASUS_ID               = "n_kasus_id";
    public static final String N_WAKTU                  = "n_waktu";
    public static final String N_BERKAS_KE              = "n_berkas_ke";
}
