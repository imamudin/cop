package com.imamudin.cop.config;

/**
 * Created by agung on 09/03/2016.
 */
public class CLogin {
    //URL
    public static final String URL_LOGIN = "/login";

    //SESSION
    public static final String IS_LOGIN = "is_login";

    //LEVEL 1
    public static final String STATUS   = "status";
    public static final String MESSAGE  = "message";
    public static final String DATA     = "data";

    //LEVEL 2 (DATA)
    public static final String USER_ID      = "user_id";
    public static final String USER_NAME    = "user_name";
    public static final String USER_REGID   = "user_regId";
    public static final String USER_ROLE    = "user_role";

    //SEND DATA
    public static final String USER_PASWORD = "user_password";

    //GET DATA FROM API
    public static final String G_ID_USER    = "ID_USER";
    public static final String G_USER_NAME  = "USER_NAME";
    public static final String G_TIPE_USER  = "TIPE_USER";
    public static final String G_NAMA_SATWIL= "NAMA_SATWIL";
    public static final String G_NAMA_SATKER= "NAMA_SATKER";
    public static final String G_ID_SATKER  = "ID_SATKER";
    public static final String G_NAMA_POLSEK= "NAMA_POLSEK";
    public static final String G_tipe_satwil= "tipe_satwil";
    public static final String G_IS_OPSNAL  = "IS_OPSNAL";


    //NOTIFIKASI
    public static final String notif_form_tidak_kosong  = "Username atau password tidak boleh kosong!";
    public static final String notif_form_tidak_cocok   = "Username atau password tidak cocok!";
    public static final String notif_butuh_koneksi      = "Aplikasi membutuhkan koneksi internet!";
}
