package com.imamudin.cop.config;
/**
 * Created by imamudin on 20/11/16.
 */
public class CBerkasSaya {
    public static final String URL_BERKAS_SAYA  = "/berkas_saya";
    public static final String USER_ID          = "user_id";
    public static final String USER_REGID       = "user_regId";
    public static final String OFFSET           = "offset";

    public static final String G_ID_KASUS       = "id_kasus";
    public static final String G_ID_DOC         = "id_doc_opsnal";
    public static final String G_BERKAS_KE      = "berkas_ke";
    public static final String G_TGL_BA         = "tgl_ba";
    public static final String G_NRP            = "nrp";
    public static final String G_USER_NAMA      = "USER_NAME";
    public static final String G_NAMA_KASUS     = "NAMA_KASUS";
    public static final String G_NO_LP          = "NO_LP";
    public static final String G_NAMA_PELAPOR   = "NAMA_PELAPOR";
    public static final String G_NAMA_PENYIDIK  = "NAMA_PENYIDIK";
}
