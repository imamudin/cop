package com.imamudin.cop.config;

import com.imamudin.cop.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by agung on 09/03/2016.
 */
public class GlobalConfig {
    public static final String FOLDER_NAMA      = "cop";
    public static final String PREF_GCM_REG_ID  = "PREF_GCM_REG_ID";        //mengakses GCM REG ID di sharedpreferences
    public static final String GCM_SENDER_ID    = " 871591514114 ";         //project ID dari console.google
    public static final String KEY_PREFERENCES  = "shared_com.imamudin.cop";//key untuk mengakses sharedpreferences
    public static final String APP_ID           = "com.imamudin.cop";       //key untuk menerima gcm
    public static final String APP_NAME         = "appName";                //key untuk mengirim app
    public static final String gcmregId         = "gcmregId";               //key untuk menyimpan gcmregId

    public static final String NAMA_PREF        = "com.imamudin.cop.pref";  //key untuk menyimpan file preferences

    //192.168.43.208
    public static final String IP               = "cop.muslimtekno.com";          //alamat web/ip website
    //public static final String IP               = "192.168.43.208";          //alamat web/ip website
    public static final String IP_KEY           = "ip";                     //key untuk alamat web/ip website
//    public static final String WEB_URL          = "/apicop";                //nama domain url
    public static final String WEB_URL          = "";                           //nama domain url


    public static final String TAG              = "mycop";                  //key untuk LOG

    public static final int ROLE_OPSNAL         = 1;                        //TIPE USER OPSNAL untuk input berkas
    public static final int ROLE_NON_OPSNAL     = 0;                        //TIPE USER MONITOR  untuk monitoring

    public static final int MY_SOCKET_TIMEOUT_MS   = 10000;                  //10 detik

    public static final String MSG_KESALAHAN    = "Mohon maaf terjadi kesalahan.";

    public static final Map<String, String> namaDokumen;
    static
    {
        namaDokumen = new HashMap<String, String>();
        namaDokumen.put("KEJ00", "Kejadian Menonjol");
        namaDokumen.put("DOC11", "Observasi");
        namaDokumen.put("DOC12", "Interogasi");
        namaDokumen.put("DOC13", "Surveillance");
        namaDokumen.put("DOC16", "LIT Dokumen");
    }

    public static final Map<String, Integer> imgDokumen;
    static
    {
        imgDokumen = new HashMap<String, Integer>();
        imgDokumen.put("KEJ00", R.drawable.ic_kejadian_baru_48);
        imgDokumen.put("DOC11", R.drawable.ic_observasi);
        imgDokumen.put("DOC12", R.drawable.ic_interogasi);
        imgDokumen.put("DOC13", R.drawable.ic_surveillance);
        imgDokumen.put("DOC16", R.drawable.ic_dokumen);
    };


    public static final String USER_ID      = "user_id";
    public static final String USER_REGID   = "user_regId";

    public static final String UP_NAMA_KEJADIAN     = "nama_kejad";
    public static final String UP_ALAMAT            = "alamat";
    public static final String UP_NAMA_PELAKU       = "nama_pelaku";
    public static final String UP_NAMA_KORBAN       = "nama_korban";
    public static final String UP_KERUGIAN          = "kerugian";
    public static final String UP_SAKSI             = "saksi";
    public static final String UP_KETERANGAN        = "keterangan";
    public static final String UP_KOORDINAT         = "koordinat";
    public static final String UP_KEJADIAN_ID       = "kejadian_id";

    public static final int KODE_INTENT_NOTIFIKASI  = 105;
    public static final String URL_UPLOAD_KEJADIAN_BARU     = "/kejadian_baru";
    public static final String URL_get_KEJADIAN_BARU        = "/getKejadian_baru";

    public static final String G_NAMA_KEJADIAN     = "NAMA_KEJADIAN";
    public static final String G_ALAMAT            = "LOKASI";
    public static final String G_NAMA_PELAKU       = "PELAKU";
    public static final String G_NAMA_KORBAN       = "KORBAN";
    public static final String G_KERUGIAN          = "KERUGIAN";
    public static final String G_SAKSI             = "SAKSI";
    public static final String G_KETERANGAN        = "KETERANGAN";
    public static final String G_LATITUDE          = "LATITUDE";
    public static final String G_LONGITUDE         = "LONGITUDE";
    public static final String G_TGL_KEJADIAN      = "TGL_KEJADIAN";
    public static final String G_NAMA_PENYIDIK     = "NAMA_PENYIDIK";
    public static final String G_NRP               = "NRP";
    public static final String G_ID_KEJADIAN       = "ID_KEJADIAN";


}
